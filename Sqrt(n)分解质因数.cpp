#include <cstdio>
#include <map>
using namespace std;

typedef long long int64;

map<int64, int> Factorize(int64 n) {
    map<int64, int> answer;
    for (int64 i = 2; i * i <= n; ++i) {
        while (n % i == 0) {
            n /= i;
            ++answer[i];
        }
    }
    if (n != 1) ++answer[n];
    return answer;
}

int main() {
    int64 n;
    while (scanf("%lld", &n) == 1) {
        map<int64, int> a = Factorize(n);
        for (
            map<int64, int>::iterator it = a.begin();
            it != a.end();
            ++it
        ) {
            printf("%lld ** %d\n", it->first, it->second);
        }
    }
}
