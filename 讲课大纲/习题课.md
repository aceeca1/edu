# 习题课

1. [codeforces 263A] 美丽的矩阵。有一个 $5 \times 5$ 的矩阵，其中一个数是 1，其余数是 0。现在要让 1 走到正中间，每步能往上下左右四个方向走，请输出需要几步。
2. [codeforces 236A] 男孩还是女孩。如果一个用户名中不同的字母数量是奇数，就是男孩，反之则是女孩。如果输入是男孩，输出 IGNORE HIM!，否则输出 CHAT WITH HER!。
3. [codeforces 158B] 出租车。放学后，有 $n$ 组学生想要坐车出行。第 $i$ 组有 $s_i$ 个学生 ($1\le s_i \le 4$)，每组学生不能拆散，但是每辆车最多坐四个学生。请输出最少需要多少辆车，如果每辆车允许坐多于一组的学生，但是每组学生需要坐同一辆车？
4. [codeforces 122A] 幸运的除法。所有数字都是 4 或者 7 的数，叫做幸运数。能被一个幸运数整除的数，叫做勉强幸运数。输入 $n$，如果它是勉强幸运数，输出 YES，反之输出 NO。
5. [codeforces 271A] 美丽的年份。2013年是一个美丽的年份，因为它的四位数字各不相同。上一个美丽的年份还是 1987 年。输入一个年份，找到比它大的最小美丽年份并输出。
6. [codeforces 469A] 我想成为那家伙。有一个游戏，由 $n$ 个关卡构成。小 X 能过其中的 $p$ 关，小 Y 能过其中的 $q$ 关。第一行输入 $n$，第二行输入 $p$ 和小 X 能过哪些关，第三行输入 $q$ 和小 Y 能过哪些关，如果他们俩合作，能否通关？如果能，输出 I become the guy.，否则输出 Oh, my keyboard!。
7. [codeforces 443A] Anton 和字符。输入一个小写字母组成的集合，输出有多少个互异的元素。
8. [codeforces 228A] 马蹄穿鞋子。输入四个数表示四个鞋的颜色，现在一只马要穿四只不同颜色的鞋子，请输出还需要买多少鞋子。
9. [codeforces 520A] 全字母。一个串称为全字母的，当且仅当每个字母都在其中出现过，大小写均可。输入一个字符串，是全字母的输出 YES，否则输出 NO。
10. [codeforces 141A] 有趣的笑话。输入三个串，如果前两个串拆开重排能得到第三个串，输出 YES，否则输出 NO。