#include <cstdio>
using namespace std;

int main() {
    int a, b;
    scanf("%d%d", &a, &b);
    int fail = 0;
    if (a < 60) ++fail;
    if (b < 60) ++fail;
    printf(fail == 1 ? "YES\n" : "NO\n");
    return 0;
}
