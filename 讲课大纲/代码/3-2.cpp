#include <cstdio>
using namespace std;

int main() {
    int answer = 1;
    int i = 1;
    while (i <= 10) {
        answer *= i;
        ++i;
    }
    printf("%d\n", answer);
    return 0;
}
