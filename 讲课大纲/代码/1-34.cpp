#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    bool runNian = ((n % 4 == 0) && (n % 100 != 0))
        || (n % 400 == 0);
    printf(runNian ? "RunNian\n" : "PingNian\n");
    return 0;
}
