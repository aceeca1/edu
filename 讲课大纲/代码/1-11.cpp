#include <cstdio>
using namespace std;

int main() {
    int a;
    scanf("%d", &a);
    printf("%d%d\n", a % 10, a / 10);
    return 0;
}
