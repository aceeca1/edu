#include <cstdio>
using namespace std;

int main() {
    int answer = 0;
    int i = 1;
    while (i <= 99) {
        answer += i;
        i += 2;
    }
    printf("%d\n", answer);
    return 0;
}
