#include <cstdio>
using namespace std;

int main() {
    int a, b;
    scanf("%d%d", &a, &b);
    int step = b - a;
    if (step < 0) step = -step;
    if (12 - step < step) step = 12 - step;
    printf("%d\n", step);
    return 0;
}
