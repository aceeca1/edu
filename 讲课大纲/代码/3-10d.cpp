#include <cstdio>
using namespace std;

int main() {
    int sum3 = 0, sum5 = 0;
    for (int i = 1; i <= 100; ++i) {
        if (i % 3 == 0) sum3 += i;
        if (i % 5 == 0) sum5 += i;
    }
    printf("%d\n%d\n", sum3, sum5);
    return 0;
}
