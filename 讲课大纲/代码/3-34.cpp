#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    int police = 0, escaped = 0;
    for (int i = 0; i < n; ++i) {
        int x;
        scanf("%d", &x);
        if (0 < x) {
            police += x;
        } else if (police == 0) {
            ++escaped;
        } else {
            --police;
        }
    }
    printf("%d\n", escaped);
    return 0;
}
