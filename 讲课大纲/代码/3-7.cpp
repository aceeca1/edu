#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    int answer = 0;
    while (n % 10 == 0) {
        n /= 10;
        ++answer;
    }
    printf("%d\n", answer);
    return 0;
}
