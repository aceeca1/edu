#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    int answer = 0;
    for (int i = 0; i < n; ++i) {
        int x1, x2, x3;
        scanf("%d%d%d", &x1, &x2, &x3);
        if (2 <= x1 + x2 + x3) ++answer;
    }
    printf("%d\n", answer);
    return 0;
}
