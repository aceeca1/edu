#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    int answer = 0;
    for (int i = 0; i < n; ++i) {
        int actual, full;
        scanf("%d%d", &actual, &full);
        if (actual < full) ++answer;
    }
    printf("%d\n", answer);
    return 0;
}
