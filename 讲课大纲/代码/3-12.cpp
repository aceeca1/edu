#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    bool hard = false;
    for (int i = 0; i < n; ++i) {
        int x;
        scanf("%d", &x);
        if (x != 0) hard = true;
    }
    printf(hard ? "HARD\n" : "EASY\n");
    return 0;
}
