#include <cstdio>
using namespace std;

int main() {
    int n, m, a;
    scanf("%d%d%d", &n, &m, &a);
    int n1 = (n + a - 1) / a;
    int m1 = (m + a - 1) / a;
    printf("%d\n", n1 * m1);
    return 0;
}
