#include <cstdio>
using namespace std;

int main() {
    int answer = 0;
    for (int i = 1; i <= 100; ++i) {
        if (i % 3 == 0 || i % 5 == 0) answer += i;
    }
    printf("%d\n", answer);
    return 0;
}
