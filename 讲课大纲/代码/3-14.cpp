#include <cstdio>
using namespace std;

int main() {
    int k;
    scanf("%d", &k);
    for (int i = 0; i < k; ++i) printf("*");
    printf("\n");
    return 0;
}
