#include <cstdio>
using namespace std;

int main() {
    int a, b;
    scanf("%d%d", &a, &b);
    bool fail = a < 60 || b < 60;
    if (a < 60 && b < 60) fail = false;
    printf(fail ? "YES\n" : "NO\n");
    return 0;
}

