#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    bool hard = true;
    for (int i = 0; i < n; ++i) {
        int x;
        scanf("%d", &x);
        if (x == 0) hard = false;
    }
    printf(hard ? "HARD\n" : "EASY\n");
    return 0;
}
