#include <cstdio>
using namespace std;

int main() {
    int answer = 0;
    int i = 1;
    while (i <= 100) {
        if (i % 3 == 0 || i % 5 == 0) answer += i;
        ++i;
    }
    printf("%d\n", answer);
    return 0;
}
