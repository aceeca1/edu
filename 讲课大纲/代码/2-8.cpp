#include <cstdio>
using namespace std;

int main() {
    int year, month;
    scanf("%d%d", &year, &month);
    if (month == 1 || month == 3 ||
        month == 5 || month == 7 ||
        month == 8 || month == 10 ||
        month == 12) {
        printf("31\n");
    } else if (month != 2) {
        printf("30\n");
    } else if ((year % 400 == 0) ||
        ((year % 4 == 0) && (year % 100 != 0))) {
        printf("29\n");
    } else {
        printf("28\n");
    }
    return 0;
}
