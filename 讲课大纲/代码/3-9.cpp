#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    while (n != 1) {
        int next;
        if (n % 2 == 0) {
            next = n / 2;
            printf("%d / 2 = %d\n", n, next);
        } else {
            next = 3 * n + 1;
            printf("%d * 3 + 1 = %d\n", n, next);
        }
        n = next;
    }
    return 0;
}
