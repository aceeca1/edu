#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    for (int i = 0; i < n - 1; ++i) printf(" ");
    printf("A\n");
    for (int i = 2; i <= n; ++i) {
        for (int j = 0; j < n - i; ++j) printf(" ");
        printf("/");
        for (int j = 0; j < i + i - 3; ++j) {
            printf(i == n ? "_" : " ");
        }
        printf("\\\n");
    }
    return 0;
}
