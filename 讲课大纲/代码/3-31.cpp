#include <cstdio>
#include <climits>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    int answer1 = INT_MIN, answer2 = INT_MIN, answer3 = INT_MIN;
    for (int i = 0; i < n; ++i) {
        int x;
        scanf("%d", &x);
        if (answer1 < x) {
            answer3 = answer2;
            answer2 = answer1;
            answer1 = x;
        } else if (answer2 < x) {
            answer3 = answer2;
            answer2 = x;
        } else if (answer3 < x) {
            answer3 = x;
        }
    }
    printf("1st number: %d\n", answer1);
    printf("2nd number: %d\n", answer2);
    printf("3rd number: %d\n", answer3);
    return 0;
}
