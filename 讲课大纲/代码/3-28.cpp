#include <cstdio>
#include <climits>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    int answer = INT_MIN;
    for (int i = 0; i < n; ++i) {
        int x;
        scanf("%d", &x);
        if (answer < x) answer = x;
    }
    printf("%d\n", answer);
    return 0;
}
