#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    int current = 0, capacity = 0;
    for (int i = 0; i < n; ++i) {
        int depart, arrive;
        scanf("%d%d", &depart, &arrive);
        current -= depart;
        current += arrive;
        if (capacity < current) capacity = current;
    }
    printf("%d\n", capacity);
    return 0;
}
