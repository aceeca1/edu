#include <cstdio>
using namespace std;

int main() {
    int n, k;
    scanf("%d%d", &n, &k);
    int answer = 0, bottle = 0;
    while (n != 0) {
        answer += n;
        bottle += n;
        n = bottle / k;
        bottle %= k;
    }
    printf("%d\n", answer);
    return 0;
}
