#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    if (10 <= n) {
        printf("%d\n", n * 250);
    } else if (5 <= n) {
        printf("%d\n", n * 280);
    } else {
        printf("%d\n", n * 300);
    }
    return 0;
}
