#include <cstdio>
using namespace std;

int main() {
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    if (b < a && c < a) {
        printf("%d\n", a);
    } else if (c < b) {
        printf("%d\n", b);
    } else {
        printf("%d\n", c);
    }
    return 0;
}

