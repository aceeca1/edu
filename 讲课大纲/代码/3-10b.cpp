#include <cstdio>
using namespace std;

int main() {
    int answer = 0;
    for (int i = 1; i <= 100; ++i) answer += i * i;
    printf("%d\n", answer);
    return 0;
}
