#include <cstdio>
#include <climits>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    int answer = INT_MIN, times = 0;
    for (int i = 0; i < n; ++i) {
        int x;
        scanf("%d", &x);
        if (answer < x) {
            answer = x;
            times = 1;
        } else if (answer == x) {
            ++times;
        }
    }
    printf("%d appeared %d times.\n", answer, times);
    return 0;
}
