#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    double sum = 0.0;
    for (int i = 0; i < n; ++i) {
        int x;
        scanf("%d", &x);
        sum += x;
    }
    printf("%.12lf\n", sum / n);
    return 0;
}
