#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    int answer = 0;
    while (n != 0) {
        answer = answer * 10 + n % 10;
        n /= 10;
    }
    printf("%d\n", answer);
    return 0;
}
