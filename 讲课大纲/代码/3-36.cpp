#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i) {
        bool has7 = false;
        if (i % 7 == 0) {
            has7 = true;
        } else {
            for (int x = i; x != 0; x /= 10) {
                if (x % 10 == 7) {
                    has7 = true;
                    break;
                }
            }
        }
        if (has7) {
            printf("*\n");
        } else {
            printf("%d\n", i);
        }
    }
    return 0;
}
