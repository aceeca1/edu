#include <cstdio>
using namespace std;

int main() {
    int a, b;
    scanf("%d%d", &a, &b);
    if (b < a) {
        int t = a;
        a = b;
        b = t;
    }
    printf("%d %d\n", a, b);
    return 0;
}
