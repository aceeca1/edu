#include <cstdio>
using namespace std;

int main() {
    int answer = 0;
    int i = 1;
    while (i <= 100) {
        answer += i;
        ++i;
    }
    printf("%d\n", answer);
    return 0;
}
