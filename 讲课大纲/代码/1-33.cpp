#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    printf(n % 4 == 0 ? "RunNian\n" : "PingNian\n");
    return 0;
}
