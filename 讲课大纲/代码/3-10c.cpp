#include <cstdio>
using namespace std;

int main() {
    int answer = 0;
    for (int i = 1; i <= 99; i += 2) answer += i;
    printf("%d\n", answer);
    return 0;
}
