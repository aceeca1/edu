#include <cstdio>
using namespace std;

int main() {
    int answer = 1;
    for (int i = 1; i <= 10; ++i) answer *= i;
    printf("%d\n", answer);
    return 0;
}
