#include <cstdio>
using namespace std;

int main() {
    int a;
    scanf("%d", &a);
    printf("%d%d%d%d\n", a % 10, a / 10 % 10,
        a / 100 % 10, a / 1000);
    return 0;
}
