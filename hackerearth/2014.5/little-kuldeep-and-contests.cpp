#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Contest {
    int Begin, End;

    bool operator<(const Contest& that) {
        return Begin < that.Begin;
    }
};

struct Solution {
    int N;
    vector<Contest> Contests;

    bool Clash() {
        for (int i = 1; i < N; ++i) {
            if (Contests[i].Begin < Contests[i - 1].End) return true;
        }
        return false;
    }

    void Solve() {
        scanf("%d", &N);
        Contests.resize(N);
        for (int i = 0; i < N; ++i) {
            int beginH, beginM, endH, endM;
            scanf("%d:%d-%d:%d", &beginH, &beginM, &endH, &endM);
            Contests[i].Begin = beginH * 60 + beginM;
            Contests[i].End = endH * 60 + endM;
        }
        sort(Contests.begin(), Contests.end());
        printf(Clash() ? "Will need a moderator!\n"
                       : "Who needs a moderator?\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
