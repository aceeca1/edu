#include <algorithm>
#include <cstdio>
using namespace std;

struct Solution {
    int N, M, X1, Y1, X2, Y2, P;

    void Solve() {
        scanf("%d%d%d%d%d%d%d", &N, &M, &X1, &Y1, &X2, &Y2, &P);
        int x = (X1 - X2 + N) % N;
        x = min(x, N - x);
        int y = (Y1 - Y2 + M) % M;
        y = min(y, M - y);
        int answer = (x + y) * P;
        printf("%d\n", answer);
        printf(answer <= 1000 ? "You saved the group.\n"
                              : "Let go of the group.\n");
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
