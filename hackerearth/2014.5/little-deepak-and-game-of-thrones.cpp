#include <algorithm>
#include <cstdio>
#include <set>
#include <vector>
using namespace std;

typedef long long int64;

struct Event {
    int X, Delta;

    bool operator<(const Event& that) const {
        return X < that.X || X == that.X && that.Delta < Delta;
    }
};

struct Event2 {
    int X1, X2, Y, Delta;

    bool operator<(const Event2& that) const {
        return Y < that.Y || Y == that.Y && that.Delta < Delta;
    }
};

typedef multiset<Event>::iterator Type1;

struct Solution {
    int M, Deep;
    vector<Event2> Events2;
    multiset<Event> Events;

    bool Process(const Event& event) {
        Deep += event.Delta;
        return Deep > 0;
    }

    int64 Process(const Event2& event2) {
        Event eventS = {event2.X1, 1};
        Event eventT = {event2.X2, -1};
        if (event2.Delta == 1) {
            Events.insert(eventS);
            Events.insert(eventT);
        } else {
            Events.erase(Events.find(eventS));
            Events.erase(Events.find(eventT));
        }
        int64 answer = 0;
        Deep = 0;
        for (Type1 it = Events.begin(); it != Events.end(); ++it) {
            if (Process(*it)) {
                Type1 it2 = it;
                answer += (++it2)->X - it->X;
            }
        }
        return answer;
    }

    void Solve(int caseNo) {
        scanf("%d", &M);
        for (int i = 0; i < M; ++i) {
            int x0, y0, x1, y1;
            scanf("%d%d%d%d", &x0, &y0, &x1, &y1);
            Event2 eventS = {x0, x1, y0, 1};
            Event2 eventT = {x0, x1, y1, -1};
            Events2.push_back(eventS);
            Events2.push_back(eventT);
        }
        sort(Events2.begin(), Events2.end());
        int64 answer = 0;
        for (int i = 0; i < Events2.size(); ++i) {
            int64 length = Process(Events2[i]);
            if (i < Events2.size() - 1) {
                answer += length * (Events2[i + 1].Y - Events2[i].Y);
            }
        }
        printf("%lld\n", answer);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve(i);
    return 0;
}
