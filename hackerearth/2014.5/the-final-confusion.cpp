#include <cstdio>
using namespace std;

typedef long long int64;

int64 Choose(int64 n, int64 k) {
    int64 answer = 1;
    for (int i = 0; i < k; ++i) answer = answer * (n - i) / (i + 1);
    return answer;
}

int64 Catalan(int n) {
    return Choose(2 * n, n) / (n + 1);
}

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        printf("%lld\n", Catalan(N));
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
