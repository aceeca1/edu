#include <cstdio>
using namespace std;

typedef long long int64;

struct Solution {
    int64 A, B, M;

    void Solve() {
        scanf("%lld%lld%lld", &A, &B, &M);
        printf("%lld\n", B / M - (A - 1) / M);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
