#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

const int M = 1000000;
vector<int> MinFactor, NumCurrentFactor, Next;
vector<int64> DivisorNum;

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        printf("%lld\n", DivisorNum[N]);
    }
};

int main() {
    MinFactor.resize(M + 1);
    for (int i = 2; i <= M; ++i) {
        if (MinFactor[i] != 0) continue;
        MinFactor[i] = i;
        for (int64 j = int64(i) * i; j <= M; j += i) {
            if (MinFactor[j] == 0) MinFactor[j] = i;
        }
    }
    NumCurrentFactor.resize(M + 1);
    Next.resize(M + 1);
    for (int i = 2; i <= M; ++i) {
        int i2 = i / MinFactor[i];
        if (MinFactor[i] == MinFactor[i2]) {
            NumCurrentFactor[i] = NumCurrentFactor[i2] + 1;
            Next[i] = Next[i2];
        } else {
            NumCurrentFactor[i] = 1;
            Next[i] = i2;
        }
    }
    DivisorNum.resize(M + 1);
    DivisorNum[1] = 1;
    for (int i = 2; i <= M; ++i) {
        DivisorNum[i] = DivisorNum[Next[i]] * (NumCurrentFactor[i] + 1);
    }
    for (int i = 1; i <= M; ++i) {
        DivisorNum[i] += DivisorNum[i - 1] - 1;
    }
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
