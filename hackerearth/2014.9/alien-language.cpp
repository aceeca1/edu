#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[100001];

struct Solution {
    string S1, S2;

    vector<bool> Stat(const string& s) {
        vector<bool> answer(26);
        for (int i = 0; i < s.size(); ++i) {
            answer[s[i] - 'a'] = true;
        }
        return answer;
    }

    void Solve() {
        scanf("%s", Buffer);
        S1 = Buffer;
        scanf("%s", Buffer);
        S2 = Buffer;
        vector<bool> A1 = Stat(S1);
        vector<bool> A2 = Stat(S2);
        bool answer = false;
        for (int i = 0; i < 26; ++i) {
            if (A1[i] && A2[i]) answer = true;
        }
        printf(answer ? "YES\n" : "NO\n");
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
