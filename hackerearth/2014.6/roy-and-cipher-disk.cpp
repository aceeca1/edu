#include <cstdio>
#include <string>
using namespace std;

char Buffer[101];

struct Solution {
    string S;
    char Last;

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        Last = 'a';
        bool head = true;
        for (int i = 0; i < S.size(); ++i) {
            int delta = (S[i] - Last + 26) % 26;
            if (13 < delta) delta -= 26;
            if (!head) putchar(' ');
            head = false;
            printf("%d", delta);
            Last = S[i];
        }
        printf("\n");
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
