#include <cstdio>
using namespace std;

struct Solution {
    int L, N, W, H;

    void Solve() {
        scanf("%d%d", &L, &N);
        for (int i = 0; i < N; ++i) {
            scanf("%d%d", &W, &H);
            if (W < L || H < L) {
                printf("UPLOAD ANOTHER\n");
            } else if (H == W) {
                printf("ACCEPTED\n");
            } else {
                printf("CROP IT\n");
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
