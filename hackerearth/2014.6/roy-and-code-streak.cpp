#include <cstdio>
#include <set>
using namespace std;

bool AssignMax(int *p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N, S, R;
    set<int> Solved;

    void Solve() {
        scanf("%d", &N);
        int current = 0, answer = 0;
        for (int i = 0; i < N; ++i) {
            scanf("%d%d", &S, &R);
            if (R == 0) {
                current = 0;
            } else {
                if (Solved.insert(S).second) ++current;
                AssignMax(&answer, current);
            }
        }
        printf("%d\n", answer);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
