#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[1000001];
char Vowel[6] = "aeiou";
int VowelNum[256];

typedef long long int64;
const int64 M = 1000000007;

int64 Sum(const vector<int>& a) {
    int64 answer = 0;
    for (int i = 0; i < a.size(); ++i) answer += a[i];
    return answer;
}

int64 Factorial(int n) {
    int64 answer = 1;
    for (int i = 2; i <= n; ++i) answer = answer * i % M;
    return answer;
}

int64 ModPow(int64 b, int e) {
    int64 answer = 1;
    while (e != 0) {
        if ((e & 1) != 0) answer = answer * b % M;
        b = b * b % M;
        e >>= 1;
    }
    return answer;
}

int64 ModDiv(int64 n, int64 d) {
    return n * ModPow(d, M - 2) % M;
}

int64 MultiChoose(int sumA, const vector<int>& a) {
    int64 nominator = Factorial(sumA), denominator = 1;
    for (int i = 0; i < a.size(); ++i) {
        denominator = denominator * Factorial(a[i]) % M;
    }
    return ModDiv(nominator, denominator);
}

struct Solution {
    string S;
    vector<int> A, B, C;

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        A.resize(6);
        for (int i = 0; i < S.size(); ++i) ++A[VowelNum[S[i]]];
        for (int i = 0; i < 5; ++i) A[i] = A[i + 1];
        A.pop_back();
        int sumA = Sum(A);
        int64 answerA = MultiChoose(sumA, A);
        B.resize(26);
        for (int i = 0; i < S.size(); ++i) ++B[S[i] - 'a'];
        for (int i = 0; Vowel[i]; ++i) B[Vowel[i] - 'a'] = 0;
        int sumB = Sum(B);
        int64 answerB = MultiChoose(sumB, B);
        if (sumA <= sumB + 1) {
            C.resize(2);
            C[0] = sumA;
            C[1] = sumB + 1 - sumA;
            int sumC = Sum(C);
            int64 answerC = MultiChoose(sumC, C);
            int64 answer = answerA * answerB % M * answerC % M;
            printf("%lld\n", answer);
        } else {
            printf("-1\n");
        }
    }
};

int main() {
    for (int i = 0; Vowel[i]; ++i) VowelNum[Vowel[i]] = i + 1;
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
