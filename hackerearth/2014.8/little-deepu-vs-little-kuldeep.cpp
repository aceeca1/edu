#include <cstdio>
using namespace std;

typedef long long int64;

const int64 M = 1000000007, N = 2000000;

int64 Factorial[N + 1];

int64 ModPow(int64 b, int e) {
    int64 answer = 1;
    while (e != 0) {
        if ((e & 1) != 0) answer = answer * b % M;
        b = b * b % M;
        e >>= 1;
    }
    return answer;
}

int64 ModDiv(int64 n, int64 d) {
    return n * ModPow(d, M - 2) % M;
}

int64 Choose(int n, int k) {
    return ModDiv(Factorial[n], Factorial[k] * Factorial[n - k] % M);
}

int64 Catalan(int n) {
    return ModDiv(Choose(2 * n, n), n + 1);
}

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        printf("%lld\n", Catalan(N));
    }
};

int main() {
    Factorial[1] = 1;
    for (int i = 2; i <= N; ++i) Factorial[i] = Factorial[i - 1] * i % M;
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
