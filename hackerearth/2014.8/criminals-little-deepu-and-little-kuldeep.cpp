#include <cstdio>
#include <map>
using namespace std;

typedef map<int, int>::iterator Type1;

bool AssignMax(int *p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N, X;
    map<int, int> A;

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            scanf("%d", &X);
            ++A[X];
        }
        int answer = 0;
        for (Type1 it = A.begin(); it != A.end(); ++it) {
            AssignMax(&answer, it->second);
        }
        printf("%d\n", answer);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
