#include <algorithm>
#include <cstdio>
using namespace std;

typedef long long int64;

template <class Predicate>
int64 BinarySearch(int64 begin, int64 end, const Predicate& p) {
    while (begin != end) {
        int64 mid = begin + (end - begin) / 2;
        if (p.Test(mid)) {
            end = mid;
        } else {
            begin = mid + 1;
        }
    }
    return begin;
}

int GCD(int a, int b) {
    if (b == 0) return a;
    return GCD(b, a % b);
}

struct Solution {
    int A, B, C, N;

    bool Test(int64 x) const {
        return N <= x / A + x / B - x / C;
    }

    void Solve() {
        scanf("%d%d%d", &A, &B, &N);
        C = A * B / GCD(A, B);
        int64 maxAnswer = min(A, B) * int64(N);
        int64 answer = BinarySearch(0, maxAnswer, *this);
        printf("%lld\n", answer);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
