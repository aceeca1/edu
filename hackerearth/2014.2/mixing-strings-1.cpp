#include <cstdio>
#include <climits>
#include <string>
#include <vector>
using namespace std;

char Buffer[16];

struct Solution {
    int N, Answer;
    vector<string> S;
    string Prefix;

    void Search() {
        if (Answer < Prefix.size()) return;
        if (S.size() == 0) {
            Answer = Prefix.size();
            return;
        }
        for (int i = 0; i < S.size(); ++i) {
            string SI = S[i];
            S[i] = S.back();
            S.pop_back();
            int prefixSize = Prefix.size();
            for (int i = min(Prefix.size(), SI.size()); i >= 0; --i) {
                string s1 = Prefix.substr(Prefix.size() - i, i);
                string s2 = SI.substr(0, i);
                if (s1 == s2) {
                    Prefix += SI.substr(i);
                    Search();
                    Prefix.resize(prefixSize);
                }
            }
            S.push_back(i == S.size() ? "" : S[i]);
            S[i] = SI;
        }
    }

    void Solve() {
        scanf("%d", &N);
        S.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%s", Buffer);
            S[i] = Buffer;
        }
        Answer = INT_MAX;
        Search();
        printf("%d\n", Answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
