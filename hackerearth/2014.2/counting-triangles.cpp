#include <cstdio>
#include <map>
using namespace std;

typedef unsigned long long uint64;
typedef map<uint64, int>::iterator Type1;

uint64 Fingerprint(uint64 x) {
    const uint64 kMul = 0x9ddfea08eb382d69ULL;
    x *= kMul, x ^= x >> 47;
    x *= kMul, x ^= x >> 47;
    x *= kMul, x ^= x >> 47;
    return x * kMul;
}

struct Solution {
    int N;
    map<uint64, int> Map;

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            int a, b, c;
            scanf("%d%d%d", &a, &b, &c);
            uint64 hash = Fingerprint(a) + Fingerprint(b) + Fingerprint(c);
            ++Map[hash];
        }
        int answer = 0;
        for (Type1 it = Map.begin(); it != Map.end(); ++it) {
            if (it->second == 1) ++answer;
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
