#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[51];

struct Solution {
    int N;
    vector<string> Board;

    bool SymmetricH() {
        return std::equal(Board.begin(), Board.end(), Board.rbegin());
    }

    bool SymmetricV() {
        for (int i = 0; i < Board.size(); ++i) {
            if (!std::equal(Board[i].begin(), Board[i].end(),
                Board[i].rbegin())) return false;
        }
        return true;
    }

    void Solve() {
        scanf("%d", &N);
        Board.resize(N);
        for (int i = 0; i < Board.size(); ++i) {
            scanf("%s", Buffer);
            Board[i] = Buffer;
        }
        if (SymmetricH()) {
            printf(SymmetricV() ? "BOTH\n" : "HORIZONTAL\n");
        } else {
            printf(SymmetricV() ? "VERTICAL\n" : "NO\n");
        }
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
