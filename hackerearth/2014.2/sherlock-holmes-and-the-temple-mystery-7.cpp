#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> A;

    bool Divide() {
        int sum = 0;
        for (int i = 0; i < N; ++i) sum += A[i];
        if (sum % 2 != 0) return false;
        int target = sum / 2;
        vector<bool> can(target + 1);
        can[0] = true;
        for (int i = 0; i < N; ++i) {
            for (int j = target; j >= A[i]; --j) {
                can[j] = can[j] | can[j - A[i]];
            }
        }
        return can[target];
    }

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        printf("%s\n", Divide() ? "YES" : "NO");
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
