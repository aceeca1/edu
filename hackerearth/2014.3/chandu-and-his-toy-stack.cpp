#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int N;
    int64 X, Y;
    vector<int64> A, B;

    void Solve() {
        scanf("%d%lld%lld", &N, &X, &Y);
        A.resize(N);
        B.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%lld%lld", &A[i], &B[i]);
        }
        sort(A.begin(), A.end());
        sort(B.begin(), B.end());
        int64 answer = 0;
        for (int i = 0; i < N; ++i) {
            if (A[i] < B[i]) {
                answer += X * (B[i] - A[i]);
            } else {
                answer += Y * (A[i] - B[i]);
            }
        }
        printf("%lld\n", answer);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
