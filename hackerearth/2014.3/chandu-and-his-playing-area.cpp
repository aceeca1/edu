#include <cstdio>
using namespace std;

typedef long long int64;

struct Solution {
    int64 N;

    void Solve() {
        scanf("%lld", &N);
        int64 xy = N / 2;
        int64 x = xy / 2;
        int64 y = xy - x;
        printf("%lld\n", x * y);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
