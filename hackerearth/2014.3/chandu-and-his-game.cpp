#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int N, K;
    vector<int> X, Y;

    void Solve() {
        scanf("%d%d", &N, &K);
        X.resize(K);
        for (int i = 0; i < K; ++i) scanf("%d", &X[i]);
        Y.resize(K);
        for (int i = 0; i < K; ++i) scanf("%d", &Y[i]);
        int64 answer = 0;
        for (int i = 0; i < K; ++i) {
            answer += min(N - X[i], X[i] - 1);
            answer += min(N - Y[i], Y[i] - 1);
        }
        printf("%lld\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
