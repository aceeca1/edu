#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

int64 GCD(int64 a, int64 b) {
    if (b == 0) return a;
    return GCD(b, a % b);
}

struct Query {
    int N, M;
};

struct Solution {
    int T, MaxN;
    vector<Query> Queries;
    vector<vector<int64> > F;

    void Solve() {
        scanf("%d", &T);
        Queries.resize(T);
        MaxN = 0;
        for (int i = 0; i < T; ++i) {
            scanf("%d%d", &Queries[i].N, &Queries[i].M);
            AssignMax(&MaxN, Queries[i].N);
        }
        F.resize(MaxN + 1);
        F[1].resize(2);
        F[1][1] = 1;
        for (int i = 2; i <= MaxN; ++i) {
            F[i].resize(i + 1);
            for (int j = 1; j <= i; ++j) {
                int64 f1 = 1 < j ? F[i - 1][j - 1] : 0;
                int64 f2 = (i - 1) * (j <= i - 1 ? F[i - 1][j] : 0);
                F[i][j] = f1 + f2;
            }
        }
        for (int i = 0; i < T; ++i) {
            int64 hitted = 0, total = 0;
            for (int j = 1; j <= Queries[i].M; ++j) {
                hitted += F[Queries[i].N][j];
            }
            for (int j = 1; j <= Queries[i].N; ++j) {
                total += F[Queries[i].N][j];
            }
            int64 gcd = GCD(hitted, total);
            printf("%lld/%lld\n", hitted / gcd, total / gcd);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
