#include <cstdio>
using namespace std;

typedef long long int64;
const int64 M = 1000000007;

struct Solution {
    int64 N;

    void Solve() {
        scanf("%d", &N);
        int64 minX = 0;
        if (N % 2 == 0) {
            int64 k = N - 1;
            int64 k1 = k / 2;
            int64 k2 = k - k1;
            minX = ((k1 * k1 + k2 * k2) % M * (N / 2) % M) % M;
        } else {
            int64 k = (N - 1) / 2;
            minX = (k * k) % M * N % M;
        }
        int64 maxX = N * (N - 1) % M * (N + N - 1) % M * 166666668 % M;
        printf("%lld %lld\n", minX, maxX);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
