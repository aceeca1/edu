#include <algorithm>
#include <climits>
#include <cstdio>
#include <vector>
using namespace std;

bool AssignMin(int* p, int v) {
    if (v < *p) return *p = v, true;
    return false;
}

struct Solution {
    int N, M, Q;
    vector<vector<int> > Require;

    void Solve() {
        scanf("%d%d%d", &N, &M, &Q);
        Require.resize(N);
        for (int i = 0; i < N; ++i) {
            Require[i].resize(M);
            for (int j = 0; j < M; ++j) {
                scanf("%d", &Require[i][j]);
            }
        }
        for (int i = 0; i < Q; ++i) {
            int answer = INT_MAX;
            for (int j = 0; j < N; ++j) {
                int x;
                scanf("%d", &x);
                int level = upper_bound(Require[j].begin(), Require[j].end(),
                    x) - Require[j].begin();
                AssignMin(&answer, level);
            }
            printf("%d\n", answer);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
