#include <cstdio>
#include <vector>
using namespace std;

const int M = 100;
vector<vector<vector<bool> > > Ashima;

struct Solution {
    int P1, P2, P3;

    void Solve() {
        scanf("%d%d%d", &P1, &P2, &P3);
        printf(Ashima[P1][P2][P3] ? "Ashima\n" : "Aishwarya\n");
    }
};

bool Compute(int p1, int p2, int p3) {
    if (p1 && !Ashima[p1 - 1][p2][p3]) return true;
    if (p2 && !Ashima[p1][p2 - 1][p3]) return true;
    if (p3 && !Ashima[p1][p2][p3 - 1]) return true;
    if (p1 && p2 && !Ashima[p1 - 1][p2 - 1][p3]) return true;
    if (p1 && p3 && !Ashima[p1 - 1][p2][p3 - 1]) return true;
    if (p2 && p3 && !Ashima[p1][p2 - 1][p3 - 1]) return true;
    if (p1 && p2 && p3 && !Ashima[p1 - 1][p2 - 1][p3 - 1]) return true;
    return false;
}

int main() {
    Ashima.resize(M + 1);
    for (int i = 0; i <= M; ++i) {
        Ashima[i].resize(M + 1);
        for (int j = 0; j <= M; ++j) {
            Ashima[i][j].resize(M + 1);
            for (int k = 0; k <= M; ++k) {
                Ashima[i][j][k] = Compute(i, j, k);
            }
        }
    }
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
