#include <algorithm>
#include <cstdio>
#include <climits>
#include <vector>
using namespace std;

typedef long long int64;

struct Army {
    int64 Begin, End;

    bool operator<(const Army& that) {
        return Begin < that.Begin || Begin == that.Begin && End < that.End;
    }
};

struct Solution {
    int N;
    int64 S, E, X, P;
    vector<Army> Armies;

    void Solve() {
        scanf("%d%lld%lld", &N, &S, &E);
        Armies.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%lld%lld", &X, &P);
            Armies[i].Begin = max(X - P, S);
            Armies[i].End = min(X + P, E);
        }
        sort(Armies.begin(), Armies.end());
        int64 current = LONG_LONG_MIN, answer = 0;
        for (int i = 0; i < Armies.size(); ++i) {
            int64 left = max(current, Armies[i].Begin);
            int64 right = max(left, Armies[i].End);
            answer += right - left;
            current = right;
        }
        printf("%lld\n", E - S - answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
