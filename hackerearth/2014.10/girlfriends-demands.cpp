#include <cstdio>
#include <string>
using namespace std;

typedef long long int64;

char Buffer[100001];

struct Solution {
    string S;
    int Q;
    int64 A, B;

    void Solve() {
        scanf("%s%d", Buffer, &Q);
        S = Buffer;
        for (int i = 0; i < Q; ++i) {
            scanf("%lld%lld", &A, &B);
            A = (A - 1) % S.size();
            B = (B - 1) % S.size();
            if (S[A] == S[B]) {
                printf("Yes\n");
            } else {
                printf("No\n");
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
