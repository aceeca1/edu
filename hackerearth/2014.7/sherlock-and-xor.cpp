#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int N;
    vector<int> A;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        int64 odd = 0, even = 0;
        for (int i = 0; i < N; ++i) {
            if (A[i] % 2 != 0) {
                ++odd;
            } else {
                ++even;
            }
        }
        printf("%lld\n", odd * even);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
