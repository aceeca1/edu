#include <cstdio>
#include <map>
#include <string>
using namespace std;

char Buffer[80];
string Month[12] = {
    "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
};
int Dates[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
map<string, string> PreviousMonth;
map<string, int> MonthDate;

bool Leap(int year) {
    if (year % 400 == 0) return true;
    if (year % 100 == 0) return false;
    return year % 4 == 0;
}

int GetDates(const string& month, int year) {
    if (month == "February") return Leap(year) ? 29 : 28;
    return MonthDate[month];
}

struct Solution {
    int D, Y;
    string M;

    void Solve() {
        scanf("%d%s%d", &D, Buffer, &Y);
        M = Buffer;
        if (D != 1) {
            --D;
        } else if (M != "January") {
            M = PreviousMonth[M];
            D = GetDates(M, Y);
        } else {
            --Y;
            M = "December";
            D = GetDates(M, Y);
        }
        printf("%d %s %d\n", D, M.c_str(), Y);
    }
};

int main() {
    for (int i = 0; i < 11; ++i) PreviousMonth[Month[i + 1]] = Month[i];
    for (int i = 0; i < 12; ++i) MonthDate[Month[i]] = Dates[i];
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
