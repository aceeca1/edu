#include <cstdio>
#include <map>
#include <vector>
using namespace std;

struct Criminal {
    int X, Y;
    char Z;

    bool operator<(const Criminal& that) const {
        return X < that.X || X == that.X && Y < that.Y;
    }
};

typedef map<Criminal, int>::iterator Type1;
typedef pair<Type1, bool> Type2;

struct Solution {
    int N;
    vector<Criminal> Criminals;

    void Go(Criminal* c) {
        switch (c->Z) {
            case 'N': ++c->Y; return;
            case 'E': ++c->X; return;
            case 'W': --c->X; return;
            case 'S': --c->Y; return;
        }
    }

    void GoAndDie() {
        map<Criminal, int> next;
        for (int i = 0; i < Criminals.size(); ++i) {
            Go(&Criminals[i]);
            ++next[Criminals[i]];
        }
        Criminals.clear();
        for (Type1 it = next.begin(); it != next.end(); ++it) {
            if (it->second == 1) Criminals.push_back(it->first);
        }
    }

    void Solve() {
        scanf("%d", &N);
        Criminals.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%d %d %c",
                &Criminals[i].X, &Criminals[i].Y, &Criminals[i].Z);
            Criminals[i].X <<= 1;
            Criminals[i].Y <<= 1;
        }
        for (int i = 0; i <= 4000; ++i) {
            GoAndDie();
        }
        printf("%d\n", Criminals.size());
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
