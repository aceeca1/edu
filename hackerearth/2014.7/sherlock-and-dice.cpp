#include <cstdio>
#include <vector>
using namespace std;

char Buffer[80];

struct Solution {
    int M, N, K;
    vector<vector<double> > F;

    void Solve() {
        scanf("%d%d%d", &M, &N, &K);
        F.resize(M + 1);
        for (int i = 0; i <= M; ++i) {
            F[i].resize(K + 1);
            for (int j = 0; j <= K; ++j) {
                if (i == 0) {
                    F[i][j] = j == 0 ? 1.0 : 0.0;
                } else {
                    double result = 0.0;
                    for (int k = 1; k <= N; ++k) {
                        if (j < k) break;
                        result += F[i - 1][j - k];
                    }
                    F[i][j] = result / N;
                }
            }
        }
        sprintf(Buffer, "%.3e", F[M][K]);
        int e = 0;
        while (Buffer[e] != 'e') ++e;
        Buffer[e] = 0;
        sscanf(Buffer + e + 1, "%d", &e);
        printf("%s %d\n", Buffer, -e);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
