#include <cstdio>
using namespace std;

typedef long long int64;

int64 ModPow(int64 a, int64 b, int64 m) {
    int64 answer = 1;
    while (b != 0) {
        if ((b & 1) != 0) answer = answer * a % m;
        a = a * a % m;
        b >>= 1;
    }
    return answer;
}

int64 ModDiv(int64 a, int64 b, int64 m) {
    return a * ModPow(b, m - 2, m) % m;
}

struct Solution {
    int64 N, P;

    int64 Nominator() {
        if (P <= 3 * N) return 0;
        int64 a = 1;
        for (int i = P - 1; i > 3 * N; --i) a = a * i % P;
        return ModDiv(P - 1, a, P);
    }

    int64 Denominator() {
        return ModPow(6, N, P);
    }

    void Solve() {
        scanf("%lld%lld", &N, &P);
        if (P == 1) {
            printf("0\n");
        } else if (P == 2) {
            printf(N == 1 ? "1\n" : "0\n");
        } else if (P == 3) {
            printf(N == 1 ? "1\n" : N == 2 ? "2\n" : "0\n");
        } else {
            printf("%lld\n", ModDiv(Nominator(), Denominator(), P));
        }
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
