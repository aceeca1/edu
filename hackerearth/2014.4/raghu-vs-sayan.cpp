#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int A, B, N;
    vector<int> Dish;

    int Eat(int total) {
        for (int i = 0; i < Dish.size(); ++i) {
            if (total < Dish[i]) return i;
            total -= Dish[i];
        }
        return Dish.size();
    }

    void Solve() {
        scanf("%d%d%d", &A, &B, &N);
        Dish.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &Dish[i]);
        sort(Dish.begin(), Dish.end());
        int eatA = Eat(A), eatB = Eat(B);
        if (eatA < eatB) {
            printf("Sayan Won\n");
        } else if (eatB < eatA) {
            printf("Raghu Won\n");
        } else {
            printf("Tie\n");
        }
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
