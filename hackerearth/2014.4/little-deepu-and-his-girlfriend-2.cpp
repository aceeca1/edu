#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int M, N;
    vector<int> S;
    vector<bool> DeepuWin;

    void Solve() {
        scanf("%d%d", &M, &N);
        S.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &S[i]);
        DeepuWin.resize(M + 1);
        for (int i = 1; i <= M; ++i) {
            for (int j = 0; j < N; ++j) {
                if (S[j] <= i && !DeepuWin[i - S[j]]) {
                    DeepuWin[i] = true;
                    break;
                }
            }
        }
        printf("%s\n", DeepuWin[M] ? "Little Deepu" : "Kate");
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
