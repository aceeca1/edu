#include <cstdio>
using namespace std;

typedef long long int64;

int64 ExGCD(int64 a, int64 b, int64* ax, int64* bx) {
    if (b == 0) return *ax = 1, *bx = 0, a;
    int64 answer = ExGCD(b, a % b, bx, ax);
    return *bx -= a / b * (*ax), answer;
}

struct Solution {
    int64 A, B;

    void Solve() {
        scanf("%lld%lld", &A, &B);
        int64 x, y;
        ExGCD(A, B, &x, &y);
        printf("%lld\n", (x % B + B) % B);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
