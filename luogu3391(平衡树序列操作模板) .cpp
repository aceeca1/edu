#include <climits>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;

long long Fingerprint(long long x) {
    const long long kMul = 0x9ddfea08eb382d69ULL;
    x *= kMul, x ^= x >> 47;
    x *= kMul, x ^= x >> 47;
    x *= kMul, x ^= x >> 47;
    return x * kMul;
}

long long Random() {
    static long long Seed = 2;
    return Fingerprint(Seed++) & LONG_LONG_MAX;
}

struct RBST {
    RBST *ChildL, *ChildR;
    int Size;
    long long Data;
    bool NeedReverse;

    RBST() { ChildL = ChildR = 0; NeedReverse = false; }
};

RBST* Push(RBST* root) {
    if (root->NeedReverse) {
        swap(root->ChildL, root->ChildR);
        if (root->ChildL) root->ChildL->NeedReverse ^= 1;
        if (root->ChildR) root->ChildR->NeedReverse ^= 1;
        root->NeedReverse = false;
    }
    return root;
}

int GetSize(const RBST* root) { return root ? root->Size : 0; }

RBST* SetSize(RBST* root) {
    root->Size = GetSize(root->ChildL) + GetSize(root->ChildR) + 1;
    return root;
}

void SplitAt(RBST* root, int index, RBST** treeL, RBST** treeR) {
    if (!root) {
        *treeL = *treeR = 0;
    } else {
        int sizeL = GetSize(Push(root)->ChildL);
        if (index <= sizeL) {
            SplitAt(root->ChildL, index, treeL, &root->ChildL);
            *treeR = SetSize(root);
        } else {
            SplitAt(root->ChildR, index - sizeL - 1, &root->ChildR, treeR);
            *treeL = SetSize(root);
        }
    }
}

RBST* Join(RBST* treeL, RBST* treeR) {
    int sizeL = GetSize(treeL);
    int sizeR = GetSize(treeR);
    int size = sizeL + sizeR;
    if (size == 0) return 0;
    if (Random() % size < sizeL) {
        treeL->ChildR = Join(Push(treeL)->ChildR, treeR);
        return SetSize(treeL);
    } else {
        treeR->ChildL = Join(treeL, Push(treeR)->ChildL);
        return SetSize(treeR);
    }
}

RBST* InsertAsRoot(RBST* root, int index, long long item) {
    RBST *newRoot = new RBST;
    newRoot->Data = item;
    SplitAt(root, index, &newRoot->ChildL, &newRoot->ChildR);
    return SetSize(newRoot);
}

RBST* Insert(RBST* root, int index, long long item) {
    if (Random() % (GetSize(root) + 1) == 0) {
        return InsertAsRoot(root, index, item);
    } else {
        int sizeL = GetSize(Push(root)->ChildL);
        if (index <= sizeL) {
            root->ChildL = Insert(root->ChildL, index, item);
        } else {
            root->ChildR = Insert(root->ChildR, index - sizeL - 1, item);
        }
        return SetSize(root);
    }
}

void Clear(RBST* root) {
    if (root->ChildL) Clear(root->ChildL);
    if (root->ChildR) Clear(root->ChildR);
    delete root;
}

struct Solution {
    int N, M;
    RBST *Tree;
    bool Head;

    void Print(RBST* root) {
        if (!root) return;
        Print(Push(root)->ChildL);
        if (!Head) putchar(' ');
        Head = false;
        printf("%lld", root->Data);
        Print(root->ChildR);
    }

    void Solve() {
        scanf("%d%d", &N, &M);
        for (int i = 1; i <= N; ++i) Tree = Insert(Tree, GetSize(Tree), i);
        for (int i = 0; i < M; ++i) {
            int left, right;
            scanf("%d%d", &left, &right);
            --left;
            RBST *tree1, *tree2, *tree3;
            SplitAt(Tree, left, &tree1, &tree2);
            SplitAt(tree2, right - left, &tree2, &tree3);
            tree2->NeedReverse ^= 1;
            Tree = Join(tree1, Join(tree2, tree3));
        }
        Head = true;
        Print(Tree);
        printf("\n");
        Clear(Tree);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
