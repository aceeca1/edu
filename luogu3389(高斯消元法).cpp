#include <cstdio>
#include <valarray>
#include <vector>
using namespace std;

bool AssignMax(double* p, double v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N;
    vector<vector<double> > A;
    vector<double> X;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) {
            A[i].resize(N + 1);
            for (int j = 0; j <= N; ++j) {
                scanf("%lf", &A[i][j]);
            }
        }
        X.resize(N);
        if (Gauss()) {
            for (int i = 0; i < N; ++i) printf("%.2lf\n", X[i]);
        } else {
            printf("No Solution\n");
        }
    }

    int GetPivotIndex(const vector<double>& a) {
        double max = 0.0;
        int answer = 0;
        for (int i = 0; i < N; ++i) {
            if (AssignMax(&max, abs(a[i]))) answer = i;
        }
        return answer;
    }

    bool Gauss() {
        for (int i = 0; i < N; ++i) X[i] = -1;
        for (int i = 0; i < N; ++i) {
            int index = GetPivotIndex(A[i]);
            if (abs(A[i][index]) < 1e-12) {
                if (abs(A[i][N]) < 1e-12) continue;
                return false;
            }
            double m = 1.0 / A[i][index];
            for (int j = 0; j <= N; ++j) A[i][j] *= m;
            for (int j = 0; j < N; ++j) {
                if (j == i) continue;
                double m1 = A[j][index];
                for (int k = 0; k <= N; ++k) A[j][k] -= A[i][k] * m1;
            }
            X[index] = i;
        }
        for (int i = 0; i < N; ++i) {
            if (X[i] == -1) return false;
        }
        for (int i = 0; i < N; ++i) X[i] = A[X[i]][N];
        return true;
    }
};

int main() {
    Solution().Solve();
    return 0;
}
