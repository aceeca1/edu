#include <cstdio>
#include <map>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int N, K;
    vector<int> A, B1, B2;
    map<int64, int> M1, M2;

    void Solve() {
        scanf("%d%d", &N, &K);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        B1.resize(N);
        for (int i = 0; i < N; ++i) {
            if (A[i] % K == 0) {
                B1[i] = M1[A[i] / K];
            } else {
                B1[i] = 0;
            }
            ++M1[A[i]];
        }
        M1.clear();
        B2.resize(N);
        for (int i = N - 1; i >= 0; --i) {
            B2[i] = M2[A[i] * int64(K)];
            ++M2[A[i]];
        }
        M2.clear();
        int64 answer = 0;
        for (int i = 0; i < N; ++i) answer += int64(B1[i]) * B2[i];
        printf("%lld\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
