#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> A, B1, B2;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        long long sum = 0, prefixSum = 0, suffixSum = 0;
        for (int i = 0; i < N; ++i) {
            scanf("%d", &A[i]);
            sum += A[i];
        }
        if (sum % 3 != 0) {
            printf("0\n");
            return;
        }
        B1.resize(N);
        for (int i = 0; i < N; ++i) {
            prefixSum += A[i];
            B1[i] = prefixSum == sum / 3;
        }
        B2.resize(N);
        for (int i = N - 1; i >= 0; --i) {
            suffixSum += A[i];
            B2[i] = suffixSum == sum / 3;
        }
        for (int i = 1; i < N; ++i) B1[i] += B1[i - 1];
        long long answer = 0;
        for (int i = 2; i < N; ++i) {
            answer += B1[i - 2] * B2[i];
        }
        printf("%lld\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
