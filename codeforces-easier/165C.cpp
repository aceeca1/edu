#include <cstdio>
#include <string>
#include <vector>
using namespace std;

typedef long long int64;

char Buffer[1000001];

struct Solution {
    int N;
    string S;
    vector<int> A;

    void Solve() {
        scanf("%d%s", &N, Buffer);
        S = Buffer;
        A.push_back(0);
        for (int i = 0; i < S.size(); ++i) {
            if (S[i] == '0') {
                ++A.back();
            } else {
                A.push_back(0);
            }
        }
        int64 answer = 0;
        if (N == 0) {
            for (int i = 0; i < A.size(); ++i) {
                answer += int64(A[i] + 1) * A[i] / 2;
            }
        } else {
            for (int i = 0; i + N < A.size(); ++i) {
                answer += int64(A[i] + 1) * (A[i + N] + 1);
            }
        }
        printf("%lld\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
