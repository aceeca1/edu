#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, M;
    vector<vector<int> > Out;
    vector<bool> Visited;
    vector<int> CurrentComponent;

    void DFS(int no) {
        Visited[no] = true;
        CurrentComponent.push_back(no);
        for (int i = 0; i < Out[no].size(); ++i) {
            int target = Out[no][i];
            if (!Visited[target]) DFS(target);
        }
    }

    bool IsCycle() {
        for (int i = 0; i < CurrentComponent.size(); ++i) {
            if (Out[CurrentComponent[i]].size() != 2) return false;
        }
        return true;
    }

    void Solve() {
        scanf("%d%d", &N, &M);
        Out.resize(N);
        for (int i = 0; i < M; ++i) {
            int v1, v2;
            scanf("%d%d", &v1, &v2);
            Out[v1 - 1].push_back(v2 - 1);
            Out[v2 - 1].push_back(v1 - 1);
        }
        Visited.resize(N);
        int answer = 0;
        for (int i = 0; i < N; ++i) {
            if (!Visited[i]) {
                DFS(i);
                if (IsCycle()) ++answer;
                CurrentComponent.clear();
            }
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
