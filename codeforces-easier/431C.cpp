#include <cstdio>
#include <vector>
using namespace std;

const int M = 1000000007;

struct Solution {
    int N, K, D;
    vector<long long> F, G;

    void Solve() {
        scanf("%d%d%d", &N, &K, &D);
        F.resize(N + 1);
        G.resize(N + 1);
        F[0] = 0;
        G[0] = 1;
        for (int i = 1; i <= N; ++i) {
            long long g1 = i - K >= 1 ? G[i - K - 1] : 0;
            long long g2 = i >= D ? G[i - D] : 0;
            long long f2 = i >= D ? F[i - D] : 0L;
            long long gi = (G[i - 1] + (G[i - 1] - g1)) % M;
            G[i] = (gi + M) % M;
            long long fi = (F[i - 1] + (F[i - 1] - f2) + (g2 - g1)) % M;
            F[i] = (fi + M) % M;
        }
        long long answer = (F[N] - F[N - 1]) % M;
        printf("%lld\n", (answer + M) % M);
    }

};

int main() {
    Solution().Solve();
    return 0;
}
