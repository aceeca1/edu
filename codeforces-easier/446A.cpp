#include <cstdio>
#include <vector>
using namespace std;

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

bool AssignMin(int* p, int v) {
    if (v < *p) return *p = v, true;
    return false;
}

struct Solution {
    int N;
    vector<int> A;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        int f0 = 1, f1 = A[1] > A[0] ? 2 : 1;
        int g0 = 1, g1 = 2;
        int answer = 2;
        for (int i = 2; i < N; ++i) {
            int f2 = 1, g2 = 1;
            if (A[i - 1] < A[i]) f2 = f1 + 1;
            if (A[i - 1] < A[i]) g2 = g1 + 1;
            if (A[i - 2] + 1 < A[i]) AssignMax(&g2, f0 + 2);
            AssignMax(&answer, f2 + 1);
            AssignMax(&answer, g2);
            f0 = f1, f1 = f2;
            g0 = g1, g1 = g2;
        }
        AssignMin(&answer, N);
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
