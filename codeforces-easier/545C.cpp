#include <cstdio>
#include <climits>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> X, H;

    void Solve() {
        scanf("%d", &N);
        X.resize(N);
        H.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d%d", &X[i], &H[i]);
        int last = INT_MIN;
        int answer = 0;
        for (int i = 0; i < N; ++i) {
            int next = i == N - 1 ? INT_MAX : X[i + 1];
            if (last < X[i] - H[i]) {
                ++answer;
                last = X[i];
            } else if (X[i] + H[i] < next) {
                ++answer;
                last = X[i] + H[i];
            } else {
                last = X[i];
            }
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
