#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, M;
    vector<vector<int> > Out;
    vector<bool> Visited;

    void DFS(int no) {
        Visited[no] = true;
        for (int i = 0; i < Out[no].size(); ++i) {
            int target = Out[no][i];
            if (!Visited[target]) DFS(target);
        }
    }

    void Solve() {
        scanf("%d%d", &N, &M);
        Out.resize(N);
        for (int i = 0; i < M; ++i) {
            int v1, v2;
            scanf("%d%d", &v1, &v2);
            Out[v1 - 1].push_back(v2 - 1);
            Out[v2 - 1].push_back(v1 - 1);
        }
        Visited.resize(N);
        int answer = 0;
        for (int i = 0; i < N; ++i) {
            if (!Visited[i]) {
                ++answer;
                DFS(i);
            }
        }
        printf("%lld\n", 1LL << (N - answer));
    }
};

int main() {
    Solution().Solve();
    return 0;
}
