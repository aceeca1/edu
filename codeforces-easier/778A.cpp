#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[200001];

struct Solution {
    string S, S1;
    vector<int> P;

    bool OK(int k) {
        vector<bool> Removed(S.size());
        for (int i = 0; i < k; ++i) Removed[P[i]] = true;
        int p = 0;
        for (int i = 0; i < S.size(); ++i) {
            if (Removed[i]) continue;
            if (S[i] == S1[p]) ++p;
            if (p == S1.size()) return true;
        }
        return false;
    }

    int BinarySearch(int lower, int upper) {
        while (lower < upper) {
            int mid = lower + (upper - lower) / 2;
            if (OK(mid)) {
                lower = mid + 1;
            } else {
                upper = mid;
            }
        }
        return lower;
    }

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        scanf("%s", Buffer);
        S1 = Buffer;
        P.resize(S.size());
        for (int i = 0; i < P.size(); ++i) {
            scanf("%d", &P[i]);
            --P[i];
        }
        int maxOK = BinarySearch(0, P.size() + 1) - 1;
        printf("%d\n", maxOK);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
