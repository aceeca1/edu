#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> A, B;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        B.resize(1 + *max_element(A.begin(), A.end()));
        for (int i = 0; i < N; ++i) ++B[A[i]];
        vector<long long> f(B.size());
        f[0] = 0;
        for (int i = 1; i < f.size(); ++i) {
            long long f1 = 2 <= i ? f[i - 2] : 0;
            f[i] = max(f[i - 1], f1 + (long long)B[i] * i);
        }
        printf("%lld\n", f.back());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
