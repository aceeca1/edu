#include <cstdio>
using namespace std;

struct Solution {
    int N, M, X;

    void Solve() {
        scanf("%d%d", &N, &M);
        double answer = M;
        for (int i = 0; i < N + N; ++i) {
            scanf("%d", &X);
            answer *= X / double(X - 1);
        }
        if (answer > 1e10) {
            printf("-1\n");
        } else {
            printf("%.12lf\n", answer - M);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
