#include <algorithm>
#include <cstdio>
#include <climits>
#include <queue>
#include <vector>
using namespace std;

struct Solution {
    int N, M;
    vector<vector<bool> > Railway;

    int BFS(int start, int end, bool rail) {
        vector<int> distance(N);
        for (int i = 0; i < N; ++i) distance[i] = INT_MAX;
        distance[start] = 0;
        queue<int> q;
        q.push(start);
        while (!q.empty()) {
            int front = q.front();
            q.pop();
            for (int i = 0; i < N; ++i) {
                if (Railway[front][i] != rail) continue;
                if (distance[i] != INT_MAX) continue;
                distance[i] = distance[front] + 1;
                q.push(i);
            }
        }
        return distance[end];
    }

    void Solve() {
        scanf("%d%d", &N, &M);
        Railway.resize(N);
        for (int i = 0; i < N; ++i) Railway[i].resize(N);
        for (int i = 0; i < M; ++i) {
            int v1, v2;
            scanf("%d%d", &v1, &v2);
            Railway[v1 - 1][v2 - 1] = true;
            Railway[v2 - 1][v1 - 1] = true;
        }
        int answer1 = BFS(0, N - 1, false);
        int answer2 = BFS(0, N - 1, true);
        int answer = max(answer1, answer2);
        printf("%d\n", answer == INT_MAX ? -1 : answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
