#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

const int M = 1000000007;

struct Solution {
    int T, K;
    vector<int> A, B;

    void Solve() {
        scanf("%d%d", &T, &K);
        A.resize(T);
        B.resize(T);
        for (int i = 0; i < T; ++i) {
            scanf("%d%d", &A[i], &B[i]);
        }
        int m = *max_element(B.begin(), B.end());
        vector<int> f(m + 1);
        f[0] = 1;
        for (int i = 1; i <= m; ++i) {
            int f1 = K <= i ? f[i - K] : 0;
            f[i] = (f[i - 1] + f1) % M;
        }
        for (int i = 1; i <= m; ++i) f[i] = (f[i] + f[i - 1]) % M;
        for (int i = 0; i < T; ++i) {
            printf("%d\n", (f[B[i]] + M - f[A[i] - 1]) % M);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
