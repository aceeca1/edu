#include <cstdio>
using namespace std;

typedef long long int64;

const int M = 1000000007;

int PowerMod(int a, int b, int m) {
    int answer = 1;
    while (b != 0) {
        if ((b & 1) != 0) answer = int64(answer) * a % m;
        a = int64(a) * a % m;
        b >>= 1;
    }
    return answer;
}

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        int a1 = PowerMod(27, N, M);
        int a2 = PowerMod(7, N, M);
        printf("%d\n", (a1 + M - a2) % M);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
