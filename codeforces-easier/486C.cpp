#include <algorithm>
#include <cstdio>
#include <string>
using namespace std;

char Buffer[100001];

int Difference(char c1, char c2) {
    int d1 = (c1 - c2 + 26) % 26;
    int d2 = (c2 - c1 + 26) % 26;
    return min(d1, d2);
}

struct ToPalindrome {
    string S;
    int N, P, Answer;

    void GoLeft() {
        int target = 0;
        while (target <= P && S[target] == S[N - 1 - target]) ++target;
        while (true) {
            Answer += Difference(S[P], S[N - 1 - P]);
            S[P] = S[N - 1 - P];
            if (P <= target) break;
            ++Answer;
            --P;
        }
    }

    void GoRight() {
        int target = N / 2 - 1;
        while (P <= target && S[target] == S[N - 1 - target]) --target;
        while (true) {
            Answer += Difference(S[P], S[N - 1 - P]);
            S[P] = S[N - 1 - P];
            if (target <= P) break;
            ++Answer;
            ++P;
        }
    }

    int GetAnswer1() {
        GoRight();
        GoLeft();
        return Answer;
    }

    int GetAnswer2() {
        GoLeft();
        GoRight();
        return Answer;
    }
};

struct Solution {
    int N, P;
    string S;

    void Solve() {
        scanf("%d%d%s", &N, &P, Buffer), --P;
        S = Buffer;
        if (N / 2 < P) {
            P = N - 1 - P;
            reverse(S.begin(), S.end());
        }
        ToPalindrome to1 = {S, N, P, 0};
        ToPalindrome to2 = {S, N, P, 0};
        printf("%d\n", min(to1.GetAnswer1(), to2.GetAnswer2()));
    }
};

int main() {
    Solution().Solve();
    return 0;
}
