#include <algorithm>
#include <cstdio>
#include <climits>
#include <string>
#include <vector>
using namespace std;

char Buffer[100001];

bool AssignMin(long long* p, long long v) {
    if (v < *p) return *p = v, true;
    return false;
}

struct Solution {
    int N;
    vector<long long> C, F[2];
    vector<string> S, R;

    void Solve() {
        scanf("%d", &N);
        C.resize(N);
        for (int i = 0; i < N; ++i) scanf("%lld", &C[i]);
        S.resize(N);
        R.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%s", Buffer);
            S[i] = Buffer;
            R[i] = Buffer;
            reverse(R[i].begin(), R[i].end());
        }
        for (int i = 0; i < 2; ++i) F[i].resize(N);
        F[0][0] = 0;
        F[1][0] = C[0];
        for (int i = 1; i < N; ++i) {
            F[0][i] = F[1][i] = LONG_LONG_MAX >> 1;
            if (S[i - 1] <= S[i]) AssignMin(&F[0][i], F[0][i - 1]);
            if (S[i - 1] <= R[i]) AssignMin(&F[1][i], F[0][i - 1] + C[i]);
            if (R[i - 1] <= S[i]) AssignMin(&F[0][i], F[1][i - 1]);
            if (R[i - 1] <= R[i]) AssignMin(&F[1][i], F[1][i - 1] + C[i]);
        }
        long long answer = min(F[0][N - 1], F[1][N - 1]);
        printf("%lld\n", answer == LONG_LONG_MAX >> 1 ? -1 : answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
