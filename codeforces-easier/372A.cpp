#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> S;

    void Solve() {
        scanf("%d", &N);
        S.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &S[i]);
        sort(S.begin(), S.end());
        int i2 = N / 2, answer = N - N / 2;
        for (int i = 0; i < N / 2; ++i) {
            while (i2 < N && S[i2] < S[i] + S[i]) ++i2;
            if (i2 < N) {
                ++i2;
            } else {
                ++answer;
            }
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
