#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[501];

struct Solution {
    int H, W, Q;
    vector<string> Grid, GridT;
    vector<vector<int> > Prefix, PrefixT;

    vector<vector<int> > ComputePrefix(const vector<string>& grid) {
        vector<vector<int> > prefix(grid.size() + 1);
        for (int i = 0; i < prefix.size(); ++i) {
            prefix[i].resize(grid[0].size() + 1);
        }
        for (int i = 0; i < grid.size(); ++i) {
            for (int j = 0; j < grid[i].size() - 1; ++j) {
                prefix[i + 1][j + 1] = 
                    grid[i][j] == '.' && grid[i][j + 1] == '.';
            }
        }
        for (int i = 0; i < prefix.size(); ++i) {
            for (int j = 1; j < prefix[i].size(); ++j) {
                prefix[i][j] += prefix[i][j - 1];
            }
        }
        for (int i = 1; i < prefix.size(); ++i) {
            for (int j = 0; j < prefix[i].size(); ++j) {
                prefix[i][j] += prefix[i - 1][j];
            }
        }
        return prefix;
    }

    int Compute(const vector<vector<int> >& prefix, 
        int r1, int c1, int r2, int c2) {
        int a1 = prefix[r2][c2];
        int a2 = prefix[r2][c1 - 1];
        int a3 = prefix[r1 - 1][c2];
        int a4 = prefix[r1 - 1][c1 - 1];
        return a1 - a2 - a3 + a4;
    }

    void Solve() {
        scanf("%d%d", &H, &W);
        Grid.resize(H);
        for (int i = 0; i < H; ++i) {
            scanf("%s", Buffer);
            Grid[i] = Buffer;
        }
        GridT.resize(W);
        for (int i = 0; i < W; ++i) {
            GridT[i].resize(H);
            for (int j = 0; j < H; ++j) {
                GridT[i][j] = Grid[j][i];
            }
        }
        Prefix = ComputePrefix(Grid);
        PrefixT = ComputePrefix(GridT);
        scanf("%d", &Q);
        for (int i = 0; i < Q; ++i) {
            int r1, c1, r2, c2;
            scanf("%d%d%d%d", &r1, &c1, &r2, &c2);
            int answer1 = Compute(Prefix, r1, c1, r2, c2 - 1);
            int answer2 = Compute(PrefixT, c1, r1, c2, r2 - 1);
            printf("%d\n", answer1 + answer2);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
