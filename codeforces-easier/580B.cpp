#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

bool AssignMax(long long* p, long long v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Person {
    int M, S;

    bool operator<(const Person& that) { return M < that.M; }
};

struct Solution {
    int N, D;
    vector<Person> A;

    void Solve() {
        scanf("%d%d", &N, &D);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d%d", &A[i].M, &A[i].S);
        sort(A.begin(), A.end());
        int p1 = 0, p2 = 0;
        long long current = 0, answer = 0;
        while (p2 < A.size()) {
            while (A[p1].M + D <= A[p2].M) current -= A[p1++].S;
            AssignMax(&answer, current += A[p2++].S);
        }
        printf("%lld\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
