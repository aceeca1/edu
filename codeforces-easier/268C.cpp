#include <algorithm>
#include <cstdio>
using namespace std;

struct Solution {
    int N, M;

    void Solve() {
        scanf("%d%d", &N, &M);
        int k = min(N, M);
        printf("%d\n", k + 1);
        for (int i = 0; i <= k; ++i) printf("%d %d\n", N - i, i);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
