#include <cstdio>
#include <string>
using namespace std;

struct Solution {
    string S, Result, Chosen;

    bool Put(int pos) {
        if (3 < Chosen.size()) return false;
        if (S.size() < pos) return false;
        if (Put(pos + 1)) return true;
        Chosen.push_back(S[pos]);
        int value;
        bool success = sscanf(Chosen.c_str(), "%d", &value) == 1;
        if (success && value % 8 == 0) {
            Result = Chosen;
            return true;
        }
        if (Put(pos + 1)) return true;
        Chosen.pop_back();
        return false;
    }

    void Solve() {
        char s[101];
        scanf("%s", s);
        S = s;
        if (Put(0)) {
            printf("YES\n%s\n", Result.c_str());
        } else {
            printf("NO\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
