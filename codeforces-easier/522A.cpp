#include <cctype>
#include <cstdio>
#include <cstring>
#include <map>
#include <string>
using namespace std;

char Buffer1[25], Buffer2[25];

bool AssignMax(int *p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    map<string, int> Length;
    int N;

    string Lower(const char* s) {
        string answer;
        answer.reserve(strlen(s));
        for (int i = 0; s[i]; ++i) answer += char(tolower(s[i]));
        return answer;
    }

    void Solve() {
        Length["polycarp"] = 1;
        scanf("%d", &N);
        int answer = 0;
        for (int i = 0; i < N; ++i) {
            scanf("%s reposted %s", Buffer1, Buffer2);
            string s1 = Lower(Buffer1);
            string s2 = Lower(Buffer2);
            int length = Length[s1] = Length[s2] + 1;
            AssignMax(&answer, length);
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
