#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<vector<int> > Out;
    vector<int> C, Subtree;

    void Hang(int no, int parent) {
        for (int i = 0; i < Out[no].size(); ++i) {
            if (Out[no][i] == parent) {
                swap(Out[no][0], Out[no][i]);
            } else {
                Hang(Out[no][i], no);
            }
        }
    }

    void ComputeSubtree(int no) {
        Subtree[no] = C[no];
        for (int i = 1; i < Out[no].size(); ++i) {
            int target = Out[no][i];
            ComputeSubtree(target);
            if (Subtree[target] != Subtree[no]) Subtree[no] = 0;
        }
    }

    int FindGoodRoot(int no, int upColor) {
        bool good = (upColor != 0);
        for (int i = 1; i < Out[no].size(); ++i) {
            int target = Out[no][i];
            if (Subtree[target] == 0) good = false;
        }
        if (good) return no;
        if (upColor != C[no]) return -1;
        int different = 0;
        for (int i = 1; i < Out[no].size(); ++i) {
            int target = Out[no][i];
            if (Subtree[target] != upColor) ++different;
        }
        for (int i = 1; i < Out[no].size(); ++i) {
            int target = Out[no][i];
            if (Subtree[target] != upColor) --different;
            if (different == 0) {
                int ret = FindGoodRoot(target, upColor);
                if (ret != -1) return ret;
            }
            if (Subtree[target] != upColor) ++different;
        }
        return -1;
    }

    void Solve() {
        scanf("%d", &N);
        Out.resize(N);
        for (int i = 1; i < N; ++i) {
            int v1, v2;
            scanf("%d%d", &v1, &v2);
            Out[v1 - 1].push_back(v2 - 1);
            Out[v2 - 1].push_back(v1 - 1);
        }
        C.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &C[i]);
        Out[0].push_back(-1);
        Hang(0, -1);
        Subtree.resize(N);
        ComputeSubtree(0);
        int ret = FindGoodRoot(0, C[0]);
        if (ret == -1) {
            printf("NO\n");
        } else {
            printf("YES\n%d\n", ret + 1);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
