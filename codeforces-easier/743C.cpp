#include <cstdio>
using namespace std;

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        if (N == 1) {
            printf("-1\n");
        } else {
            printf("%d %d %d\n", N, N + 1, N * (N + 1));
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
