#include <cstdio>
#include <map>
#include <string>
#include <vector>
using namespace std;

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N;
    vector<string> Name;
    vector<int> Score;

    void Solve() {
        scanf("%d", &N);
        Name.resize(N);
        Score.resize(N);
        map<string, int> finalScore;
        for (int i = 0; i < N; ++i) {
            char s[33];
            scanf("%s%d", s, &Score[i]);
            Name[i] = s;
            finalScore[Name[i]] += Score[i];
        }
        int maxScore = 0;
        for (
            map<string, int>::iterator it = finalScore.begin();
            it != finalScore.end();
            ++it
        ) AssignMax(&maxScore, it->second);
        map<string, int> currentScore;
        for (int i = 0; i < N; ++i) {
            int score = currentScore[Name[i]] += Score[i];
            if (maxScore <= score && finalScore[Name[i]] == maxScore) {
                printf("%s\n", Name[i].c_str());
                return;
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
