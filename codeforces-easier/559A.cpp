#include <cstdio>
#include <complex>
#include <math.h>
using namespace std;

const double PI = acos(0.0) * 2.0;

typedef complex<double> Point;

double Cross(Point a1, Point a2) {
    return a1.real() * a2.imag() - a1.imag() * a2.real();
}

struct Solution {
    int A[6];
    Point P[6];

    double Area() {
        double answer = 0.0;
        for (int i = 0; i < 6; ++i) {
            Point a1 = P[i];
            Point a2 = i == 5 ? P[0] : P[i + 1];
            answer += Cross(a1, a2);
        }
        return answer;
    }

    void Solve() {
        scanf("%d%d%d%d%d%d", &A[0], &A[1], &A[2], &A[3], &A[4], &A[5]);
        for (int i = 0; i < 6; ++i) {
            P[i] = polar((double)A[i], PI / 3.0 * i);
        }
        Point last;
        for (int i = 0; i < 6; ++i) {
            last = P[i] += last;
        }
        printf("%.0lf\n", Area() / (sin(PI / 3.0)));
    }
};

int main() {
    Solution().Solve();
    return 0;
}
