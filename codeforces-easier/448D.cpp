#include <algorithm>
#include <cstdio>
using namespace std;

typedef long long int64;

struct Solution {
    int64 N, M, K;

    int64 CountNoMoreThan(int64 x) {
        int64 answer = 0;
        for (int64 i = 1; i <= N; ++i) {
            answer += min(x / i, M);
        }
        return answer;
    }

    int64 BinarySearch(int64 lower, int64 upper) {
        while (lower < upper) {
            int64 mid = lower + (upper - lower) / 2;
            if (CountNoMoreThan(mid) < K) {
                lower = mid + 1;
            } else {
                upper = mid;
            }
        }
        return lower;
    }

    void Solve() {
        scanf("%lld%lld%lld", &N, &M, &K);
        printf("%lld\n", BinarySearch(1, N * M));
    }
};

int main() {
    Solution().Solve();
    return 0;
}
