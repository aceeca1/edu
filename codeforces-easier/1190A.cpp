#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int64 N, M, K;
    vector<int64> P;

    void Solve() {
        scanf("%lld%lld%lld", &N, &M, &K);
        P.resize(M);
        for (int i = 0; i < M; ++i) scanf("%lld", &P[i]);
        int answer = 0;
        for (int i = 0; i < M; ) {
            int64 page = (P[i] - i - 1) / K;
            int j = i + 1;
            for (; j < M; ++j) {
                int64 page1 = (P[j] - i - 1) / K;
                if (page1 != page) break;
            }
            ++answer;
            i = j;
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}

