#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int D, SumTime;
    vector<int> MinTime, MaxTime;
    
    void Solve() {
        scanf("%d%d", &D, &SumTime);
        MinTime.resize(D);
        MaxTime.resize(D);
        int sumMinTime = 0, sumMaxTime = 0;
        for (int i = 0; i < D; ++i) {
            scanf("%d%d", &MinTime[i], &MaxTime[i]);
            sumMinTime += MinTime[i];
            sumMaxTime += MaxTime[i];
        }
        if (SumTime < sumMinTime || sumMaxTime < SumTime) {
            printf("NO\n");
            return;
        }
        printf("YES\n");
        bool head = true;
        SumTime -= sumMinTime;
        for (int i = 0; i < D; ++i) {
            int flexibleTime = MaxTime[i] - MinTime[i];
            int actualTime = min(SumTime, flexibleTime);
            SumTime -= actualTime;
            if (!head) putchar(' ');
            head = false;
            printf("%d", actualTime + MinTime[i]);
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
