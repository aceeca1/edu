#include <algorithm>
#include <cstdio>
#include <string>
using namespace std;

struct Solution {
    int N;

    bool Parse(const string& s, int* r, int* c) {
        return sscanf(s.c_str(), "R%dC%d", r, c) == 2;
    }

    string FromRC(int r, int c) {
        string answer;
        while (c != 0) {
            int c0 = (c - 1) % 26;
            answer += char('A' + c0);
            c = (c - (c0 + 1)) / 26;
        }
        reverse(answer.begin(), answer.end());
        char s[80];
        sprintf(s, "%d", r);
        return answer += s;
    }

    string ToRC(string s) {
        char s0[80];
        int r, c = 0;
        sscanf(s.c_str(), "%[A-Z]%d", s0, &r);
        for (int i = 0; s0[i]; ++i) c = c * 26 + (s0[i] - 'A' + 1);
        sprintf(s0, "R%dC%d", r, c);
        return s0;
    }

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            char s[80];
            scanf("%s", s);
            int r, c;
            if (Parse(s, &r, &c)) {
                printf("%s\n", FromRC(r, c).c_str());
            } else {
                printf("%s\n", ToRC(s).c_str());
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
