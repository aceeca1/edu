#include <cstdio>
#include <stack>
#include <vector>
using namespace std;

struct Solution {
    int N, M;
    vector<vector<int> > Out;
    vector<int> A[2], Visited;

    bool DFS(int no, int part) {
        stack<int> stack0;
        Visited[no] = part;
        stack0.push(no);
        while (!stack0.empty()) {
            no = stack0.top();
            part = Visited[no];
            A[part - 1].push_back(no);
            stack0.pop();
            for (int i = 0; i < Out[no].size(); ++i) {
                int target = Out[no][i];
                if (Visited[target] == 0) {
                    Visited[target] = 3 - part;
                    stack0.push(target);
                } else if (Visited[target] != 3 - part) {
                    return false;
                }
            }
        }
        return true;
    }

    bool Bipartite() {
        Visited.resize(N);
        for (int i = 0; i < N; ++i) {
            if (!Visited[i] && !DFS(i, 1)) return false;
        }
        return true;
    }

    void Print(const vector<int>& a) {
        printf("%d\n", a.size());
        bool head = true;
        for (int i = 0; i < a.size(); ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i] + 1);
        }
        printf("\n");
    }

    void Solve() {
        scanf("%d%d", &N, &M);
        Out.resize(N);
        for (int i = 0; i < M; ++i) {
            int u, v;
            scanf("%d%d", &u, &v);
            Out[u - 1].push_back(v - 1);
            Out[v - 1].push_back(u - 1);
        }
        if (Bipartite()) {
            Print(A[0]);
            Print(A[1]);
        } else {
            printf("-1\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
