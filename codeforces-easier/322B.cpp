#include <algorithm>
#include <cstdio>
using namespace std;

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int R, G, B;

    void Solve() {
        scanf("%d%d%d", &R, &G, &B);
        int minRGB = min(R, min(G, B));
        int answer = 0;
        for (int i = 0; i < 3; ++i) {
            if (minRGB < i) break;
            int r = (R - i) / 3;
            int g = (G - i) / 3;
            int b = (B - i) / 3;
            AssignMax(&answer, r + g + b + i);
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
