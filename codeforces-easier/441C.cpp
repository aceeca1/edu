#include <cstdio>
using namespace std;

struct Snake {
    int N, M, X, Y, DY;

    Snake(int n, int m) {
        N = n, M = m;
        X = 1, Y = 1, DY = 1;
    }

    void Next() {
        Y += DY;
        if (Y == 0) {
            ++X;
            Y = 1;
            DY = -DY;
        } else if (Y == M + 1) {
            ++X;
            Y = M;
            DY = -DY;
        }
    }
};

struct Solution {
    int N, M, K;

    void Solve() {
        scanf("%d%d%d", &N, &M, &K);
        Snake snake(N, M);
        for (int i = 0; i < K - 1; ++i) {
            printf("2 %d %d ", snake.X, snake.Y);
            snake.Next();
            printf("%d %d\n", snake.X, snake.Y);
            snake.Next();
        }
        int total = N * M - (K + K - 2);
        printf("%d", total);
        for (int i = 0; i < total; ++i) {
            printf(" %d %d", snake.X, snake.Y);
            snake.Next();
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
}
