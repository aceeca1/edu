#include <cstdio>
using namespace std;

const int M = 1000000007;

struct Solution {
    int N;
    
    void Solve() {
        scanf("%d", &N);
        long long a0 = 1, a1 = 0;
        for (int i = 2; i <= N; ++i) {
            long long a2 = (2 * a1 + 3 * a0) % M;
            a0 = a1;
            a1 = a2;
        }
        printf("%lld\n", a1);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
