#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, Q;
    vector<int> A, B;

    void Solve() {
        scanf("%d%d", &N, &Q);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        B.resize(N + 2);
        for (int i = 0; i < Q; ++i) {
            int left, right;
            scanf("%d%d", &left, &right);
            ++B[left];
            --B[right + 1];
        }
        for (int i = 0; i <= N; ++i) B[i + 1] += B[i];
        sort(A.begin(), A.end());
        sort(B.begin(), B.end());
        long long answer = 0;
        for (int i = 0; i < N; ++i) {
            answer += (long long)A[i] * B[i + 2];
        }
        printf("%lld\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
