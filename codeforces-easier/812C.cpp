#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int N, S;
    vector<int> A;

    int64 FindSum(int k) {
        vector<int64> price(N);
        for (int i = 0; i < N; ++i) price[i] = A[i] + k * int64(i + 1);
        sort(price.begin(), price.end());
        int64 sum = 0;
        for (int i = 0; i < k; ++i) sum += price[i];
        return sum;
    }

    int BinarySearch(int lower, int upper) {
        while (lower < upper) {
            int mid = lower + (upper - lower) / 2;
            if (FindSum(mid) <= S) {
                lower = mid + 1;
            } else {
                upper = mid;
            }
        }
        return lower;
    }

    void Solve() {
        scanf("%d%d", &N, &S);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        int items = BinarySearch(1, N + 1) - 1;
        int64 sum = FindSum(items);
        printf("%d %lld\n", items, sum);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
