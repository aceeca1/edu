#include <cstdio>
#include <stack>
#include <string>
#include <vector>
using namespace std;

char Buffer[51];

int DeltaX[] = {1, 0, -1, 0};
int DeltaY[] = {0, 1, 0, -1};

struct Node {
    int X, Y, lastX, lastY;
};

struct Solution {
    int N, M;
    vector<string> A;
    bool Cycle;

    void DFS(int x, int y) {
        char c = A[x][y];
        A[x][y] = '.';
        stack<Node> stack0;
        Node node = {x, y, -1, -1};
        stack0.push(node);
        while (!stack0.empty()) {
            Node node = stack0.top();
            stack0.pop();
            A[node.X][node.Y] = '.';
            for (int i = 0; i < 4; ++i) {
                int newX = node.X + DeltaX[i];
                int newY = node.Y + DeltaY[i];
                if (newX < 0 || N <= newX || newY < 0 || M <= newY) continue;
                if (newX == node.lastX && newY == node.lastY) continue;
                if (A[newX][newY] == '.') Cycle = true;
                if (A[newX][newY] == c) {
                    A[newX][newY] = '.';
                    Node newNode = {newX, newY, node.X, node.Y};
                    stack0.push(newNode);
                }
            }
            A[node.X][node.Y] = '#';
        }
    }

    void Solve() {
        scanf("%d%d", &N, &M);
        A.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%s", Buffer);
            A[i] = Buffer;
        }
        Cycle = false;
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < M; ++j) {
                if (A[i][j] != '#') DFS(i, j);
            }
        }
        printf("%s\n", Cycle ? "Yes" : "No");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
