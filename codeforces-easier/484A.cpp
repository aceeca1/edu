#include <algorithm>
#include <cstdio>
using namespace std;

typedef long long int64;

bool AssignMin(int64* p, int64 v) {
    if (v < *p) return *p = v, true;
    return false;
}

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N;
    int64 L, R;

    int PopCount(int64 x) {
        int answer = 0;
        while (x != 0) {
            x &= x - 1;
            ++answer;
        }
        return answer;
    }

    int64 MaxPopCount(int64 boundL, int64 boundR) {
        int depth = 0, answer = 0;
        int64 arg = 0;
        int popCountL = PopCount(boundL);
        int popCountR = PopCount(boundR);
        while ((boundL ^ boundR) != 1) {
            if ((boundL & 1) == 0) {
                int answer1 = depth + popCountL + 1;
                int64 arg1 = ((boundL + 1 + 1) << depth) - 1;
                if (AssignMax(&answer, answer1)) {
                    arg = arg1;
                } else if (answer == answer1) {
                    AssignMin(&arg, arg1);
                }
            }
            if ((boundR & 1) != 0) {
                int answer1 = depth + popCountR - 1;
                int64 arg1 = ((boundR - 1 + 1) << depth) - 1;
                if (AssignMax(&answer, answer1)) {
                    arg = arg1;
                } else if (answer == answer1) {
                    AssignMin(&arg, arg1);
                }
            }
            if (boundL & 1) --popCountL;
            boundL >>= 1;
            if (boundR & 1) --popCountR;
            boundR >>= 1;
            ++depth;
        }
        return arg;
    }

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            scanf("%lld%lld", &L, &R);
            printf("%lld\n", MaxPopCount(max(0LL, L - 1), R + 1));
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
