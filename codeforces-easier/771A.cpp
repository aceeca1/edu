#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int N, M;
    vector<vector<int> > Out;
    vector<bool> Visited;

    void DFS(int no, int* nodes, int* edges) {
        Visited[no] = true;
        ++*nodes;
        for (int i = 0; i < Out[no].size(); ++i) {
            ++*edges;
            int target = Out[no][i];
            if (Visited[target]) continue;
            DFS(target, nodes, edges);
        }
    }

    bool Reasonable() {
        Visited.resize(N + 1);
        for (int i = 1; i < N; ++i) {
            if (Visited[i]) continue;
            int nodes = 0, edges = 0;
            DFS(i, &nodes, &edges);
            if (int64(nodes) * (nodes - 1) != edges) return false;
        }
        return true;
    }

    void Solve() {
        scanf("%d%d", &N, &M);
        Out.resize(N + 1);
        for (int i = 0; i < M; ++i) {
            int v1, v2;
            scanf("%d%d", &v1, &v2);
            Out[v1].push_back(v2);
            Out[v2].push_back(v1);
        }
        printf(Reasonable() ? "YES\n" : "NO\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
