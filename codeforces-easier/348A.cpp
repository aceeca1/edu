#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int N;
    vector<int> A;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        int64 sum = 0;
        for (int i = 0; i < N; ++i) sum += A[i];
        int64 answer = (sum + N - 2) / (N - 1);
        int64 maxA = *max_element(A.begin(), A.end());
        printf("%lld\n", max(maxA, answer));
    }
};

int main() {
    Solution().Solve();
    return 0;
}
