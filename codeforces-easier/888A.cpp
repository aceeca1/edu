#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> A;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        int answer = 0;
        for (int i = 1; i < N - 1; ++i) {
            if (A[i - 1] < A[i] && A[i + 1] < A[i]) ++answer;
            if (A[i] < A[i - 1] && A[i] < A[i + 1]) ++answer;
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
