#include <cstdio>
#include <cmath>
using namespace std;

struct Solution {
    int M, N;

    void Solve() {
        scanf("%d%d", &M, &N);
        double answer = 0.0;
        for (int i = 1; i <= M; ++i) {
            answer += i * (pow(double(i) / M, N) - pow(double(i - 1) / M, N));
        }
        printf("%.12lf\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
