#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Exam {
    int A, B;

    bool operator<(const Exam& that) {
        return A < that.A || (A == that.A && B < that.B);
    }
};

struct Solution {
    int N;
    vector<Exam> Exams;

    void Solve() {
        scanf("%d", &N);
        Exams.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%d%d", &Exams[i].A, &Exams[i].B);
        }
        sort(Exams.begin(), Exams.end());
        int current = 0;
        for (int i = 0; i < N; ++i) {
            if (Exams[i].B < current) {
                current = Exams[i].A;
            } else {
                current = Exams[i].B;
            }
        }
        printf("%d\n", current);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
