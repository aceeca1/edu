#include <algorithm>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

struct Solution {
    int V, MinA, Length;
    vector<int> A;

    string ComputeAnswer() {
        string answer;
        int exceed = V - Length * MinA;
        for (int i = 9; ; --i) {
            while (A[i] - MinA <= exceed) {
                exceed -= A[i] - MinA;
                answer += char('0' + i);
                if (answer.size() == Length) return answer;
            }
        }
    }

    void Solve() {
        scanf("%d", &V);
        A.resize(10);
        for (int i = 1; i < 10; ++i) scanf("%d", &A[i]);
        MinA = *min_element(A.begin() + 1, A.end());
        Length = V / MinA;
        if (Length == 0) {
            printf("-1\n");
        } else {
            printf("%s\n", ComputeAnswer().c_str());
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
