#include <cstdio>
#include <vector>
using namespace std;

struct SegTree {
    int K, M;
    vector<int> Tree;
    
    SegTree(int k) {
        K = k;
        M = 1 << K;
        Tree.resize(M << 1);
    }
    
    void Compute(int p, int k) {
        if ((K - k) & 1) {
            Tree[p] = Tree[p + p] | Tree[p + p + 1];
        } else {
            Tree[p] = Tree[p + p] ^ Tree[p + p + 1];
        }
    }
    
    void Build() {
        for (int i = K - 1; i >= 0; --i) {
            for (int j = 1 << i; j < (1 << (i + 1)); ++j) Compute(j, i);
        }
    }
    
    int Set(int p, int b) {
        p += M;
        Tree[p] = b;
        for (int i = 1; i <= K; ++i) Compute(p >> i, K - i);
        return Tree[1];
    }
};

struct Solution {
    int N, M;
    
    void Solve() {
        scanf("%d%d", &N, &M);
        SegTree tree(N);
        for (int i = 0; i < (1 << N); ++i) scanf("%d", &tree.Tree[tree.M + i]);
        tree.Build();
        for (int i = 0; i < M; ++i) {
            int p, b;
            scanf("%d%d", &p, &b);
            printf("%d\n", tree.Set(p - 1, b));
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
