#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Compare {
    const vector<int>* V;

    Compare(const vector<int>& v) { V = &v; }

    bool operator()(int i1, int i2) {
        return V->at(i2) < V->at(i1);
    }
};

struct Solution {
    int N, M;
    vector<int> V, No;
    vector<vector<int> > Out;
    vector<bool> Removed;

    void Solve() {
        scanf("%d%d", &N, &M);
        V.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &V[i]);
        Out.resize(N);
        for (int i = 0; i < M; ++i) {
            int u, v;
            scanf("%d%d", &u, &v);
            Out[u - 1].push_back(v - 1);
            Out[v - 1].push_back(u - 1);
        }
        No.resize(N);
        for (int i = 0; i < N; ++i) No[i] = i;
        sort(No.begin(), No.end(), Compare(V));
        Removed.resize(N);
        int answer = 0;
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < Out[No[i]].size(); ++j) {
                int target = Out[No[i]][j];
                if (Removed[target]) continue;
                answer += V[target];
            }
            Removed[No[i]] = true;
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
