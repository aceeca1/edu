#include <cstdio>
#include <set>
using namespace std;

struct Solution {
    int N;
    set<int> A;

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            int v;
            scanf("%d", &v);
            A.insert(v);
        }
        int answer = 1;
        while (A.count(answer)) ++answer;
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
