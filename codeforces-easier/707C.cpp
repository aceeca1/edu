#include <cstdio>
using namespace std;

typedef long long int64;

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        if (N == 1 || N == 2) {
            printf("-1\n");
        } else {
            int lowbit = N & -N;
            N /= lowbit;
            if (N == 1) {
                int64 sum = int64(lowbit) * lowbit / 2;
                int64 difference = 2;
                printf("%lld %lld\n",
                    (sum + difference) / 2, (sum - difference) / 2);
            } else {
                int64 sum = int64(N) * N;
                int64 difference = 1;
                printf("%lld %lld\n",
                    (sum + difference) / 2 * lowbit,
                    (sum - difference) / 2 * lowbit);
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
