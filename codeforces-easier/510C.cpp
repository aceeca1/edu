#include <algorithm>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[101];

struct Solution {
    int N;
    string Last, Current, Answer;
    vector<vector<bool> > Less;
    vector<int> Visited;
    bool Failure;

    bool AddEdge() {
        for (int i = 0; ; ++i) {
            if (i == Last.size()) return true;
            if (i == Current.size()) return false;
            if (Last[i] != Current[i]) {
                Less[Last[i] - 'a'][Current[i] - 'a'] = true;
                return true;
            }
        }
    }

    bool DFS(int no) {
        Visited[no] = 1;
        for (int i = 0; i < 26; ++i) {
            if (!Less[no][i]) continue;
            if (Visited[i] == 1) return false;
            if (Visited[i] == 0 && !DFS(i)) return false;
        }
        Visited[no] = 2;
        Answer += char('a' + no);
        return true;
    }

    bool TopoSort() {
        Visited.resize(26);
        for (int i = 0; i < 26; ++i) {
            if (Visited[i] == 0 && !DFS(i)) return false;
        }
        reverse(Answer.begin(), Answer.end());
        return Answer.size() == 26;
    }

    void Solve() {
        Less.resize(26);
        for (int i = 0; i < 26; ++i) Less[i].resize(26);
        scanf("%d", &N);
        Failure = false;
        for (int i = 0; i < N; ++i) {
            scanf("%s", Buffer);
            Current = Buffer;
            if (!AddEdge()) Failure = true;
            Last = Current;
        }
        if (Failure || !TopoSort()) {
            printf("Impossible\n");
        } else {
            printf("%s\n", Answer.c_str());
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
