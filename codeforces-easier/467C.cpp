#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

bool AssignMax(int64* p, int64 v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N, M, K;
    vector<int64> A;
    vector<vector<int64> > F;

    void Solve() {
        scanf("%d%d%d", &N, &M, &K);
        A.resize(N + 1);
        for (int i = 1; i <= N; ++i) scanf("%lld", &A[i]);
        for (int i = 1; i <= N; ++i) A[i] += A[i - 1];
        F.resize(N + 1);
        F[0].resize(K + 1);
        for (int i = 1; i <= N; ++i) {
            F[i].resize(K + 1);
            for (int j = 1; j <= K; ++j) {
                AssignMax(&F[i][j], F[i - 1][j]);
                if (M <= i) {
                    AssignMax(&F[i][j], A[i] - A[i - M] + F[i - M][j - 1]);
                }
            }
        }
        printf("%lld\n", F[N][K]);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
