#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[201];

struct Solution {
    vector<string> Path;
    int N;

    vector<string> SplitPath(const string& path) {
        vector<string> answer;
        int p = 0;
        while (p < path.size()) {
            if (path[p] == '/') ++p;
            int nchar = 0;
            if (sscanf(&path[p], "%[^/]%n", Buffer, &nchar) == 1) {
                answer.push_back(Buffer);
                p += nchar;
            }
        }
        return answer;
    }

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            scanf("%s", Buffer);
            string command = Buffer;
            if (command == "pwd") {
                for (int i = 0; i < Path.size(); ++i) {
                    printf("/%s", Path[i].c_str());
                }
                printf("/\n");
            } else {
                scanf("%s", Buffer);
                string newPath = Buffer;
                if (newPath[0] == '/') Path.clear();
                vector<string> newSplitted = SplitPath(newPath);
                for (int i = 0; i < newSplitted.size(); ++i) {
                    if (newSplitted[i] == "..") {
                        Path.pop_back();
                    } else {
                        Path.push_back(newSplitted[i]);
                    }
                }
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
