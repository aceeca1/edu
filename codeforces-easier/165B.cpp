#include <cstdio>
using namespace std;

struct Solution {
    int N, K;

    int ComputeLines(int init) {
        int answer = init;
        while (init != 0) {
            init /= K;
            answer += init;
        }
        return answer;
    }

    int BinarySearch(int lower, int upper) {
        while (lower < upper) {
            int mid = lower + (upper - lower) / 2;
            if (N <= ComputeLines(mid)) {
                upper = mid;
            } else {
                lower = mid + 1;
            }
        }
        return lower;
    }

    void Solve() {
        scanf("%d%d", &N, &K);
        int answer = BinarySearch(0, N);
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
