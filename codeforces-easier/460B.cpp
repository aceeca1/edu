#include <cstdio>
#include <vector>
using namespace std;

const int M = 1000000000;

struct Solution {
    int A, B, C;

    int DigitSum(int x) {
        int answer = 0;
        while (x != 0) {
            answer += x % 10;
            x /= 10;
        }
        return answer;
    }

    void Solve() {
        scanf("%d%d%d", &A, &B, &C);
        vector<int> answer;
        for (int i = 1; i <= 81; ++i) {
            long long x = B;
            for (int j = 0; j < A; ++j) x *= i;
            x += C;
            if (x < M && DigitSum(x) == i) answer.push_back(x);
        }
        printf("%d\n", answer.size());
        if (!answer.empty()) {
            bool head = true;
            for (int i = 0; i < answer.size(); ++i) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", answer[i]);
            }
            printf("\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
