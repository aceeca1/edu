#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> A, B;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        B.resize(*max_element(A.begin(), A.end()) + 1);
        for (int i = 0; i < N; ++i) ++B[A[i]];
        int answer = 0, carry = 0;
        for (int i = 0; i < B.size(); ++i) {
            carry += B[i];
            answer += carry % 2;
            carry /= 2;
        }
        while (carry != 0) {
            answer += carry % 2;
            carry /= 2;
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
