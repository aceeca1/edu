#include <cstdio>
#include <vector>
using namespace std;

struct Graph {
    vector<vector<int> > Out;
    vector<int> Component;
    int ComponentSize;

    void AddEdge(int no1, int no2) {
        Out[no1].push_back(no2);
        Out[no2].push_back(no1);
    }

    void DFS(int no, int current) {
        Component[no] = current;
        for (int i = 0; i < Out[no].size(); ++i) {
            int target = Out[no][i];
            if (Component[target] == 0) DFS(target, current);
        }
    }

    void ComputeConnectedComponents() {
        ComponentSize = 0;
        Component.resize(Out.size());
        for (int i = 0; i < Out.size(); ++i) {
            if (Component[i] == 0) {
                DFS(i, ++ComponentSize);
            }
        }
    }

    bool SameComponent(int no1, int no2) {
        return Component[no1] == Component[no2];
    }
};

struct Solution {
    int N, M, Q;
    vector<Graph> Graphs;

    void Solve() {
        scanf("%d%d", &N, &M);
        Graphs.resize(M);
        for (int i = 0; i < M; ++i) Graphs[i].Out.resize(N);
        for (int i = 0; i < M; ++i) {
            int a, b, c;
            scanf("%d%d%d", &a, &b, &c);
            Graphs[c - 1].AddEdge(a - 1, b - 1);
        }
        for (int i = 0; i < M; ++i) Graphs[i].ComputeConnectedComponents();
        scanf("%d", &Q);
        for (int i = 0; i < Q; ++i) {
            int u1, u2;
            scanf("%d%d", &u1, &u2);
            int answer = 0;
            for (int j = 0; j < M; ++j) {
                if (Graphs[j].SameComponent(u1 - 1, u2 - 1)) ++answer;
            }
            printf("%d\n", answer);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
