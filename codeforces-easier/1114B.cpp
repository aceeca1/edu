#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct LessBy {
	const vector<int>* A;
	
	LessBy(const vector<int>& a) {A = &a;}
	
	bool operator()(int a1, int a2) {
		return A->at(a1) < A->at(a2);
	}
};

struct Solution {
	int N, M, K;
	vector<int> A;
	
	void Solve() {
		scanf("%d%d%d", &N, &M, &K);
		A.resize(N);
		for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
		vector<int> b(N);
		for (int i = 0; i < N; ++i) b[i] = i;
		sort(b.begin(), b.end(), LessBy(A));
		int64 answer = 0;
		for (int i = N - M * K; i < N; ++i) answer += A[b[i]];
		printf("%lld\n", answer);
		for (int i = 0; i < N; ++i) A[i] = 0;
		for (int i = N - M * K; i < N; ++i) A[b[i]] = 1;
		bool head = true;
		int p = 0;
		for (int i = 1; i < K; ++i) {
			for (int j = 0; j < M; ++j) {
				while (A[p] == 0) ++p;
				++p;
			}
			if (!head) putchar(' ');
			head = false;
			printf("%d", p);
		}
		printf("\n");
	}
};

int main() {
	Solution().Solve();
}
