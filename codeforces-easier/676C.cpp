#include <algorithm>
#include <cstdio>
#include <string>
using namespace std;

char Buffer[100001];

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N, K;
    string S;

    int Maximize(char a) {
        int p = 0, q = 0, b = 0, answer = 0;
        while (q < N) {
            if (S[q++] != a) ++b;
            while (K < b) {
                if (S[p++] != a) --b;
            }
            AssignMax(&answer, q - p);
        }
        return answer;
    }

    void Solve() {
        scanf("%d%d", &N, &K);
        scanf("%s", Buffer);
        S = Buffer;
        printf("%d\n", max(Maximize('a'), Maximize('b')));
    }
};

int main() {
    Solution().Solve();
    return 0;
}
