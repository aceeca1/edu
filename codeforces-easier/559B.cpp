#include <cstdio>
#include <string>
using namespace std;

typedef unsigned long long uint64;

uint64 Fingerprint(uint64 x) {
    const uint64 kMul = 0x9ddfea08eb382d69ULL;
    x *= kMul, x ^= x >> 47;
    x *= kMul, x ^= x >> 47;
    x *= kMul, x ^= x >> 47;
    return x * kMul;
}

char Buffer[200001];

struct Solution {
    string S1, S2;

    uint64 Hash(int size, const char* s) {
        if (size % 2 == 0) {
            uint64 finger1 = Fingerprint(Hash(size / 2, s));
            uint64 finger2 = Fingerprint(Hash(size / 2, s + size / 2));
            return finger1 + finger2;
        }
        uint64 answer = 0;
        for (int i = 0; i < size; ++i) {
            answer = Fingerprint(answer + s[i]);
        }
        return answer;
    }

    void Solve() {
        scanf("%s", Buffer);
        S1 = Buffer;
        scanf("%s", Buffer);
        S2 = Buffer;
        if (Hash(S1.size(), S1.c_str()) == Hash(S2.size(), S2.c_str())) {
            printf("YES\n");
        } else {
            printf("NO\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
