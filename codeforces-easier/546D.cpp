#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Query {
    int A, B;
};

struct Solution {
    int N, M;
    vector<Query> Queries;
    vector<int> MinFactor, NumFactor;

    void Solve() {
        scanf("%d", &N);
        Queries.resize(N);
        int M = 0;
        for (int i = 0; i < N; ++i) {
            scanf("%d%d", &Queries[i].A, &Queries[i].B);
            AssignMax(&M, Queries[i].A);
        }
        MinFactor.resize(M + 1);
        for (int i = 2; i <= M; ++i) {
            if (MinFactor[i] == 0) {
                MinFactor[i] = i;
                for (int64 j = int64(i) * i; j <= M; j += i) {
                    if (MinFactor[j] == 0) MinFactor[j] = i;
                }
            }
        }
        NumFactor.resize(M + 1);
        for (int i = 2; i <= M; ++i) {
            NumFactor[i] = NumFactor[i / MinFactor[i]] + 1;
        }
        for (int i = 1; i <= M; ++i) NumFactor[i] += NumFactor[i - 1];
        for (int i = 0; i < N; ++i) {
            printf("%d\n", NumFactor[Queries[i].A] - NumFactor[Queries[i].B]);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
