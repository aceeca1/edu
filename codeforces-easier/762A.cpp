#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

vector<int64> Divisors(int64 n) {
    vector<int64> answer1, answer2;
    int64 i = 1;
    for (; i * i < n; ++i) {
        if (n % i == 0) {
            answer1.push_back(i);
            answer2.push_back(n / i);
        }
    }
    if (i * i == n) answer1.push_back(i);
    for (int i = answer2.size() - 1; i >= 0; --i) {
        answer1.push_back(answer2[i]);
    }
    return answer1;
}

struct Solution {
    int64 N;
    int K;

    void Solve() {
        scanf("%lld%d", &N, &K);
        vector<int64> divisors = Divisors(N);
        printf("%lld\n", K <= divisors.size() ? divisors[K - 1] : -1);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
