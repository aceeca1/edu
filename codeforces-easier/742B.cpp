#include <cstdio>
#include <map>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int N, X;
    vector<int> A;
    map<int, int> S;

    void Solve() {
        scanf("%d%d", &N, &X);
        A.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%d", &A[i]);
            ++S[A[i]];
        }
        int64 answer = 0;
        if (X == 0) {
            for (int i = 0; i < N; ++i) {
                answer += S[A[i]] - 1;
            }
        } else {
            for (int i = 0; i < N; ++i) {
                answer += S[A[i] ^ X];
            }
        }
        printf("%lld\n", answer / 2);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
