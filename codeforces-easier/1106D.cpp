#include <cstdio>
#include <set>
#include <vector>
using namespace std;

struct Solution {
    int N, M;
    vector<vector<int> > Out;
    vector<bool> Visited;
    set<int> Q;

    void Solve() {
        scanf("%d%d", &N, &M);
        Out.resize(N);
        for (int i = 0; i < M; ++i) {
            int v1, v2;
            scanf("%d%d", &v1, &v2);
            Out[v1 - 1].push_back(v2 - 1);
            Out[v2 - 1].push_back(v1 - 1);
        }
        Visited.resize(N);
        Visited[0] = true;
        Q.insert(0);
        bool head = true;
        while (!Q.empty()) {
            int front = *Q.begin();
            Q.erase(Q.begin());
            if (!head) putchar(' ');
            head = false;
            printf("%d", front + 1);
            for (int i = 0; i < Out[front].size(); ++i) {
                int target = Out[front][i];
                if (!Visited[target]) {
                    Visited[target] = true;
                    Q.insert(target);
                }
            }
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
