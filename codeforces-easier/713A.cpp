#include <cstdio>
#include <string>
#include <vector>
using namespace std;

const int M = 18;

char Buffer[19];

struct Solution {
    int T;
    vector<int> A;
    
    int ToBinary(const string& b) {
        int answer = 0;
        for (int i = 0; i < b.size(); ++i) {
            answer = answer + answer + b[i] % 2;
        }
        return answer;
    }
    
    void Solve() {
        A.resize(1 << M);
        scanf("%d", &T);
        for (int i = 0; i < T; ++i) {
            char op;
            scanf(" %c %s", &op, Buffer);
            int x = ToBinary(Buffer);
            switch (op) {
                case '+': ++A[x]; break;
                case '-': --A[x]; break;
                case '?': printf("%d\n", A[x]); break;
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
