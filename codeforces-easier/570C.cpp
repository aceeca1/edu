#include <cstdio>
#include <string>
using namespace std;

char Buffer[300001];

struct Solution {
    int N, M;
    string S;

    void Solve() {
        scanf("%d%d", &N, &M);
        scanf("%s", Buffer);
        S = Buffer;
        int answer = 0;
        for (int i = 0; i < N - 1; ++i) {
            if (S[i] == '.' && S[i + 1] == '.') ++answer;
        }
        for (int i = 0; i < M; ++i) {
            int place;
            char newChar;
            scanf("%d %c", &place, &newChar);
            --place;
            if (place + 1 < N && S[place] == '.' && S[place + 1] == '.') {
                --answer;
            }
            if (0 <= place - 1 && S[place - 1] == '.' && S[place] == '.') {
                --answer;
            }
            S[place] = newChar;
            if (place + 1 < N && S[place] == '.' && S[place + 1] == '.') {
                ++answer;
            }
            if (0 <= place - 1 && S[place - 1] == '.' && S[place] == '.') {
                ++answer;
            }
            printf("%d\n", answer);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
