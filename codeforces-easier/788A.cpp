#include <cstdio>
#include <cmath>
#include <climits>
#include <vector>
using namespace std;

typedef long long int64;

bool AssignMax(int64* p, int64 v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N;
    vector<int> A;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        for (int i = 0; i < N - 1; ++i) A[i] = abs(A[i + 1] - A[i]);
        int64 answer = INT_MIN;
        if (1 <= N - 1) {
            int64 answer1 = A[0], current1 = A[0];
            for (int i = 2; i < N - 1; i += 2) {
                current1 -= A[i - 1];
                current1 = current1 > 0 ? current1 + A[i] : A[i];
                AssignMax(&answer1, current1);
            }
            AssignMax(&answer, answer1);
        }
        if (2 <= N - 1) {
            int64 answer2 = A[1], current2 = A[1];
            for (int i = 3; i < N - 1; i += 2) {
                current2 -= A[i - 1];
                current2 = current2 > 0 ? current2 + A[i] : A[i];
                AssignMax(&answer2, current2);
            }
            AssignMax(&answer, answer2);
        }
        printf("%lld\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
