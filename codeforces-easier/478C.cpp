#include <cstdio>
using namespace std;

bool AssignMin(long long* p, long long v) {
    if (v < *p) return *p = v, true;
    return false;
}

struct Solution {
    long long R, G, B;
    
    void Solve() {
        scanf("%lld%lld%lld", &R, &G, &B);
        AssignMin(&R, G + G + B + B);
        AssignMin(&G, B + B + R + R);
        AssignMin(&B, R + R + G + G);
        printf("%lld\n", (R + G + B) / 3);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
