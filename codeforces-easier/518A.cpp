#include <cstdio>
#include <string>
using namespace std;

char Buffer[101];

struct Solution {
    string S1, S2, S3;

    string Inc(string s) {
        int k = s.size() - 1;
        while (k >= 0) {
            if (s[k] != 'z') {
                ++s[k];
                break;
            }
            --k;
        }
        for (int i = k + 1; i < s.size(); ++i) s[i] = 'a';
        return s;
    }

    void Solve() {
        scanf("%s", Buffer);
        S1 = Buffer;
        scanf("%s", Buffer);
        S2 = Buffer;
        S3 = Inc(S1);
        if (S3 < S2) {
            printf("%s\n", S3.c_str());
        } else {
            printf("%s\n", "No such string");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
