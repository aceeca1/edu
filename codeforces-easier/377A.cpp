#include <cstdio>
#include <stack>
#include <string>
#include <vector>
using namespace std;

char S[501];

struct Solution {
    int N, M, K;
    vector<string> A;
    vector<pair<int, int> > DFSOrder, Connect4;
    
    Solution() {
        Connect4.push_back(make_pair(1, 0));
        Connect4.push_back(make_pair(0, 1));
        Connect4.push_back(make_pair(-1, 0));
        Connect4.push_back(make_pair(0, -1));
    }
    
    void AnyFreeCell(int* x, int* y) {
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < M; ++j) {
                if (A[i][j] == '.') {
                    *x = i;
                    *y = j;
                    return;
                }
            }
        }
    }
    
    void DFS(int x, int y) {
        stack<pair<int, int> > stack0;
        stack0.push(make_pair(x, y));
        A[x][y] = 'V';
        while (!stack0.empty()) {
            int x = stack0.top().first;
            int y = stack0.top().second;
            stack0.pop();
            DFSOrder.push_back(make_pair(x, y));
            for (int i = 0; i < 4; ++i) {
                int dx = Connect4[i].first;
                int dy = Connect4[i].second;
                int newX = x + dx;
                int newY = y + dy;
                if (newX < 0 || N <= newX) continue;
                if (newY < 0 || M <= newY) continue;
                if (A[newX][newY] == '.') {
                    A[newX][newY] = 'V';
                    stack0.push(make_pair(newX, newY));
                }
            }
        }
    }
    
    void Solve() {
        scanf("%d%d%d", &N, &M, &K);
        A.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%s", S);
            A[i] = S;
        }
        int x, y;
        AnyFreeCell(&x, &y);
        DFS(x, y);
        for (int i = 0; i < K; ++i) {
            x = DFSOrder[DFSOrder.size() - 1 - i].first;
            y = DFSOrder[DFSOrder.size() - 1 - i].second;
            A[x][y] = 'X';
        }
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < M; ++j) {
                if (A[i][j] == 'V') A[i][j] = '.';
            }
            printf("%s\n", A[i].c_str());
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
