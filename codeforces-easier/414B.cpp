#include <cstdio>
#include <vector>
using namespace std;

const int M = 1000000007;

struct Solution {
    int N, K;
    vector<int> F;
    
    void Solve() {
        scanf("%d%d", &N, &K);
        F.resize(N + 1);
        for (int i = 0; i <= N; ++i) F[i] = 1;
        for (int i = 1; i < K; ++i) {
            for (int j = N; j >= 1; --j) {
                for (int k = j + j; k <= N; k += j) {
                    F[k] = (F[k] + F[j]) % M;
                }
            }
        }
        int answer = 0;
        for (int i = 1; i <= N; ++i) answer = (answer + F[i]) % M;
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
