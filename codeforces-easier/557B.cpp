#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

bool AssignMin(double* p, double v) {
    if (v < *p) return *p = v, true;
    return false;
}

struct Solution {
    int N, W;
    vector<int> A;

    void Solve() {
        scanf("%d%d", &N, &W);
        A.resize(N + N);
        for (int i = 0; i < N + N; ++i) scanf("%d", &A[i]);
        sort(A.begin(), A.end());
        double minCup1 = 1.0 / 0.0, minCup2 = 1.0 / 0.0;
        for (int i = 0; i < N; ++i) AssignMin(&minCup1, A[i]);
        for (int i = 0; i < N; ++i) AssignMin(&minCup2, A[N + i]);
        double minCup = min(minCup1, minCup2 * 0.5);
        double capacity = minCup * 3.0 * N;
        printf("%.12lf\n", min(capacity, double(W)));
    }
};

int main() {
    Solution().Solve();
    return 0;
}
