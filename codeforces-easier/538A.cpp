#include <algorithm>
#include <cstdio>
#include <string>
using namespace std;

const string C = "CODEFORCES";

char Buffer[101];

struct Solution {
    string S;

    int MaxMatch(const string& s1, const string& s2) {
        for (int i = 0; ; ++i) {
            if (i == s1.size()) return i;
            if (i == s2.size()) return i;
            if (s1[i] != s2[i]) return i;
        }
    }

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        int match1 = MaxMatch(S, C);
        string S2 = S, C2 = C;
        reverse(S2.begin(), S2.end());
        reverse(C2.begin(), C2.end());
        int match2 = MaxMatch(S2, C2);
        printf(match1 + match2 >= C.size() ? "YES\n" : "NO\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
