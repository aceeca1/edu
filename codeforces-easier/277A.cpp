#include <cstdio>
#include <stack>
#include <vector>
using namespace std;

struct Solution {
    int N, M;
    vector<vector<int> > Out;
    vector<bool> Visited;
    
    void DFS(int no) {
        stack<int> stack0;
        stack0.push(no);
        Visited[no] = true;
        while (!stack0.empty()) {
            int n = stack0.top();
            stack0.pop();
            for (int i = 0; i < Out[n].size(); ++i) {
                int target = Out[n][i];
                if (Visited[target]) continue;
                Visited[target] = true;
                stack0.push(target);
            }
        }
    }
    
    bool NoneKnown() {
        for (int i = 0; i < N; ++i) {
            if (0 < Out[i].size()) return false;
        }
        return true;
    }
    
    void Solve() {
        scanf("%d%d", &N, &M);
        Out.resize(N + M);
        for (int i = 0; i < N; ++i) {
            int k;
            scanf("%d", &k);
            for (int j = 0; j < k; ++j) {
                int language;
                scanf("%d", &language);
                Out[i].push_back(N + language - 1);
                Out[N + language - 1].push_back(i);
            }
        }
        if (NoneKnown()) {
            printf("%d\n", N);
        } else {
            Visited.resize(Out.size());
            int answer = 0;
            for (int i = 0; i < N; ++i) {
                if (Visited[i]) continue;
                ++answer;
                DFS(i);
            }
            printf("%d\n", answer - 1);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
