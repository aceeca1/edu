#include <cstdio>
using namespace std;

typedef long long int64;

struct Solution {
    int64 L, R, K;

    void Solve() {
        scanf("%lld%lld%lld", &L, &R, &K);
        bool head = true;
        if (L == 1) {
            if (!head) putchar(' ');
            head = false;
            printf("1");
        }
        int64 p = 1;
        while (p < (L + K - 1) / K) p *= K;
        while (p <= R / K) {
            if (!head) putchar(' ');
            head = false;
            printf("%lld", p * K);
            p *= K;
        }
        if (head) {
            printf("-1\n");
        } else {
            printf("\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
