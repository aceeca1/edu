#include <cstdio>
#include <climits>
#include <string>
#include <vector>
using namespace std;

bool AssignMin(int *p, int v) {
    if (v < *p) return *p = v, true;
    return false;
}

char Buffer[100001];

struct Bag {
    vector<int> A;
    int NonZero;

    Bag() {
        A.resize(256);
        NonZero = 0;
    }

    void Add(char c) {
        if (A[c]++ == 0) ++NonZero;
    }

    void Remove(char c) {
        if (--A[c] == 0) --NonZero;
    }
};

struct Solution {
    int N;
    string S;

    void Solve() {
        scanf("%d %s", &N, Buffer);
        S = Buffer;
        Bag bag, bag0;
        for (int i = 0; i < S.size(); ++i) bag.Add(S[i]);
        int different = bag.NonZero;
        int p = 0, q = 0, answer = INT_MAX;
        while (true) {
            if (bag0.NonZero < different) {
                if (q == S.size()) break;
                bag0.Add(S[q++]);
            } else {
                AssignMin(&answer, q - p);
                bag0.Remove(S[p++]);
            }
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
