#include <algorithm>
#include <climits>
#include <cstdio>
#include <vector>
using namespace std;

bool AssignMin(int* p, int v) {
    if (v < *p) return *p = v, true;
    return false;
}

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N, L, R, X, Answer;
    vector<int> C;

    void Put(int no, int num, int sum, int minimum, int maximum) {
        if (num + N - no < 2) return;
        if (no == N) {
            if (X <= maximum - minimum && L <= sum && sum <= R) ++Answer;
            return;
        }
        Put(no + 1, num, sum, minimum, maximum);
        AssignMin(&minimum, C[no]);
        AssignMax(&maximum, C[no]);
        Put(no + 1, num + 1, sum + C[no], minimum, maximum);
    }

    void Solve() {
        scanf("%d%d%d%d", &N, &L, &R, &X);
        C.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &C[i]);
        Answer = 0;
        Put(0, 0, 0, INT_MAX, INT_MIN);
        printf("%d\n", Answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
