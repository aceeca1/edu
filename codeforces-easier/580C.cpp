#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, M;
    vector<vector<int> > Out;
    vector<bool> HasCat;

    void Hang(int no, int parent = -1) {
        int newSize = 0;
        for (int i = 0; i < Out[no].size(); ++i) {
            if (Out[no][i] != parent) Out[no][newSize++] = Out[no][i];
        }
        Out[no].resize(newSize);
        for (int i = 0; i < Out[no].size(); ++i) {
            Hang(Out[no][i], no);
        }
    }

    int Kefa(int no, int cat) {
        cat = HasCat[no] ? cat + 1 : 0;
        if (M < cat) return 0;
        int answer = 0;
        bool isLeaf = true;
        for (int i = 0; i < Out[no].size(); ++i) {
            answer += Kefa(Out[no][i], cat);
            isLeaf = false;
        }
        return isLeaf ? 1 : answer;
    }

    void Solve() {
        scanf("%d%d", &N, &M);
        Out.resize(N);
        HasCat.resize(N);
        for (int i = 0; i < N; ++i) {
            int hasCat;
            scanf("%d", &hasCat);
            HasCat[i] = hasCat;
        }
        for (int i = 0; i < N - 1; ++i) {
            int v1, v2;
            scanf("%d%d", &v1, &v2);
            Out[v1 - 1].push_back(v2 - 1);
            Out[v2 - 1].push_back(v1 - 1);
        }
        Hang(0);
        printf("%d\n", Kefa(0, 0));
    }
};

int main() {
    Solution().Solve();
    return 0;
}
