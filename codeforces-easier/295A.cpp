#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Operation {
    int L, R;
    int64 D;
};

struct Query {
    int X, Y;
};

struct Solution {
    int N, M, K;
    vector<int64> A;
    vector<Operation> Ops;
    vector<int> OpsTime;
    vector<Query> Queries;

    void Solve() {
        scanf("%d%d%d", &N, &M, &K);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        Ops.resize(M);
        for (int i = 0; i < M; ++i) {
            scanf("%d%d%lld", &Ops[i].L, &Ops[i].R, &Ops[i].D);
            --Ops[i].L, --Ops[i].R;
        }
        Queries.resize(K);
        for (int i = 0; i < K; ++i) {
            scanf("%d%d", &Queries[i].X, &Queries[i].Y);
            --Queries[i].X, --Queries[i].Y;
        }
        OpsTime.resize(M + 1);
        for (int i = 0; i < K; ++i) {
            ++OpsTime[Queries[i].X];
            --OpsTime[Queries[i].Y + 1];
        }
        for (int i = N - 1; i > 0; --i) A[i] -= A[i - 1];
        A.push_back(0);
        for (int i = 0; i < M; ++i) {
            OpsTime[i + 1] += OpsTime[i];
            Ops[i].D *= OpsTime[i];
            A[Ops[i].L] += Ops[i].D;
            A[Ops[i].R + 1] -= Ops[i].D;
        }
        for (int i = 1; i < N; ++i) A[i] += A[i - 1];
        bool head = true;
        for (int i = 0; i < N; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%lld", A[i]);
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
