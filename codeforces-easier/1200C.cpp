#include <cstdio>
using namespace std;

typedef long long int64;

int64 GCD(int64 a, int64 b) {
    if (b == 0) return a;
    return GCD(b, a % b);
}

struct Solution {
    int64 N, M, G;
    int Q;

    void Solve() {
        scanf("%lld%lld%d", &N, &M, &Q);
        G = GCD(N, M);
        for (int i = 0; i < Q; ++i) {
            int sx, ex;
            int64 sy, ey;
            scanf("%d%lld%d%lld", &sx, &sy, &ex, &ey), --sy, --ey;
            sy /= (sx == 1 ? N : M) / G;
            ey /= (ex == 1 ? N : M) / G;
            if (sy == ey) {
                printf("YES\n");
            } else {
                printf("NO\n");
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
