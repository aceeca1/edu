#include <cstdio>
using namespace std;

struct Solution {
    int N;

    int InvalidNum(int n) {
        int answer = 0;
        while (n != 0) {
            answer += (1 < n % 10);
            n /= 10;
        }
        return answer;
    }

    void Solve() {
        scanf("%d", &N);
        ++N;
        int invalidNum = InvalidNum(N);
        int depth = 0, answer = 0;
        while (N != 0) {
            invalidNum -= (1 < N % 10);
            if (invalidNum == 0 && 0 < N % 10) answer += 1 << depth;
            if (invalidNum == 0 && 1 < N % 10) answer += 1 << depth;
            N /= 10;
            ++depth;
        }
        printf("%d\n", answer - 1);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
