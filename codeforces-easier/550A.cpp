#include <cstdio>
#include <cstring>
#include <string>
using namespace std;

char Buffer[100001];

struct Solution {
    string S;
    
    bool Find(const char* s, const char* s1, const char* s2) {
        const char *s3 = strstr(s, s1);
        if (!s3) return false;
        s3 += strlen(s1);
        const char *s4 = strstr(s3, s2);
        return s4;
    }

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        bool answer = false;
        if (Find(S.c_str(), "AB", "BA")) answer = true;
        if (Find(S.c_str(), "BA", "AB")) answer = true;
        printf(answer ? "YES\n" : "NO\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
