#include <algorithm>
#include <cstdio>
#include <string>
using namespace std;

char Buffer[100001];

struct Solution {
    string S, S1, S2;

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        S1.reserve(S.size());
        int one = 0;
        for (int i = 0; i < S.size(); ++i) {
            if (S[i] == '1') {
                ++one;
            } else {
                S1 += S[i];
            }
        }
        int p = 0;
        while (p < S1.size() && S1[p] == '0') ++p;
        S2 = S1.substr(0, p);
        S2.reserve(S.size());
        for (int i = 0; i < one; ++i) S2 += '1';
        S2 += S1.substr(p);
        printf("%s\n", S2.c_str());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
