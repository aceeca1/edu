#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> A;

    void Solve() {
        scanf("%d", &N);
        A.resize(5);
        for (int i = 0; i < N; ++i) {
            int s;
            scanf("%d", &s);
            ++A[s];
        }
        int answer = A[4] + A[3] + (A[2] + 1) / 2;
        A[1] -= A[3];
        if (A[2] % 2 != 0) A[1] -= 2;
        if (A[1] < 0) A[1] = 0;
        answer += (A[1] + 3) / 4;
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
