#include <cstdio>
#include <map>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<pair<int, int>> A;

    template <class T, class Get>
    long long Compute(Get get) {
        map<T, int> map0;
        for (int i = 0; i < A.size(); ++i) ++map0[get(A[i])];
        long long answer = 0;
        for (
            typename map<T, int>::iterator it = map0.begin();
            it != map0.end();
            ++it
        ) answer += (long long)it->second * (it->second - 1) / 2;
        return answer;
    }

    static int GetFirst(const pair<int, int>& p) { return p.first; }
    static int GetSecond(const pair<int, int>& p) { return p.second; }
    static const pair<int, int>& GetWhole(const pair<int, int>& p) { return p; }

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) {
            int x, y;
            scanf("%d%d", &x, &y);
            A[i] = make_pair(x, y);
        }
        long long A1 = Compute<int>(GetFirst);
        long long A2 = Compute<int>(GetSecond);
        long long A3 = Compute<pair<int, int> >(GetWhole);
        printf("%lld\n", A1 + A2 - A3);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
