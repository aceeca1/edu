#include <cstdio>
using namespace std;

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        if (N <= 3) {
            printf("NO\n");
        } else if (N % 2 == 0) {
            printf("YES\n");
            printf("1 * 2 = 2\n");
            printf("2 * 3 = 6\n");
            printf("6 * 4 = 24\n");
            for (int i = 5; i < N; i += 2) {
                printf("%d - %d = 1\n", i + 1, i);
                printf("24 * 1 = 24\n");
            }
        } else {
            printf("YES\n");
            printf("1 * 2 = 2\n");
            printf("3 + 4 = 7\n");
            printf("7 + 5 = 12\n");
            printf("2 * 12 = 24\n");
            for (int i = 6; i < N; i += 2) {
                printf("%d - %d = 1\n", i + 1, i);
                printf("24 * 1 = 24\n");
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
