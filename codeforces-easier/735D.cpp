#include <cstdio>
using namespace std;

bool Prime(int n) {
    for (int i = 2; i * i <= n; ++i) {
        if (n % i == 0) return false;
    }
    return true;
}

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        if (Prime(N)) {
            printf("1\n");
        } else if (N % 2 == 0 || Prime(N - 2)) {
            printf("2\n");
        } else {
            printf("3\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
