#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<vector<int> > G;

    double ExpectLength(int no, int parent) {
        double total = 0.0;
        int leaves = 0;
        for (int i = 0; i < G[no].size(); ++i) {
            if (G[no][i] == parent) continue;
            ++leaves;
            total += ExpectLength(G[no][i], no) + 1.0;
        }
        if (leaves == 0) return 0;
        return total / leaves;
    }

    void Solve() {
        scanf("%d", &N);
        G.resize(N);
        for (int i = 1; i < N; ++i) {
            int v1, v2;
            scanf("%d%d", &v1, &v2);
            G[v1 - 1].push_back(v2 - 1);
            G[v2 - 1].push_back(v1 - 1);
        }
        printf("%.12f\n", ExpectLength(0, -1));
    }
};

int main() {
    Solution().Solve();
    return 0;
}
