#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> AllPrimes, Answer;
    vector<bool> Composite;

    void Generate() {
        Composite.resize(N + 1);
        for (int i = 2; i <= N; ++i) {
            if (!Composite[i]) {
                AllPrimes.push_back(i);
                for (long long j = (long long)i * i; j <= N; j += i) {
                    Composite[j] = true;
                }
            }
        }
    }

    void Solve() {
        scanf("%d", &N);
        Generate();
        for (int i = 0; i < AllPrimes.size(); ++i) {
            for (int j = AllPrimes[i]; j <= N; j *= AllPrimes[i]) {
                Answer.push_back(j);
            }
        }
        printf("%d\n", Answer.size());
        bool head = true;
        for (int i = 0; i < Answer.size(); ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", Answer[i]);
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
