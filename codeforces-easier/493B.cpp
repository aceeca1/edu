#include <cstdio>
#include <string>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int N, Last;
    vector<int> B1, B2;

    int64 Sum(const vector<int>& v) {
        int64 answer = 0;
        for (int i = 0; i < v.size(); ++i) {
            answer += v[i];
        }
        return answer;
    }

    string Winner() {
        int64 sum1 = Sum(B1);
        int64 sum2 = Sum(B2);
        if (sum2 < sum1) return "first";
        if (sum1 < sum2) return "second";
        for (int i = 0; ; ++i) {
            if (B1.size() <= i && B2.size() <= i) break;
            if (B1.size() <= i) return "second";
            if (B2.size() <= i) return "first";
            if (B1[i] < B2[i]) return "second";
            if (B2[i] < B1[i]) return "first";
        }
        if (Last == 1) return "first";
        if (Last == 2) return "second";
        return string();
    }

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            int a;
            scanf("%d", &a);
            if (0 < a) {
                B1.push_back(a);
                Last = 1;
            } else {
                B2.push_back(-a);
                Last = 2;
            }
        }
        printf("%s\n", Winner().c_str());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
