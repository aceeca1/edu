#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> A;
    vector<vector<char> > Adjacent;
    vector<bool> Visited;
    vector<int> CurrentComponent;

    void DFS(int no) {
        Visited[no] = true;
        CurrentComponent.push_back(no);
        for (int i = 0; i < N; ++i) {
            if (Adjacent[no][i] == '1' && !Visited[i]) DFS(i); 
        }
    }

    void SortCurrentComponent() {
        sort(CurrentComponent.begin(), CurrentComponent.end());
        vector<int> b(CurrentComponent.size());
        for (int i = 0; i < b.size(); ++i) b[i] = A[CurrentComponent[i]];
        sort(b.begin(), b.end());
        for (int i = 0; i < b.size(); ++i) A[CurrentComponent[i]] = b[i];
    }

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        Adjacent.resize(N);
        for (int i = 0; i < N; ++i) {
            Adjacent[i].resize(N);
            for (int j = 0; j < N; ++j) {
                scanf(" %c", &Adjacent[i][j]);
            }
        }
        Visited.resize(N);
        for (int i = 0; i < N; ++i) {
            if (!Visited[i]) {
                DFS(i);
                SortCurrentComponent();
                CurrentComponent.clear();
            }
        }
        bool head = true;
        for (int i = 0; i < N; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", A[i]);
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
