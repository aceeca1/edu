#include <cstdio>
using namespace std;

struct Solution {
    int N, K;

    void Solve() {
        scanf("%d%d", &N, &K);
        int answer = N + N - K + 1;
        for (int i = 0; i < K; ++i) {
            int length;
            scanf("%d", &length);
            for (int j = 0; j < length; ++j) {
                int a;
                scanf("%d", &a);
                if (a == j + 1) answer -= 2;
            }
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
