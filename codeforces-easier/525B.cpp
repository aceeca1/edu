#include <algorithm>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[200001];

struct Solution {
    string S;
    vector<bool> Reversed;
    int M, A;

    void Solve() {
        scanf("%s%d", Buffer, &M);
        S = Buffer;
        Reversed.resize(S.size() / 2);
        for (int i = 0; i < M; ++i) {
            scanf("%d", &A);
            Reversed[A - 1] = Reversed[A - 1] ^ 1;
        }
        for (int i = 1; i < Reversed.size(); ++i) {
            Reversed[i] = Reversed[i] ^ Reversed[i - 1];
        }
        for (int i1 = 0; i1 < S.size(); ++i1) {
            int i2 = S.size() - 1 - i1;
            int iMin = min(i1, i2);
            if (iMin < Reversed.size() && Reversed[iMin]) {
                putchar(S[i2]);
            } else {
                putchar(S[i1]);
            }
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}