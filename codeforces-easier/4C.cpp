#include <cstdio>
#include <map>
#include <string>
using namespace std;

typedef map<string, int>::iterator Type1;
typedef pair<Type1, bool> Type2;

char Buffer[33];

struct Solution {
    int N;
    map<string, int> H;

    string UniqueName(const string& s) {
        Type2 result = H.insert(make_pair(s, 0));
        if (result.second) return "OK";
        sprintf(Buffer, "%d", ++result.first->second);
        return s + Buffer;
    }

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            scanf("%s", Buffer);
            printf("%s\n", UniqueName(Buffer).c_str());
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
