#include <cstdio>
using namespace std;

struct Solution {
    int N, K;

    void Solve() {
        scanf("%d%d", &N, &K);
        int n1 = N / 2;
        if (N == 1) {
            printf(K == 0 ? "1\n" : "-1\n");
        } else if (K < n1) {
            printf("-1\n");
        } else {
            int a1 = K - (n1 - 1);
            printf("%d %d", a1 * 2, a1 * 3);
            const int m = 400000000;
            for (int i = 1; i < n1; ++i) {
                printf(" %d %d", m + i + i, m + i + i + 1);
            }
            if (N % 2 != 0) printf(" 1");
            printf("\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
