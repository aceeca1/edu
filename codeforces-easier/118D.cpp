#include <cstdio>
#include <vector>
using namespace std;

const int M = 100000000;

struct Solution {
    int N1, N2, K1, K2;
    vector<vector<int> > F1, F2;

    void Solve() {
        scanf("%d%d%d%d", &N1, &N2, &K1, &K2);
        F1.resize(N1 + 1);
        F2.resize(N1 + 1);
        for (int i = 0; i <= N1; ++i) {
            F1[i].resize(N2 + 1);
            F2[i].resize(N2 + 1);
            if (i == 0) F1[i][0] = F2[i][0] = 1;
            for (int j = 0; j <= N2; ++j) {
                for (int k = 1; k <= K1; ++k) {
                    if (i - k < 0) continue;
                    F1[i][j] = (F1[i][j] + F2[i - k][j]) % M;
                }
                for (int k = 1; k <= K2; ++k) {
                    if (j - k < 0) continue;
                    F2[i][j] = (F2[i][j] + F1[i][j - k]) % M;
                }
            }
        }
        printf("%d\n", (F1[N1][N2] + F2[N1][N2]) % M);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
