#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, K;
    vector<int> A;

    void Solve() {
        scanf("%d%d", &N, &K);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        int answer = 0;
        for (int i = 0; i < N; ++i) {
            if (0 < A[i] && A[K - 1] <= A[i]) ++answer;
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
