#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int N;
    vector<int> A;

    bool CanDecrease() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        int64 sum = 0, max = *max_element(A.begin(), A.end());
        for (int i = 0; i < N; ++i) sum += A[i];
        if (sum % 2 != 0) return false;
        if (max + max > sum) return false;
        return true;
    }

    void Solve() {
        printf(CanDecrease() ? "YES\n" : "NO\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
