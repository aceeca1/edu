#include <cstdio>
using namespace std;

struct Solution {
    int N, A, G;

    void Solve() {
        scanf("%d", &N);
        int difference = 0;
        for (int i = 0; i < N; ++i) {
            scanf("%d%d", &A, &G);
            if (difference + A <= 500) {
                putchar('A');
                difference += A;
            } else {
                putchar('G');
                difference -= G;
            }
        }
        putchar('\n');
    }
};

int main() {
    Solution().Solve();
    return 0;
}
