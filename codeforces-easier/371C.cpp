#include <algorithm>
#include <cstdio>
using namespace std;

char Buffer[101];

typedef long long int64;

struct Solution {
    int NeedB, NeedS, NeedC;
    int HaveB, HaveS, HaveC;
    int PriceB, PriceS, PriceC;
    int64 Money;

    void Solve() {
        scanf("%s", Buffer);
        NeedB = NeedS = NeedC = 0;
        for (int i = 0; Buffer[i]; ++i) {
            switch (Buffer[i]) {
                case 'B': ++NeedB; break;
                case 'S': ++NeedS; break;
                case 'C': ++NeedC; break;
            }
        }
        scanf("%d%d%d", &HaveB, &HaveS, &HaveC);
        scanf("%d%d%d", &PriceB, &PriceS, &PriceC);
        scanf("%lld", &Money);
        int64 answer = 0;
        while (HaveB && NeedB || HaveS && NeedS || HaveC && NeedC) {
            int64 needMoneyB = max(NeedB - HaveB, 0) * PriceB;
            int64 needMoneyS = max(NeedS - HaveS, 0) * PriceS;
            int64 needMoneyC = max(NeedC - HaveC, 0) * PriceC;
            int64 needMoney = needMoneyB + needMoneyS + needMoneyC;
            if (Money < needMoney) break;
            ++answer;
            Money -= needMoney;
            HaveB = HaveB + needMoneyB / PriceB - NeedB;
            HaveS = HaveS + needMoneyS / PriceS - NeedS;
            HaveC = HaveC + needMoneyC / PriceC - NeedC;
        }
        int64 eachMoneyB = NeedB * PriceB;
        int64 eachMoneyS = NeedS * PriceS;
        int64 eachMoneyC = NeedC * PriceC;
        int64 eachMoney = eachMoneyB + eachMoneyS + eachMoneyC;
        printf("%lld\n", answer + Money / eachMoney);
    }
};

int main() {
    Solution().Solve();
    return 0;
}