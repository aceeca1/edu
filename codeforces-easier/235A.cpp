#include <cstdio>
using namespace std;

typedef long long int64;

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        if (N == 1) {
            printf("1\n");
        } else if (N == 2) {
            printf("2\n");
        } else if (N == 3) {
            printf("6\n");
        } else if (N == 4) {
            printf("12\n");
        } else if (N % 2 != 0) {
            printf("%lld\n", int64(N) * (N - 1) * (N - 2));
        } else if (N % 3 != 0) {
            printf("%lld\n", int64(N) * (N - 1) * (N - 3));
        } else {
            printf("%lld\n", int64(N - 1) * (N - 2) * (N - 3));
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
