#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[1000001];

struct Solution {
    string S;
    vector<int> Next;

    int GetAnswer() {
        if (Next[S.size()] == 0) return 0;
        for (int i = 0; i < S.size(); ++i) {
            if (Next[i] == Next[S.size()]) return Next[S.size()];
        }
        if (Next[Next[S.size()]] == 0) return 0;
        return Next[Next[S.size()]];
    }

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        Next.resize(S.size() + 1);
        int k = Next[0] = -1;
        for (int i = 0; i < S.size(); ++i) {
            while (k >= 0 && S[k] != S[i]) k = Next[k];
            Next[i + 1] = ++k;
        }
        int answer = GetAnswer();
        if (answer == 0) {
            printf("Just a legend\n");
        } else {
            printf("%s\n", S.substr(0, answer).c_str());
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
