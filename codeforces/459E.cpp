#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

const int W = 100000;

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Edge {
    int U, V;
};

struct Solution {
    int N, M;
    vector<vector<Edge> > E;
    vector<int> L, LNew;
    
    void Solve() {
        scanf("%d%d", &N, &M);
        E.resize(W + 1);
        for (int i = 0; i < M; ++i) {
            int u, v, w;
            scanf("%d%d%d", &u, &v, &w);
            Edge edge = {u, v};
            E[w].push_back(edge);
        }
        L.resize(N + 1);
        LNew.resize(N + 1);
        for (int i = 0; i < E.size(); ++i) {
            for (int j = 0; j < E[i].size(); ++j) {
                int u = E[i][j].U, v = E[i][j].V;
                AssignMax(&LNew[v], L[u] + 1);
            }
            for (int j = 0; j < E[i].size(); ++j) {
                int u = E[i][j].U, v = E[i][j].V;
                AssignMax(&L[v], LNew[v]);
            }
        }
        int answer = *max_element(L.begin(), L.end());
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
