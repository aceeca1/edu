#include <cstdio>
#include <vector>
using namespace std;

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

void AssignMax2(int* p1, int* p2, int v) {
    if (*p1 < v) {
        *p2 = *p1;
        *p1 = v;
    } else if (*p2 < v) {
        *p2 = v;
    }
}

struct UnionFind {
    vector<int> Parent, Size;

    void SetSize(int n) {
        Parent.resize(n);
        Size.resize(n);
        for (int i = 0; i < n; ++i) {
            Parent[i] = i;
            Size[i] = 1;
        }
    }

    int Union(int u1, int u2) {
        int root1 = Find(u1);
        int root2 = Find(u2);
        if (root1 == root2) return root1;
        if (Size[root1] < Size[root2]) {
            Parent[root1] = root2;
            Size[root2] += Size[root1];
            return root2;
        } else {
            Parent[root2] = root1;
            Size[root1] += Size[root2];
            return root1;
        }
    }

    int Find(int u) {
        if (Parent[u] != u) Parent[u] = Find(Parent[u]);
        return Parent[u];
    }
};

struct Solution {
    int N, M, Q;
    vector<vector<int> > Out;
    vector<int> Diameter;
    UnionFind U;

    void Compute(int no, int parent, int* diameter, int* depth) {
        int maxDepth1 = 0, maxDepth2 = 0;
        *diameter = 0;
        for (int i = 0; i < Out[no].size(); ++i) {
            int target = Out[no][i];
            if (target == parent) continue;
            int diameter0, depth0;
            Compute(target, no, &diameter0, &depth0);
            AssignMax(diameter, diameter0);
            AssignMax2(&maxDepth1, &maxDepth2, depth0 + 1);
        }
        AssignMax(diameter, maxDepth1 + maxDepth2);
        if (depth) *depth = maxDepth1;
    }

    void Solve() {
        scanf("%d%d%d", &N, &M, &Q);
        U.SetSize(N);
        Out.resize(N);
        for (int i = 0; i < M; ++i) {
            int u1, u2;
            scanf("%d%d", &u1, &u2);
            U.Union(u1 - 1, u2 - 1);
            Out[u1 - 1].push_back(u2 - 1);
            Out[u2 - 1].push_back(u1 - 1);
        }
        Diameter.resize(N);
        for (int i = 0; i < N; ++i) Diameter[i] = -1;
        for (int i = 0; i < N; ++i) {
            int component = U.Find(i);
            if (Diameter[component] == -1) {
                Compute(component, -1, &Diameter[component], NULL);
            }
        }
        for (int i = 0; i < Q; ++i) {
            int op;
            scanf("%d", &op);
            switch (op) {
                case 1: {
                    int u;
                    scanf("%d", &u);
                    printf("%d\n", Diameter[U.Find(u - 1)]);
                    break;
                }
                case 2: {
                    int u1, u2;
                    scanf("%d%d", &u1, &u2);
                    int root1 = U.Find(u1 - 1);
                    int root2 = U.Find(u2 - 1);
                    if (root1 == root2) break;
                    int diameter1 = Diameter[root1];
                    int diameter2 = Diameter[root2];
                    int root = U.Union(u1 - 1, u2 - 1);
                    int diameter3 = 1 +
                        (diameter1 + 1) / 2 + (diameter2 + 1) / 2;
                    Diameter[root] = max(max(diameter1, diameter2), diameter3);
                    break;
                }
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
