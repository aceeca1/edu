#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int R, G;

    void Solve() {
        scanf("%d%d", &R, &G);
        int s = R + G;
        int h = 1;
        while (h <= s) s -= h++;
        --h;
        int minimum = R - s;
        int maximum = R;
        vector<int> f(maximum + 1);
        f[0] = 1;
        for (int i = 1; i <= h; ++i) {
            for (int j = maximum; j >= i; --j) {
                f[j] = (f[j] + f[j - i]) % 1000000007;
            }
        }
        int answer = 0;
        for (int i = max(minimum, 0); i <= maximum; ++i) {
            answer = (answer + f[i]) % 1000000007;
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
