#include <algorithm>
#include <cstdio>
#include <climits>
#include <vector>
using namespace std;

struct Segment {
    int Left, Right;

    bool operator<(const Segment& that) {
        return Right < that.Right;
    }
};

struct Solution {
    int N;
    vector<Segment> A;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) {
            int x, w;
            scanf("%d%d", &x, &w);
            A[i].Left = x - w;
            A[i].Right = x + w;
        }
        sort(A.begin(), A.end());
        int x = INT_MIN, answer = 0;
        for (int i = 0; i < N; ++i) {
            if (x <= A[i].Left) {
                x = A[i].Right;
                ++answer;
            }
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
