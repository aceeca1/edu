#include <cstdio>
#include <climits>
#include <map>
#include <vector>
using namespace std;

struct Edge {
    int Target;
    long long U;
};

struct Graph {
    vector<Edge> Edges;
    vector<vector<int> > Out;
    int Source, Target;
    vector<int> H, Begin, SizeH;
    vector<long long> Excess;
    multimap<int, int> Q;

    void AddEdge(int s, int t, long long u) {
        Edge e1 = {t, u}, e2 = {s, 0};
        Edges.push_back(e1);
        Edges.push_back(e2);
        Out[s].push_back(Edges.size() - 2);
        Out[t].push_back(Edges.size() - 1);
    }

    void Push(int no, int ei) {
        int target = Edges[ei].Target;
        if (!(no == Source || H[target] + 1 == H[no])) return;
        long long delta = min(Excess[no], Edges[ei].U);
        if (delta == 0) return;
        Excess[no] -= delta;
        Excess[target] += delta;
        Edges[ei].U -= delta;
        Edges[ei ^ 1].U += delta;
        if (target == Source || target == Target) return;
        Q.insert(make_pair(H[target], target));
    }

    void Relabel(int no) {
        if (no == Source || no == Target) return;
        Begin[no] = 0;
        if (--SizeH[H[no]] == 0) {
            for (int i = 0; i < Out.size(); ++i) {
                if (H[no] < H[i]) H[i] = Out.size();
            }
        }
        Q.insert(make_pair(++H[no], no));
        ++SizeH[H[no]];
    }

    long long MaxFlow() {
        H.resize(Out.size());
        H[Source] = Out.size();
        SizeH.resize(Out.size() + Out.size() + 1);
        SizeH[0] = Out.size() - 1;
        SizeH[Out.size()] = 1;
        Begin.resize(Out.size());
        Excess.resize(Out.size());
        Excess[Source] = LONG_LONG_MAX;
        Q.insert(make_pair(H[Source], Source));
        while (!Q.empty()) {
            int no = Q.rbegin()->second;
            Q.erase(--Q.rbegin().base());
            if (Excess[no] == 0) continue;
            for (int& i = Begin[no]; i < Out[no].size(); ++i) {
                Push(no, Out[no][i]);
                if (Excess[no] == 0) break;
            }
            if (0 < Excess[no]) Relabel(no);
        }
        return Excess[Target];
    }
};

struct Solution {
    int N, M;
    Graph G;

    void Solve() {
        scanf("%d%d", &N, &M);
        G.Out.resize(N + N + 2);
        G.Source = N + N;
        G.Target = N + N + 1;
        int sumA = 0, sumB = 0;
        for (int i = 0; i < N; ++i) {
            int u;
            scanf("%d", &u);
            G.AddEdge(N + N, i, u);
            G.AddEdge(i, N + i, INT_MAX);
            sumA += u;
        }
        for (int i = 0; i < N; ++i) {
            int u;
            scanf("%d", &u);
            G.AddEdge(N + i, N + N + 1, u);
            sumB += u;
        }
        for (int i = 0; i < M; ++i) {
            int s, t;
            scanf("%d%d", &s, &t);
            G.AddEdge(s - 1, N + (t - 1), INT_MAX);
            G.AddEdge(t - 1, N + (s - 1), INT_MAX);
        }
        if (sumA == sumB && sumA == G.MaxFlow()) {
            printf("YES\n");
            for (int i = 0; i < N; ++i) {
                vector<int> output(N);
                for (int j = 0; j < G.Out[i].size(); ++j) {
                    int target = G.Edges[G.Out[i][j]].Target;
                    output[target - N] += G.Edges[G.Out[i][j] ^ 1].U;
                }
                bool head = true;
                for (int j = 0; j < N; ++j) {
                    if (!head) putchar(' ');
                    head = false;
                    printf("%d", output[j]);
                }
                printf("\n");
            }
        } else {
            printf("NO\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
