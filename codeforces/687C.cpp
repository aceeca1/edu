#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, K;
    vector<int> C;
    vector<vector<bool> > F;

    void Solve() {
        scanf("%d%d", &N, &K);
        C.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &C[i]);
        F.resize(K + 1);
        for (int i = 0; i < F.size(); ++i) {
            F[i].resize(K + 1);
        }
        F[0][0] = true;
        for (int i = 0; i < N; ++i) {
            for (int j = K; j >= C[i]; --j) {
                for (int k = K; k >= 0; --k) {
                    F[j][k] = F[j][k] | F[j - C[i]][k];
                    if (C[i] <= k) F[j][k] = F[j][k] | F[j - C[i]][k - C[i]];
                }
            }
        }
        int answer = 0;
        for (int i = 0; i <= K; ++i) {
            if (F[K][i]) ++answer;
        }
        printf("%d\n", answer);
        bool head = true;
        for (int i = 0; i <= K; ++i) {
            if (F[K][i]) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", i);
            }
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
