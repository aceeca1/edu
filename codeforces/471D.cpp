#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, W;
    vector<int> A, B;

    int Find(const vector<int> s, const vector<int>& s1) {
        if (s1.empty()) return s.size() + 1;
        vector<int> next(s1.size() + 1);
        int k = next[0] = -1;
        for (int i = 0; i < s1.size(); ++i) {
            while (k >= 0 && s1[k] != s1[i]) k = next[k];
            next[i + 1] = ++k;
        }
        k = 0;
        int answer = 0;
        for (int i = 0; i < s.size(); ++i) {
            while (k >= 0 && s1[k] != s[i]) k = next[k];
            if (++k == s1.size()) {
                ++answer;
                k = next[k];
            }
        }
        return answer;
    }

    void Solve() {
        scanf("%d%d", &N, &W);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        B.resize(W);
        for (int i = 0; i < W; ++i) scanf("%d", &B[i]);
        for (int i = 0; i < N - 1; ++i) A[i] = A[i + 1] - A[i];
        for (int i = 0; i < W - 1; ++i) B[i] = B[i + 1] - B[i];
        A.pop_back();
        B.pop_back();
        printf("%d\n", Find(A, B));
    }
};

int main() {
    Solution().Solve();
    return 0;
}
