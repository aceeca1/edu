#include <cstdio>
#include <string>
#include <vector>
using namespace std;

typedef long long int64;

char Buffer[5001];

struct Solution {
    string S;
    vector<int64> A;
    vector<string> D;

    bool Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        if (S == "0") return false;
        for (int i = 1; i <= 26; ++i) {
            sprintf(Buffer, "%d", i);
            D.push_back(Buffer);
        }
        A.resize(S.size() + 1);
        A[0] = 1;
        for (int i = 0; i < S.size(); ++i) {
            for (int j = 0; j < D.size(); ++j) {
                if (D[j] == S.substr(i, D[j].size())) {
                    A[i + D[j].size()] += A[i];
                }
            }
        }
        printf("%lld\n", A[S.size()]);
        return true;
    }
};

int main() {
    while (Solution().Solve())
        ;
    return 0;
}
