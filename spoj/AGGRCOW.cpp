#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, C;
    vector<int> X;

    bool P(int mid) {
        int last = 0, total = 1;
        for (int i = 1; i < N; ++i) {
            if (mid <= X[i] - X[last]) {
                ++total;
                last = i;
            }
        }
        return total < C;
    }

    int BinarySearch(int lower, int upper) {
        while (lower < upper) {
            int mid = lower + (upper - lower) / 2;
            if (P(mid)) {
                upper = mid;
            } else {
                lower = mid + 1;
            }
        }
        return lower;
    }

    void Solve() {
        scanf("%d%d", &N, &C);
        X.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &X[i]);
        sort(X.begin(), X.end());
        int answer = BinarySearch(1, X.back() + 1) - 1;
        printf("%d\n", answer);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
