#include <algorithm>
#include <cmath>
#include <cstdio>
#include <limits>
#include <vector>
using namespace std;

const int kMin = numeric_limits<int>::min() / 2;

struct Segment {
    int Sum, MaxSum, MaxSumL, MaxSumR;

    static Segment Single(int n) {
        Segment answer = {n, n, n, n};
        return answer;
    }

    static Segment Merge(const Segment& sL, const Segment& sR) {
        int sum = sL.Sum + sR.Sum;
        int maxSumL = max(sL.MaxSumL, sL.Sum + sR.MaxSumL);
        int maxSumR = max(sR.MaxSumR, sR.Sum + sL.MaxSumR);
        int maxSum = max(sL.MaxSumR + sR.MaxSumL, max(sL.MaxSum, sR.MaxSum));
        Segment answer = {sum, maxSum, maxSumL, maxSumR};
        return answer;
    }
};

int ILogB(int n) {
    int answer = 0;
    while (n != 1) {
        n /= 2;
        ++answer;
    }
    return answer;
}

struct SegTree {
    int N, K, M;
    vector<Segment> A;

    void Resize(int n) {
        N = n;
        K = ILogB(n + 2) + 1;
        M = 1 << K;
        A.resize(M + M);
    }

    void Build() {
        for (int i = M - 1; i >= 1; --i) {
            A[i] = Segment::Merge(A[i + i], A[i + i + 1]);
        }
    }

    int Query(int pL, int pR) {
        pL += M - 1;
        pR += M + 1;
        Segment answerL = {0, kMin, kMin, kMin};
        Segment answerR = {0, kMin, kMin, kMin};
        while ((pL ^ pR) != 1) {
            if ((pL & 1) == 0) answerL = Segment::Merge(answerL, A[pL ^ 1]);
            if ((pR & 1) != 0) answerR = Segment::Merge(A[pR ^ 1], answerR);
            pL >>= 1;
            pR >>= 1;
        }
        return Segment::Merge(answerL, answerR).MaxSum;
    }
};

struct Solution {
    int N, M;
    SegTree tree;

    void Solve() {
        scanf("%d", &N);
        tree.Resize(N);
        for (int i = tree.M + 1; i <= tree.M + N; ++i) {
            int x;
            scanf("%d", &x);
            tree.A[i] = Segment::Single(x);
        }
        tree.Build();
        scanf("%d", &M);
        for (int i = 0; i < M; ++i) {
            int pL, pR;
            scanf("%d%d", &pL, &pR);
            printf("%d\n", tree.Query(pL, pR));
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
