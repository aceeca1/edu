#include <cstdio>
#include <map>
using namespace std;

typedef long long int64;
typedef map<int, int64>::iterator Type1;
typedef pair<Type1, bool> Type2;

struct Solution {
    int N;
    map<int, int64> Cache;

    int64 F(int x) {
        Type2 result = Cache.insert(make_pair(x, 0LL));
        if (result.second) {
            int64 answer = F(x / 2) + F(x / 3) + F(x / 4);
            if (answer < x) answer = x;
            result.first->second = answer;
        }
        return result.first->second;
    }

    bool Solve() {
        if (scanf("%d", &N) < 1) return false;
        printf("%lld\n", F(N));
        return true;
    }
};

int main() {
    while (Solution().Solve())
        ;
    return 0;
}
