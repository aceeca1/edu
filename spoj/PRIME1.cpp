#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Common {
    vector<bool> Composite;

    void Sieve(vector<bool>* v, int64 vStart) {
        for (int i = 2; i < Composite.size(); ++i) {
            if (Composite[i]) continue;
            int64 start = max(int64(i) * i, (vStart + i - 1) / i * i);
            int64 stop = vStart + v->size();
            for (int64 j = start; j < stop; j += i) (*v)[j - vStart] = true;
        }
    }

    void Prepare() {
        Composite.resize(65536);
        Sieve(&Composite, 0);
    }
};

struct Solution {
    Common* CommonInstance;
    int M, N;

    void Solve() {
        scanf("%d%d", &M, &N);
        vector<bool> v(N - M + 1);
        CommonInstance->Sieve(&v, M);
        for (int i = 0; i < v.size(); ++i) {
            if (!v[i] && 2 <= M + i) printf("%d\n", M + i);
        }
    }
};

int main() {
    Common common;
    common.Prepare();
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) {
        if (i != 0) printf("\n");
        Solution solution = {&common};
        solution.Solve();
    }
    return 0;
}
