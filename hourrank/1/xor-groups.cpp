#include <cstdio>
using namespace std;

struct Solution {
    int N, Total;

    void Solve() {
        scanf("%d", &N);
        Total = 0;
        for (int i = 0; i < N; ++i) {
            int input;
            scanf("%d", &input);
            Total ^= input;
        }
        if (Total == 0) {
            int output = 1;
            for (int i = 1; i < N; ++i) {
                output = (output << 1) % 1000000007;
            }
            printf("%d\n", output - 1);
        } else {
            printf("0\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
