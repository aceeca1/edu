#include <algorithm>
#include <cstdio>
#include <climits>
#include <vector>
using namespace std;

bool AssignMin(int *p, int v) {
    if (v < *p) return *p = v, true;
    return false;
}

struct Solution {
    int N, K, L;
    vector<int> A;

    int RequiredCoins(int target) {
        int needIncrease = 0, needDecrease = 0;
        for (int i = 0; i < N; ++i) {
            if (A[i] < target) {
                needIncrease += target - A[i];
            } else {
                needDecrease += A[i] - target;
            }
        }
        if (needIncrease < needDecrease) return INT_MAX;
        return needDecrease * (K - L) + needIncrease * L;
    }

    int MinCoins() {
        int answer = INT_MAX;
        int maxTarget = *max_element(A.begin(), A.end());
        for (int i = maxTarget; i >= 0; --i) {
            AssignMin(&answer, RequiredCoins(i));
        }
        return answer;
    }

    void Solve() {
        scanf("%d%d%d", &N, &K, &L);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        printf("%d\n", MinCoins());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
