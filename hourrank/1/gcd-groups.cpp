#include <cstdio>
#include <set>
#include <vector>
using namespace std;

typedef long long int64;
typedef set<int>::iterator Type1;

vector<int> Factorize(int n) {
    vector<int> answer;
    for (int i = 2; int64(i) * i <= n; ++i) {
        while (n % i == 0) {
            n /= i;
            answer.push_back(i);
        }
    }
    if (1 < n) answer.push_back(n);
    return answer;
}

set<int> Distinct(const vector<int>& v) {
    return set<int>(v.begin(), v.end());
}

int GCD(int a1, int a2) {
    if (a2 == 0) return a1;
    return GCD(a2, a1 % a2);
}

struct Solution {
    int N;
    vector<int> A;

    bool Divide() {
        set<int> a0 = Distinct(Factorize(A[0]));
        for (Type1 it = a0.begin(); it != a0.end(); ++it) {
            int remainCount = 0, gcd = 0;
            for (int i = 0; i < N; ++i) {
                if (A[i] % *it != 0) ++remainCount;
            }
            if (remainCount == 0) return true;
            if (N / 2 < remainCount) continue;
            for (int i = 0; i < N; ++i) {
                if (A[i] % *it != 0) gcd = GCD(gcd, A[i]);
            }
            set<int> s = Distinct(Factorize(gcd));
            for (Type1 it1 = s.begin(); it1 != s.end(); ++it1) {
                int count = 0;
                for (int i = 0; i < N; ++i) {
                    if (A[i] % *it1 == 0) ++count;
                }
                if (N / 2 <= count) return true;
            }
        }
        return false;
    }

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        printf(Divide() ? "YES\n" : "NO\n");
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
