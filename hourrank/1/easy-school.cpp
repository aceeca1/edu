#include <algorithm>
#include <cstdio>
#include <climits>
#include <set>
#include <vector>
using namespace std;

typedef long long int64;

bool AssignMin(int64* p, int64 v) {
    if (v < *p) return *p = v, true;
    return false;
}

struct Teacher {
    int C, B;
};

struct MoreB {
    bool operator()(const Teacher& t1, const Teacher& t2) {
        return t2.B < t1.B;
    }
};

struct LessC {
    bool operator()(const Teacher& t1, const Teacher& t2) {
        return t1.C < t2.C;
    }
};

struct Set1 {
    vector<Teacher> S;
    int64 TotalCost;

    Set1(vector<Teacher>* teachers) {
        swap(S, *teachers);
        sort(S.begin(), S.end(), MoreB());
        TotalCost = 0;
        for (int i = 0; i < S.size(); ++i) TotalCost += S[i].C;
    }

    int Count() { return S.size(); }

    vector<Teacher> Pop(int b) {
        vector<Teacher> answer;
        while (!S.empty() && S.back().B == b) {
            answer.push_back(S.back());
            TotalCost -= S.back().C;
            S.pop_back();
        }
        return answer;
    }
};

struct Set2 {
    multiset<Teacher, LessC> S;

    void Add(const Teacher& teacher) {
        S.insert(teacher);
    }

    Teacher Pop() {
        Teacher answer = *S.begin();
        S.erase(S.begin());
        return answer;
    }
};

struct Set3 {
    int Count;
    int64 TotalCost;

    Set3() { Count = TotalCost = 0; }

    void Add(const Teacher& teacher) {
        ++Count;
        TotalCost += teacher.C;
    }
};

struct Solution {
    int N;
    vector<Teacher> Teachers;

    void Solve() {
        scanf("%d", &N);
        Teachers.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%d%d", &Teachers[i].C, &Teachers[i].B);
        }
        Set1 set1(&Teachers);
        Set2 set2;
        Set3 set3;
        int64 answer = LONG_LONG_MAX;
        for (int i = 0; i < N; ++i) {
            vector<Teacher> s = set1.Pop(i);
            for (int i = 0; i < s.size(); ++i) set2.Add(s[i]);
            if (set1.Count() <= i) {
                while (set1.Count() + set3.Count < i) set3.Add(set2.Pop());
                AssignMin(&answer, set1.TotalCost + set3.TotalCost);
            }
        }
        printf("%lld\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
