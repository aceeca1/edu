#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    vector<int> A;

    void Solve() {
        A.resize(5);
        for (int i = 0; i < 5; ++i) scanf("%d", &A[i]);
        int answer = 0;
        for (int i = 0; i < 5; ++i) {
            answer += max(0, 100 - 10 * (A[i] - (i + 1)));
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
