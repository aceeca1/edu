#include <cstdio>
using namespace std;

typedef long long int64;

int64 GCD(int64 a, int64 b) {
    if (b == 0) return a;
    return GCD(b, a % b);
}

struct Solution {
    int64 N, A, B;

    void Solve() {
        scanf("%lld%lld%lld", &N, &A, &B);
        for (int i = GCD(A, B); i > 0; --i) printf("%lld", N);
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
