#include <cmath>
#include <cstdio>
#include <climits>
using namespace std;

typedef long long int64;
typedef unsigned long long uint64;

uint64 Fingerprint(uint64 x) {
    const uint64 kMul = 0x9ddfea08eb382d69ULL;
    x *= kMul, x ^= x >> 47;
    x *= kMul, x ^= x >> 47;
    x *= kMul, x ^= x >> 47;
    return x * kMul;
}

int64 Random() {
    static uint64 Seed = 2;
    return Fingerprint(Seed++) & LONG_LONG_MAX;
}

int64 Random(int64 lower, int64 upper) {
    return Random() % (upper - lower + 1) + lower;
}

int64 ModMul(int64 a, int64 b, int64 m) {
    int64 answer = 0;
    while (b != 0) {
        if ((b & 1) != 0) answer = (answer + a) % m;
        a = (a + a) % m;
        b >>= 1;
    }
    return answer;
}

int64 ModPow(int64 a, int64 b, int64 m) {
    int64 answer = 1;
    while (b != 0) {
        if ((b & 1) != 0) answer = ModMul(answer, a, m);
        a = ModMul(a, a, m);
        b >>= 1;
    }
    return answer;
}

struct MillerRabin {
    int64 N, D, R;

    bool IsWitness(int64 a) {
        int64 x = ModPow(a, D, N);
        for (int i = 0; i < R; ++i) {
            if (x == 1) return i != 0;
            if (x == N - 1) return false;
            x = ModMul(x, x, N);
        }
        return true;
    }

    bool IsPrime() {
        if (N < 2) return false;
        if (N == 2) return true;
        if (N % 2 == 0) return false;
        D = N - 1;
        while (D % 2 == 0) {
            D /= 2;
            ++R;
        }
        for (int i = 0; i < 10; ++i) {
            if (IsWitness(Random(2, N - 1))) return false;
        }
        return true;
    }
};

int64 Root(int64 n, int64 k) {
    if (k == 1) return n;
    int64 answer = int64(round(pow(n, 1.0 / k)));
    int64 a = answer;
    for (int i = 1; i < k; ++i) a *= answer;
    if (a == n) return answer;
    return -1;
}

bool IsPrime(int64 n) {
    MillerRabin rabin = {n};
    return rabin.IsPrime();
}

bool IsPrimeSquare(int64 n) {
    int root = Root(n, 2);
    if (root == -1) return false;
    return IsPrime(root);
}

int64 DivisorNum(int64 n) {
    int64 answer = 0, i = 1;
    for (; i < n / i; ++i) {
        if (n % i == 0) answer += 2;
    }
    if (i * i == n) return answer + 1;
    return answer;
}

bool DivisorNumIs(int64 n, int64 k) {
    switch (k) {
        case 1: return n == 1;
        case 2: return IsPrime(n);
        case 3: return IsPrimeSquare(n);
        default: return DivisorNum(n) == k;
    }
}

struct Solution {
    int N;
    int64 X;

    int64 GetAnswer() {
        for (int numDivisor = 1; ; ++numDivisor) {
            if ((X >> (numDivisor / 2)) == 0) return -1;
            if (numDivisor % 2 == 0) {
                int64 root = Root(X, numDivisor / 2);
                if (root == -1) continue;
                if (DivisorNumIs(root, numDivisor)) return root;
            } else {
                int64 root = Root(X, numDivisor);
                if (root == -1) continue;
                int64 root2 = root * root;
                if (DivisorNumIs(root2, numDivisor)) return root2;
            }
        }
    }

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            scanf("%lld", &X);
            printf("%lld\n", GetAnswer());
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
