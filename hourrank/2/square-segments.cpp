#include <algorithm>
#include <cstdio>
#include <map>
#include <vector>
using namespace std;

typedef long long int64;
typedef unsigned long long uint64;
typedef map<uint64, int>::iterator Type1;

struct Factorization {
    vector<int> SmallestFactor;

    void Init(int n) {
        SmallestFactor.resize(n + 1);
        for (int i = 2; i <= n; ++i) {
            if (SmallestFactor[i] != 0) continue;
            SmallestFactor[i] = i;
            for (int64 j = int64(i) * i; j <= n; j += i) {
                if (SmallestFactor[j] == 0) SmallestFactor[j] = i;
            }
        }
    }

    vector<int> Factorize(int n) {
        vector<int> answer;
        while (n > 1) {
            answer.push_back(SmallestFactor[n]);
            n /= SmallestFactor[n];
        }
        return answer;
    }
};

uint64 Fingerprint(uint64 x) {
    const uint64 kMul = 0x9ddfea08eb382d69ULL;
    x *= kMul, x ^= x >> 47;
    x *= kMul, x ^= x >> 47;
    x *= kMul, x ^= x >> 47;
    return x * kMul;
}

uint64 XorFingerprint(const vector<int>& a) {
    uint64 answer = 0;
    for (int i = 0; i < a.size(); ++i) {
        answer ^= Fingerprint(a[i]);
    }
    return answer;
}

struct Solution {
    int N;
    vector<int> A;
    vector<uint64> B;
    map<uint64, int> C;
    Factorization factorization;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        factorization.Init(*max_element(A.begin(), A.end()));
        B.resize(N + 1);
        for (int i = 1; i <= N; ++i) {
            B[i] = XorFingerprint(factorization.Factorize(A[i - 1]));
            B[i] ^= B[i - 1];
        }
        for (int i = 0; i <= N; ++i) ++C[B[i]];
        int64 answer = 0;
        for (Type1 it = C.begin(); it != C.end(); ++it) {
            answer += int64(it->second - 1) * it->second / 2;
        }
        printf("%lld\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
