#include <cstdio>
using namespace std;

typedef long long int64;

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        int64 M = 1, P = 1 << 2;
        for (int i = 4; i <= N; ++i) {
            M = (M + M + P - 1) % 1000000009;
            P = (P + P) % 1000000009;
        }
        printf("%lld\n", M);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
