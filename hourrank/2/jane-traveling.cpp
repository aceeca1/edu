#include <climits>
#include <cstdio>
#include <vector>
using namespace std;

bool AssignMin(int* p, int v) {
    if (v < *p) return *p = v, true;
    return false;
}

struct Solution {
    int N;
    vector<vector<int> > D, MinRoute;

    void Solve() {
        scanf("%d", &N);
        D.resize(5);
        for (int i = 0; i < 5; ++i) {
            D[i].resize(2);
            scanf("%d%d", &D[i][0], &D[i][1]);
        }
        MinRoute.resize(N + 1);
        for (int i = 0; i <= N; ++i) {
            MinRoute[i].resize(5);
            if (i == 0) continue;
            for (int j = 0; j < 5; ++j) {
                MinRoute[i][j] = INT_MAX >> 1;
                int j0 = (j + 1) % 5, j1 = (j + 2) % 5;
                if (D[j][0] <= i) {
                    AssignMin(&MinRoute[i][j], MinRoute[i - D[j][0]][j0] + 1);
                }
                if (D[j][1] <= i) {
                    AssignMin(&MinRoute[i][j], MinRoute[i - D[j][1]][j1] + 1);
                }
            }
        }
        if (MinRoute[N][0] == INT_MAX >> 1) {
            printf("-1\n");
        } else {
            int n = N, arg = 0;
            bool head = true;
            while (true) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", arg + 1);
                if (n == 0) break;
                int arg0 = (arg + 1) % 5, arg1 = (arg + 2) % 5;
                int n0 = n - D[arg][0], n1 = n - D[arg][1];
                if (arg1 < arg0) {
                    swap(arg0, arg1);
                    swap(n0, n1);
                }
                if (0 <= n0 && MinRoute[n0][arg0] + 1 == MinRoute[n][arg]) {
                    n = n0;
                    arg = arg0;
                } else {
                    n = n1;
                    arg = arg1;
                }
            }
            printf("\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
