#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, Answer;
    vector<int> A;

    bool AllValid(int k) {
        Answer = 0;
        for (int i = 0; i < N; ++i) {
            int b = A[i] / (k + 1) + 1;
            if (A[i] / b != k) return false;
            Answer += b;
        }
        return true;
    }

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        int k = *min_element(A.begin(), A.end());
        while (!AllValid(k)) --k;
        printf("%d\n", Answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
