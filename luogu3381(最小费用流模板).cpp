#include <cstdio>
#include <climits>
#include <map>
#include <vector>
using namespace std;

bool AssignMin(long long* p, long long v) {
    if (v < *p) return *p = v, true;
    return false;
}

struct Edge {
    int Target;
    long long U, C;
};

struct Graph {
    long long Cost;
    vector<Edge> Edges;
    vector<vector<int> > Out;
    int Source, Target;
    vector<long long> Excess, P, Distance;
    multimap<int, int> Q;
    vector<bool> Permanent, Visited;

    Graph() { Cost = 0; }

    // u1: Lower Bound  u2: Upper Bound  c: Cost
    void AddEdge(int s, int t, long long u1, long long u2, long long c) {
        Edge e1 = {t, 0, c}, e2 = {s, 0, -c};
        if (c < 0) {
            Excess[t] += u2;
            Excess[s] -= u2;
            e2.U = u2 - u1;
            Cost += u2 * c;
        } else {
            Excess[t] += u1;
            Excess[s] -= u1;
            e1.U = u2 - u1;
            Cost += u1 * c;
        }
        Edges.push_back(e1);
        Edges.push_back(e2);
        Out[s].push_back(Edges.size() - 2);
        Out[t].push_back(Edges.size() - 1);
    }

    void MinCostFlow(long long* flow, long long* cost) {
        *cost = Cost;
        Out.resize(Out.size() + 2);
        int oldSource = Source, oldTarget = Target;
        Source = Out.size() - 2;
        Target = Out.size() - 1;
        for (int i = 0; i < Source; ++i) {
            if (0 < Excess[i]) AddEdge(Source, i, 0, Excess[i], 0);
            if (Excess[i] < 0) AddEdge(i, Target, 0, -Excess[i], 0);
        }
        CostFlow(flow, cost);
        for (int i = 0; i < Out[Source].size(); ++i) {
            Edges[Out[Source][i] ^ 1].U = 0;
        }
        for (int i = 0; i < Out[Target].size(); ++i) {
            Edges[Out[Target][i] ^ 1].U = 0;
        }
        Out.resize(Out.size() - 2);
        Source = oldSource;
        Target = oldTarget;
        *flow = 0;
        CostFlow(flow, cost);
    }

    void CostFlow(long long* flow, long long* cost) {
        P.resize(Out.size());
        while (Dijkstra()) {
            int delta = MaxFlow();
            *flow += delta;
            *cost += delta * (P[Source] - P[Target]);
        }
    }

    bool Dijkstra() {
        Distance.resize(Out.size());
        for (int i = 0; i < Out.size(); ++i) Distance[i] = INT_MAX;
        Distance[Target] = 0;
        Permanent.clear();
        Permanent.resize(Out.size());
        Q.clear();
        Q.insert(make_pair(0, Target));
        while (!Q.empty()) {
            int no = Q.begin()->second;
            Q.erase(Q.begin());
            if (Permanent[no]) continue;
            Permanent[no] = true;
            for (int i = 0; i < Out[no].size(); ++i) {
                int ei = Out[no][i], target = Edges[ei].Target;
                if (Edges[ei ^ 1].U == 0 || Permanent[target]) continue;
                long long newDistance =
                    Distance[no] + (Edges[ei ^ 1].C - P[target] + P[no]);
                if (AssignMin(&Distance[target], newDistance)) {
                    Q.insert(make_pair(newDistance, target));
                }
            }
        }
        for (int i = 0; i < Out.size(); ++i) {
            if (Distance[i] < INT_MAX) P[i] += Distance[i];
        }
        return Distance[Source] < INT_MAX;
    }

    long long MaxFlow() {
        long long answer = 0;
        while (true) {
            Visited.clear();
            Visited.resize(Out.size());
            long long delta = Augment(Source, INT_MAX);
            if (delta == 0) break;
            answer += delta;
        }
        return answer;
    }

    long long Augment(int no, long long flow) {
        if (no == Target) return flow;
        Visited[no] = true;
        long long left = flow;
        for (int i = 0; i < Out[no].size(); ++i) {
            int ei = Out[no][i], target = Edges[ei].Target;
            if (Visited[target] || Edges[ei].U == 0) continue;
            if (Edges[ei].C - P[no] + P[target] != 0) continue;
            long long delta = Augment(target, min(left, Edges[ei].U));
            Edges[ei].U -= delta;
            Edges[ei ^ 1].U += delta;
            left -= delta;
            if (left == 0) return flow;
        }
        return flow - left;
    }
};

struct Solution {
    int N, M;
    Graph G;

    void Solve() {
        scanf("%d%d%d%d", &N, &M, &G.Source, &G.Target);
        G.Out.resize(N + 1);
        G.Excess.resize(N + 1);
        for (int i = 0; i < M; ++i) {
            int u, v, w, c;
            scanf("%d%d%d%d", &u, &v, &w, &c);
            G.AddEdge(u, v, 0, w, c);
        }
        long long flow, cost;
        G.MinCostFlow(&flow, &cost);
        printf("%lld %lld\n", flow, cost);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
