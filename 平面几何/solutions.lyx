#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\usepackage{yhmath}
\usepackage[UTF8]{ctex}
\usepackage[pdfusetitle]{hyperref}
\usepackage{tikz}
\usetikzlibrary{calc}
\end_preamble
\use_default_options true
\begin_modules
theorems-ams
\end_modules
\maintain_unincluded_children false
\language chinese-simplified
\language_package default
\inputencoding utf8-plain
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 12
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
平面几何题解
\end_layout

\begin_layout Author
张昆玮
\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\widearc}[1]{\wideparen{#1}}
{\widehat{#1}}
\end_inset


\end_layout

\begin_layout Section
相似三角形 SIMILAR TRIANGLES
\end_layout

\begin_layout Subsection*
导引问题 Introductory problems 1
\end_layout

\begin_layout Standard
锐角三角形 
\begin_inset Formula $ABC$
\end_inset

 中的高为 
\begin_inset Formula $AA_{1}$
\end_inset

 和 
\begin_inset Formula $BB_{1}$
\end_inset

, 求证 
\begin_inset Formula $A_{1}C\cdot BC=B_{1}C\cdot AC$
\end_inset

 .
\end_layout

\begin_layout Description
解.
 
\begin_inset Formula $A_{1}C\cdot BC=AC\cdot BC\cdot\sin C=B_{1}C\cdot AC$
\end_inset

 .
\end_layout

\begin_layout Subsection*
导引问题 Introductory problems 2
\end_layout

\begin_layout Standard
\begin_inset Formula $\triangle ABC$
\end_inset

 中 
\begin_inset Formula $\angle C$
\end_inset

 为直角, 高为 
\begin_inset Formula $CH$
\end_inset

.
 求证 
\begin_inset Formula $AC^{2}=AB\cdot AH$
\end_inset

, 
\begin_inset Formula $CH^{2}=AH\cdot BH$
\end_inset

 .
\end_layout

\begin_layout Description
解.
 
\begin_inset Formula $AB\cdot AH=AB\cdot AC\sin B=AC^{2}$
\end_inset

 .
\end_layout

\begin_layout Description

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\begin_inset Formula $AH\cdot BH=AC\sin B\cdot BC\sin A=AC\sin A\cdot BC\sin B=CH^{2}$
\end_inset

 .
\end_layout

\begin_layout Subsection*
导引问题 Introductory problems 3
\end_layout

\begin_layout Standard
试证明三角形三条中线交于一点, 这一点从顶点开始分各中线为 
\begin_inset Formula $2:1$
\end_inset

.
\end_layout

\begin_layout Lemma
(Ceva 定理) 在 
\begin_inset Formula $\triangle ABC$
\end_inset

 中, 
\begin_inset Formula $A',B',C'$
\end_inset

 分别在 
\begin_inset Formula $BC,CA,AB$
\end_inset

 上.
 则 
\begin_inset Formula $AA',BB',CC'$
\end_inset

 共点 
\begin_inset Formula $\Longleftrightarrow$
\end_inset

 
\begin_inset Formula $\frac{BA'}{A'C}\cdot\frac{CB'}{B'A}\cdot\frac{AC'}{C'B}=1$
\end_inset

 .
\end_layout

\begin_layout Proof
先证明充分性.
 
\begin_inset Formula $\frac{BA'}{A'C}\cdot\frac{CB'}{B'A}\cdot\frac{AC'}{C'B}=\frac{S_{\triangle ABO}}{S_{\triangle CAO}}\cdot\frac{S_{\triangle BCO}}{S_{\triangle ABO}}\cdot\frac{S_{\triangle CAO}}{S_{\triangle BCO}}=1$
\end_inset

 .
 再证明必要性, 设 
\begin_inset Formula $O$
\end_inset

 是 
\begin_inset Formula $BB',CC'$
\end_inset

 交点, 
\begin_inset Formula $A''$
\end_inset

 是 
\begin_inset Formula $AO$
\end_inset

 延长线与 
\begin_inset Formula $BC$
\end_inset

 交点.
 则由充分性, 
\begin_inset Formula $\frac{BA''}{A''C}=\left(\frac{CB'}{B'A}\cdot\frac{AC'}{C'B}\right)^{-1}=\frac{BA'}{A'C}$
\end_inset

.
 故 
\begin_inset Formula $A'=A''$
\end_inset

.
\end_layout

\begin_layout Description
解.
 应用 Ceva 定理即可.
 为了证明 
\begin_inset Formula $BO=2B'O$
\end_inset

, 我们有 
\begin_inset Formula 
\[
\frac{BO}{B'O}=\frac{S_{\triangle BCC'}}{S_{\triangle B'CC'}}=\frac{S_{\triangle ABC}/2}{S_{\triangle BB'C}/2}=2\;.
\]

\end_inset


\end_layout

\begin_layout Subsection*
导引问题 Introductory problems 4
\end_layout

\begin_layout Standard
在 
\begin_inset Formula $\triangle ABC$
\end_inset

 中, 
\begin_inset Formula $BC$
\end_inset

 边上取 
\begin_inset Formula $A_{1}$
\end_inset

 使得 
\begin_inset Formula $BA_{1}:A_{1}C=2:1$
\end_inset

.
 求中线 
\begin_inset Formula $CC_{1}$
\end_inset

 分线段 
\begin_inset Formula $AA_{1}$
\end_inset

 的比.
\end_layout

\begin_layout Description
解.
 令 
\begin_inset Formula $O$
\end_inset

 为交点, 有 
\begin_inset Formula $\frac{AO}{A_{1}O}=\frac{S_{\triangle ACC_{1}}}{S_{\triangle A_{1}CC_{1}}}=\frac{\frac{1}{2}S_{\triangle ABC}}{\frac{1}{2}S_{\triangle AA_{1}C}}=3$
\end_inset

 .
\end_layout

\begin_layout Subsection*
导引问题 Introductory problems 5
\end_layout

\begin_layout Standard
正方形 
\begin_inset Formula $PQRS$
\end_inset

 嵌入 
\begin_inset Formula $\triangle ABC$
\end_inset

 中, 使得 
\begin_inset Formula $P,Q$
\end_inset

 分别在 
\begin_inset Formula $AB,AC$
\end_inset

 上, 
\begin_inset Formula $R,S$
\end_inset

 在 
\begin_inset Formula $BC$
\end_inset

 上.
 用 
\begin_inset Formula $a=BC$
\end_inset

 和 BC 边上的高 
\begin_inset Formula $h_{a}$
\end_inset

 表达正方形的边长.
\end_layout

\begin_layout Description
解.
 令 
\begin_inset Formula $PQ/a=x$
\end_inset

.
 则 
\begin_inset Formula $PS/h_{a}=1-x$
\end_inset

, 有
\begin_inset Formula 
\[
1-\frac{PQ}{a}=\frac{PQ}{h_{a}}\implies PQ=1\Big/\left(\frac{1}{h_{a}}+\frac{1}{a}\right)\ .
\]

\end_inset


\end_layout

\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Standard
梯形的两边 
\begin_inset Formula $AD\parallel BC$
\end_inset

, 
\begin_inset Formula $a=AD$
\end_inset

, 
\begin_inset Formula $b=BC$
\end_inset

.
\end_layout

\begin_layout Enumerate
求两对角线在中位线上的交点距离?
\end_layout

\begin_layout Enumerate
若 
\begin_inset Formula $AM:MB=DN:NC=p:q$
\end_inset

, 求 
\begin_inset Formula $MN$
\end_inset

 的长度.
\end_layout

\begin_layout Description
解.
 平行线分线段成比例定理.
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\frac{b}{2}+\frac{b}{2}-\frac{a+b}{2}=\frac{b-a}{2}$
\end_inset

.
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\frac{pb+qa}{p+q}$
\end_inset

 .
\end_layout

\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Standard
证明任意四边形中点的连线是平行四边形.
 何时为矩形, 菱形, 正方形?
\end_layout

\begin_layout Description
解.
 设四边形为 
\begin_inset Formula $ABCD$
\end_inset

, 
\begin_inset Formula $A_{1},B_{1},C_{1},D_{1}$
\end_inset

 为 
\begin_inset Formula $AB,BC,CD,DA$
\end_inset

 中点.
 则 
\begin_inset Formula $A_{1}B_{1}$
\end_inset

 平行且等于 
\begin_inset Formula $\frac{1}{2}AC$
\end_inset

 平行且等于 
\begin_inset Formula $C_{1}D_{1}$
\end_inset

, 故是平行四边形.
 原四边形对角线垂直时是矩形, 原四边形对角线相等时是菱形, 同时满足时是正方形.
\end_layout

\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Standard
在 
\begin_inset Formula $\triangle ABC$
\end_inset

 中, 
\begin_inset Formula $A_{1}$
\end_inset

 分 
\begin_inset Formula $BC$
\end_inset

 的比是 
\begin_inset Formula $BA_{1}:A_{1}C=1:p$
\end_inset

; 
\begin_inset Formula $B_{1}$
\end_inset

 分 
\begin_inset Formula $AC$
\end_inset

 的比是 
\begin_inset Formula $AB_{1}:B_{1}C=1:q$
\end_inset

; 则 
\begin_inset Formula $AA_{1}$
\end_inset

 被 
\begin_inset Formula $BB_{1}$
\end_inset

 分的比是多少?
\end_layout

\begin_layout Description
解.
 设交点为 
\begin_inset Formula $H$
\end_inset

, 则 
\begin_inset Formula 
\[
\frac{AH}{HA_{1}}=\frac{S_{\triangle ABB_{1}}}{S_{\triangle A_{1}BB_{1}}}=\frac{S_{\triangle ABC}/(1+q)}{S_{\triangle BB_{1}C}/(1+p)}=\frac{1+p}{q}\;.
\]

\end_inset


\end_layout

\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Standard
在 
\begin_inset Formula $\triangle ABC$
\end_inset

 中, 
\begin_inset Formula $CC_{1}$
\end_inset

 是中线, 在 
\begin_inset Formula $CC_{1}$
\end_inset

 上取 
\begin_inset Formula $P$
\end_inset

, 延长 
\begin_inset Formula $AP,BP$
\end_inset

 交对边于 
\begin_inset Formula $A_{1},B_{1}$
\end_inset

, 求证 
\begin_inset Formula $A_{1}B_{1}\parallel AB$
\end_inset

.
\end_layout

\begin_layout Description
解.
 只需证明 
\begin_inset Formula $CA_{1}:CB_{1}=CB:CA$
\end_inset

, 由 Ceva 定理显然.
\end_layout

\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Standard
在四边形 
\begin_inset Formula $ABCD$
\end_inset

 中, 
\begin_inset Formula $P$
\end_inset

 是 
\begin_inset Formula $AC$
\end_inset

 和 
\begin_inset Formula $BD$
\end_inset

 交点, 
\begin_inset Formula $Q$
\end_inset

 是直线 
\begin_inset Formula $AB$
\end_inset

 和 
\begin_inset Formula $CD$
\end_inset

 交点.
 若 
\begin_inset Formula $PQ$
\end_inset

 平分 
\begin_inset Formula $AD$
\end_inset

, 求证 
\begin_inset Formula $PQ$
\end_inset

 平分 
\begin_inset Formula $BC$
\end_inset

.
\end_layout

\begin_layout Description
解.
 在 
\begin_inset Formula $\triangle ADQ$
\end_inset

 中, 
\begin_inset Formula $P$
\end_inset

 在中线延长线上, 故 
\begin_inset Formula $BC\parallel AD$
\end_inset

.
 结论易得.
\end_layout

\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Standard
在平行四边形 
\begin_inset Formula $ABCD$
\end_inset

 中, 
\begin_inset Formula $P$
\end_inset

 在 
\begin_inset Formula $AD$
\end_inset

 上满足 
\begin_inset Formula $AP:AD=1:n$
\end_inset

.
 令 
\begin_inset Formula $Q$
\end_inset

 为 
\begin_inset Formula $AC$
\end_inset

 和 
\begin_inset Formula $BP$
\end_inset

 交点, 证明 
\begin_inset Formula $AQ:AC=1:(n+1)$
\end_inset

.
\end_layout

\begin_layout Description
解.
 
\begin_inset Formula 
\[
\frac{AQ}{QC}=\frac{S_{\triangle ABP}}{S_{\triangle CBP}}=\frac{S_{\triangle ABD}}{S_{\triangle CBP}}\cdot\frac{1}{n}=\frac{1}{n}\;.
\]

\end_inset


\end_layout

\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Standard
平行四边形 
\begin_inset Formula $A_{1}B_{1}C_{1}D_{1}$
\end_inset

 的四个顶点分别落在平行四边形 
\begin_inset Formula $ABCD$
\end_inset

 的四条边上 (
\begin_inset Formula $A_{1}$
\end_inset

 在 
\begin_inset Formula $AB$
\end_inset

 上, 等等).
 证明两个平行四边形的中心重合.
\end_layout

\begin_layout Description
解.
 平行四边形 
\begin_inset Formula $A_{1}B_{1}C_{1}D_{1}$
\end_inset

 的中心是 
\begin_inset Formula $B_{1}D_{1}$
\end_inset

 中点, 因此在平行四边形 
\begin_inset Formula $ABCD$
\end_inset

 中线上.
 同理中心在另一条中线上, 因此是两条中线交点, 从而和平行四边形 
\begin_inset Formula $ABCD$
\end_inset

 中心重合.
\end_layout

\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Standard
在平行四边形 
\begin_inset Formula $ABCD$
\end_inset

 中, 
\begin_inset Formula $K$
\end_inset

 在 
\begin_inset Formula $BD$
\end_inset

 上, 
\begin_inset Formula $AK$
\end_inset

 交 
\begin_inset Formula $BC$
\end_inset

 于 
\begin_inset Formula $L$
\end_inset

, 交 
\begin_inset Formula $CD$
\end_inset

 于 
\begin_inset Formula $M$
\end_inset

.
 求证 
\begin_inset Formula $AK^{2}=LK\cdot KM$
\end_inset

.
\end_layout

\begin_layout Description
解.
 
\begin_inset Formula 
\[
\frac{AK^{2}}{LK\cdot KM}=\frac{S_{\triangle ABD}\cdot S_{\triangle ABD}}{S_{\triangle LBD}\cdot S_{\triangle MBD}}=\frac{BC}{BL}\cdot\frac{DC}{DM}=\frac{AM}{AL}\cdot\frac{AL}{AM}=1\;.
\]

\end_inset


\end_layout

\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Standard
四边形 
\begin_inset Formula $ABCD$
\end_inset

 内接于圆, 
\begin_inset Formula $AC$
\end_inset

 是直径.
 求证 
\begin_inset Formula $AB$
\end_inset

 和 
\begin_inset Formula $CD$
\end_inset

 在 
\begin_inset Formula $BD$
\end_inset

 上的投影相等.
\end_layout

\begin_layout Description
解.
 设 
\begin_inset Formula $A,C$
\end_inset

 在 BD 上投影为 
\begin_inset Formula $H_{1},H_{2}$
\end_inset

.
 
\begin_inset Formula 
\begin{align*}
BH_{1} & =AB\cos\angle ABD=AB\sin\angle DBC=\frac{AB\cdot CD}{2R}\;.
\end{align*}

\end_inset

同理 
\begin_inset Formula $BH_{2}=AB\cdot CD/2R$
\end_inset

.
 故相等.
\end_layout

\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Standard
梯形 
\begin_inset Formula $ABCD$
\end_inset

, 
\begin_inset Formula $E$
\end_inset

 在 
\begin_inset Formula $AD$
\end_inset

 上且 
\begin_inset Formula $AE=BC$
\end_inset

.
 
\begin_inset Formula $CA$
\end_inset

 和 
\begin_inset Formula $CE$
\end_inset

 交 
\begin_inset Formula $BD$
\end_inset

 于 
\begin_inset Formula $O,P$
\end_inset

.
 若 
\begin_inset Formula $BO=PD$
\end_inset

, 证明 
\begin_inset Formula 
\[
AD^{2}=BC^{2}+AD\cdot BC\;.
\]

\end_inset


\end_layout

\begin_layout Description
解.
 有 
\begin_inset Formula $BO:OD=DP:PB$
\end_inset

.
 
\begin_inset Formula 
\[
\frac{BO}{OD}=\frac{S_{\triangle ABC}}{S_{\triangle ADC}}=\frac{BC}{AD}\;.
\]

\end_inset


\begin_inset Formula 
\[
\frac{DP}{PB}=\frac{S_{\triangle CDE}}{S_{\triangle CBE}}=\frac{DE}{BC}=\frac{AD-BC}{BC}\;.
\]

\end_inset

故 
\begin_inset Formula $\frac{BC}{AD}=\frac{AD-BC}{BC}$
\end_inset

, 整理得结论.
\end_layout

\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Standard
在圆 
\begin_inset Formula $O$
\end_inset

 中, 
\begin_inset Formula $\widearc{AB}=60^{\circ}$
\end_inset

.
 
\begin_inset Formula $M$
\end_inset

 在 
\begin_inset Formula $\widearc{AB}$
\end_inset

 上.
 证明过 
\begin_inset Formula $MA$
\end_inset

 和 
\begin_inset Formula $OB$
\end_inset

 中点的直线与过 
\begin_inset Formula $MB$
\end_inset

 和 
\begin_inset Formula $OA$
\end_inset

 中点的直线垂直.
\end_layout

\begin_layout Description
解.
 设 
\begin_inset Formula $P,Q,R,S$
\end_inset

 为 
\begin_inset Formula $AM,MB,BO,OA$
\end_inset

 中点.
 则 
\begin_inset Formula $PQRS$
\end_inset

 是平行四边形.
 需证它是菱形, 事实上, 
\begin_inset Formula 
\[
PS=OM/2=AB/2=RS\;.
\]

\end_inset


\end_layout

\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Enumerate
\begin_inset Formula $A,B,C$
\end_inset

 在一直线上, 
\begin_inset Formula $A_{1},B_{1},C_{1}$
\end_inset

 在另一直线上, 如果 
\begin_inset Formula $AB_{1}\parallel BA_{1}$
\end_inset

, 
\begin_inset Formula $AC_{1}\parallel CA_{1}$
\end_inset

, 求证 
\begin_inset Formula $BC_{1}\parallel CB_{1}$
\end_inset

.
\end_layout

\begin_layout Enumerate
\begin_inset Formula $A,B,C$
\end_inset

 在一直线上, 如果 
\begin_inset Formula $AB_{1}\parallel BA_{1}$
\end_inset

, 
\begin_inset Formula $AC_{1}\parallel CA_{1}$
\end_inset

, 
\begin_inset Formula $BC_{1}\parallel CB_{1}$
\end_inset

, 求证 
\begin_inset Formula $A_{1},B_{1},C_{1}$
\end_inset

 共线.
\end_layout

\begin_layout Description
解.
 
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Enumerate
设两直线相交于 
\begin_inset Formula $P$
\end_inset

, 
\begin_inset Formula 
\[
\frac{PB}{PC}=\frac{PB}{PA}\cdot\frac{PA}{PC}=\frac{PA_{1}}{PB_{1}}\cdot\frac{PC_{1}}{PA_{1}}=\frac{PC_{1}}{PB_{1}}\;,
\]

\end_inset

故平行.
\end_layout

\begin_layout Enumerate
令 
\begin_inset Formula $AB$
\end_inset

 与 
\begin_inset Formula $A_{1}B_{1}$
\end_inset

 交于 
\begin_inset Formula $P$
\end_inset

, 只需证 
\begin_inset Formula $C_{1}$
\end_inset

 在 
\begin_inset Formula $A_{1}B_{1}$
\end_inset

 上, 或
\end_layout

\begin_deeper
\begin_layout Enumerate
过 
\begin_inset Formula $A$
\end_inset

 的 
\begin_inset Formula $A_{1}C$
\end_inset

 平行线;
\end_layout

\begin_layout Enumerate
过 
\begin_inset Formula $B$
\end_inset

 的 
\begin_inset Formula $B_{1}C$
\end_inset

 平行线;
\end_layout

\begin_layout Enumerate
\begin_inset Formula $A_{1}B_{1}$
\end_inset

;
\end_layout

\begin_layout Standard
三线共点.
 令
\end_layout

\begin_layout Enumerate
过 
\begin_inset Formula $A$
\end_inset

 的 
\begin_inset Formula $A_{1}C$
\end_inset

 平行线交 
\begin_inset Formula $A_{1}B_{1}$
\end_inset

 于 
\begin_inset Formula $C_{2}$
\end_inset

;
\end_layout

\begin_layout Enumerate
过 
\begin_inset Formula $B$
\end_inset

 的 
\begin_inset Formula $B_{1}C$
\end_inset

 平行线交 
\begin_inset Formula $A_{1}B_{1}$
\end_inset

 于 
\begin_inset Formula $C_{3}$
\end_inset

;
\end_layout

\begin_layout Standard
只需证 
\begin_inset Formula $C_{2}=C_{3}$
\end_inset

.
 而 
\begin_inset Formula 
\[
PC_{2}=PA_{1}\cdot PA/PC=PB_{1}\cdot PB/PC=PC_{3}\;.
\]

\end_inset


\end_layout

\end_deeper
\begin_layout Subsection
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_layout Standard
在 
\begin_inset Formula $\triangle ABC$
\end_inset

 中, 画出角平分线 
\begin_inset Formula $AA_{1}$
\end_inset

, 
\begin_inset Formula $BB_{1}$
\end_inset

, 证明 
\begin_inset Formula $A_{1}B_{1}$
\end_inset

 上任意一点 
\begin_inset Formula $M$
\end_inset

 到 
\begin_inset Formula $AB$
\end_inset

 的距离, 等于 
\begin_inset Formula $M$
\end_inset

 到 
\begin_inset Formula $AC$
\end_inset

, 
\begin_inset Formula $BC$
\end_inset

 的距离之和.
\end_layout

\begin_layout Description
解.
 设 
\begin_inset Formula $M$
\end_inset

 在 
\begin_inset Formula $AB,AC,BC$
\end_inset

 投影为 
\begin_inset Formula $M_{1},M_{2},M_{3}$
\end_inset

.
 我们有 
\begin_inset Formula 
\begin{align*}
MM_{1} & =\frac{S_{\triangle ABM}}{AB}=\frac{B_{1}M\cdot S_{\triangle ABA_{1}}+A_{1}M\cdot S_{\triangle ABB_{1}}}{AB\cdot A_{1}B_{1}}\\
 & =\frac{B_{1}M\cdot S_{\triangle ABC}\cdot\frac{AB}{AB+AC}+A_{1}M\cdot S_{\triangle ABC}\cdot\frac{AB}{AB+BC}}{AB\cdot A_{1}B_{1}}\\
 & =\frac{1}{A_{1}B_{1}}\left(\frac{B_{1}M\cdot S_{\triangle ABC}}{AB+AC}+\frac{A_{1}M\cdot S_{\triangle ABC}}{AB+BC}\right)\;.
\end{align*}

\end_inset

而
\begin_inset Formula 
\begin{align*}
MM_{2}+MM_{3} & =\frac{S_{\triangle ACM}}{AC}+\frac{S_{\triangle BCM}}{BC}\\
 & =\frac{1}{A_{1}B_{1}}\left(\frac{S_{\triangle AA_{1}C}\cdot B_{1}M}{AC}+\frac{S_{\triangle BB_{1}C}\cdot A_{1}M}{BC}\right)\\
 & =\frac{1}{A_{1}B_{1}}\left(\frac{S_{\triangle ABC}\cdot\frac{AC}{AB+AC}\cdot B_{1}M}{AC}+\frac{S_{\triangle ABC}\cdot\frac{BC}{AB+BC}\cdot A_{1}M}{BC}\right)\\
 & =\frac{1}{A_{1}B_{1}}\left(\frac{B_{1}M\cdot S_{\triangle ABC}}{AB+AC}+\frac{A_{1}M\cdot S_{\triangle ABC}}{AB+BC}\right)\;.
\end{align*}

\end_inset

故相等.
\end_layout

\end_body
\end_document
