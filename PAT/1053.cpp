#include <cstdio>
using namespace std;

struct Solution {
    int N, D;
    double E;

    void Solve() {
        scanf("%d%lf%d", &N, &E, &D);
        int empty = 0, possibleEmpty = 0;
        for (int i = 0; i < N; ++i) {
            int k, lowPower = 0;
            scanf("%d", &k);
            for (int j = 0; j < k; ++j) {
                double power;
                scanf("%lf", &power);
                lowPower += power < E;
            }
            if (lowPower * 2 > k) {
                if (k > D) {
                    ++empty;
                } else {
                    ++possibleEmpty;
                }
            }
        }
        printf("%.1lf%% %.1lf%%\n", possibleEmpty * 100.0 / N,
               empty * 100.0 / N);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
