#include <cstdio>
#include <string>
#include <vector>
using namespace std;

struct Solution {
    vector<int> Count;

    void Solve() {
        Count.resize(10);
        for (int i = 0; i < 10; ++i) scanf("%d", &Count[i]);
        int first = 1;
        while (Count[first] == 0) ++first;
        --Count[first];
        string answer(1, '0' + first);
        for (int i = 0; i < 10; ++i) {
            answer += string(Count[i], '0' + i);
        }
        printf("%s\n", answer.c_str());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
