#include <cstdio>
using namespace std;

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        double answer = 0.0;
        for (int i = 0; i < N; ++i) {
            double x;
            scanf("%lf", &x);
            answer += x * (i + 1) * (N - i);
        }
        printf("%.2lf\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
