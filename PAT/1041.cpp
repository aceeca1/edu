#include <cstdio>
#include <map>
#include <string>
using namespace std;

struct Student {
    string Id;
    int Exam;
};

char Buffer[17];

struct Solution {
    int N;
    map<int, Student> Students;

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            int prepare;
            Student student;
            scanf("%s%d%d", Buffer, &prepare, &student.Exam);
            student.Id = Buffer;
            Students[prepare] = student;
        }
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            int prepare;
            scanf("%d", &prepare);
            const Student& student = Students[prepare];
            printf("%s %d\n", student.Id.c_str(), student.Exam);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
