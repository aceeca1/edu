#include <cstdio>
using namespace std;

struct Solution {
    int Coefficient, Power;

    void Solve() {
        bool head = true;
        while (true) {
            if (scanf("%d%d", &Coefficient, &Power) < 2) break;
            if (Power == 0) continue;
            if (!head) putchar(' ');
            head = false;
            printf("%d %d", Coefficient * Power, Power - 1);
        }
        if (head) printf("0 0");
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
