#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[100002];

struct Solution {
    string Bad, S;

    void Solve() {
        fgets(Buffer, 100002, stdin);
        Bad = Buffer;
        Bad.pop_back();
        fgets(Buffer, 100002, stdin);
        S = Buffer;
        S.pop_back();
        vector<bool> badChars(256);
        for (int i = 0; i < Bad.size(); ++i) {
            if (Bad[i] == '+') {
                for (char i = 'A'; i <= 'Z'; ++i) badChars[i] = true;
            } else {
                badChars[toupper(Bad[i])] = true;
                badChars[tolower(Bad[i])] = true;
            }
        }
        for (int i = 0; i < S.size(); ++i) {
            if (!badChars[S[i]]) putchar(S[i]);
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
