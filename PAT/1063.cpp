#include <cmath>
#include <cstdio>
using namespace std;

bool AssignMax(double* p, double v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N;

    void Solve() {
        double answer = 0.0;
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            double x, y;
            scanf("%lf%lf", &x, &y);
            double z = hypot(x, y);
            AssignMax(&answer, z);
        }
        printf("%.2lf\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
