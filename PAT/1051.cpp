#include <complex>
#include <cstdio>
using namespace std;

typedef complex<double> Point;

char Buffer[80];

struct Solution {
    double R1, P1, R2, P2;

    void Solve() {
        scanf("%lf%lf%lf%lf", &R1, &P1, &R2, &P2);
        Point a = polar(R1, P1) * polar(R2, P2);
        double real = a.real();
        double imag = a.imag();
        sprintf(Buffer, "%.2lf", real);
        string realString = Buffer;
        if (realString == "-0.00") realString = "0.00";
        sprintf(Buffer, "%.2lf", imag);
        string imagString = Buffer;
        if (imagString == "-0.00") imagString = "0.00";
        if (imagString[0] != '-') {
            printf("%s+%si", realString.c_str(), imagString.c_str());
        } else {
            printf("%s%si", realString.c_str(), imagString.c_str());
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
