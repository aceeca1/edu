#include <cstdio>
using namespace std;

typedef long long int64;

struct Solution {
    int64 A, B, C;

    void Solve(int caseNo) {
        scanf("%lld%lld%lld", &A, &B, &C);
        bool answer = A + B > C;
        printf("Case #%d: %s\n", caseNo, answer ? "true" : "false");
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 1; i <= t; ++i) Solution().Solve(i);
    return 0;
}
