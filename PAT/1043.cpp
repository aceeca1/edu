#include <cstdio>
#include <string>
#include <vector>
using namespace std;

const char PATest[] = "PATest";
char Buffer[10001];

struct Solution {
    string S;

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        vector<int> count(256);
        for (int i = 0; i < S.size(); ++i) ++count[S[i]];
        while (true) {
            bool head = true;
            for (int i = 0; i < 6; ++i) {
                if (count[PATest[i]] != 0) {
                    head = false;
                    --count[PATest[i]];
                    putchar(PATest[i]);
                }
            }
            if (head) break;
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
