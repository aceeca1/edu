#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

int Dx[] = {0, 1, 0, -1};
int Dy[] = {1, 0, -1, 0};

struct Solution {
    int N, M;
    vector<int> A;
    vector<vector<int> > B;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        sort(A.begin(), A.end());
        for (int i = 1; i <= N / i; ++i) {
            if (N % i != 0) continue;
            M = N / i;
        }
        N /= M;
        B.resize(M);
        for (int i = 0; i < M; ++i) B[i].resize(N);
        int d = 0, x = 0, y = 0;
        for (int i = A.size() - 1; i >= 0; --i) {
            B[x][y] = A[i];
            int newX = x + Dx[d];
            int newY = y + Dy[d];
            if (newX < 0 || M <= newX || newY < 0 || N <= newY ||
                B[newX][newY]) {
                if (++d == 4) d = 0;
            }
            x += Dx[d];
            y += Dy[d];
        }
        for (int i = 0; i < M; ++i) {
            bool head = true;
            for (int j = 0; j < N; ++j) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", B[i][j]);
            }
            printf("\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
