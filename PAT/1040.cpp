#include <cstdio>
#include <string>
using namespace std;

const int M = 1000000007;
char Buffer[100001];

struct Solution {
    string S;
    int P, PA, PAT;

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        P = PA = PAT = 0;
        for (int i = 0; i < S.size(); ++i) {
            switch (S[i]) {
                case 'P':
                    P = (P + 1) % M;
                    break;
                case 'A':
                    PA = (PA + P) % M;
                    break;
                case 'T':
                    PAT = (PAT + PA) % M;
                    break;
            }
        }
        printf("%d\n", PAT);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
