#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[1001];

struct Solution {
    string S1, S2;

    vector<int> Statistics(const string& s) {
        vector<int> answer(256);
        for (int i = 0; i < s.size(); ++i) ++answer[s[i]];
        return answer;
    }

    int Uncovered(const vector<int>& s1, const vector<int>& s2) {
        int answer = 0;
        for (int i = 0; i < 256; ++i) {
            if (s1[i] < s2[i]) answer += s2[i] - s1[i];
        }
        return answer;
    }

    void Solve() {
        scanf("%s", Buffer);
        S1 = Buffer;
        scanf("%s", Buffer);
        S2 = Buffer;
        vector<int> s1 = Statistics(S1);
        vector<int> s2 = Statistics(S2);
        int uncovered = Uncovered(s1, s2);
        if (uncovered == 0) {
            printf("Yes %d\n", S1.size() - S2.size());
        } else {
            printf("No %d\n", uncovered);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
