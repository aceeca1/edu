#include <algorithm>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

struct Student {
    string Name, Id;
    int Score;

    bool operator<(const Student& that) { return Score < that.Score; }
};

char Buffer[11];

struct Solution {
    int N;
    vector<Student> students;

    void Solve() {
        scanf("%d", &N);
        students.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%s", Buffer);
            students[i].Name = Buffer;
            scanf("%s", Buffer);
            students[i].Id = Buffer;
            scanf("%d", &students[i].Score);
        }
        Student* studentMin = &*min_element(students.begin(), students.end());
        Student* studentMax = &*max_element(students.begin(), students.end());
        printf("%s %s\n", studentMax->Name.c_str(), studentMax->Id.c_str());
        printf("%s %s\n", studentMin->Name.c_str(), studentMin->Id.c_str());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
