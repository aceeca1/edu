#include <cstdio>
#include <string>
using namespace std;

char Buffer[80];

struct Solution {
    int N, Count;
    double Sum;

    void Fail() { printf("ERROR: %s is not a legal number\n", Buffer); }

    void AddNumber() {
        string s = Buffer;
        int point = 0;
        while (point < s.size() && s[point] != '.') ++point;
        if (point + 3 < s.size()) return Fail();
        double x;
        if (sscanf(Buffer, "%lf", &x) != 1) return Fail();
        if (x < -1000.0 || 1000.0 < x) return Fail();
        Sum += x;
        ++Count;
    }

    void Solve() {
        scanf("%d", &N);
        Count = 0;
        Sum = 0.0;
        for (int i = 0; i < N; ++i) {
            scanf("%s", Buffer);
            AddNumber();
        }
        if (Count == 0) {
            printf("The average of 0 numbers is Undefined");
        } else if (Count == 1) {
            printf("The average of 1 number is %.2lf", Sum);
        } else {
            printf("The average of %d numbers is %.2lf", Count, Sum / Count);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
