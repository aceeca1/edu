#include <cstdio>
using namespace std;

struct Solution {
    int C1, C2;

    void Solve() {
        scanf("%d%d", &C1, &C2);
        int second = (C2 - C1 + 50) / 100;
        int minute = second / 60;
        second %= 60;
        int hour = minute / 60;
        minute %= 60;
        printf("%02d:%02d:%02d\n", hour, minute, second);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
