#include <cstdio>
#include <string>
using namespace std;

char Buffer[101];

const char* Pinyin[] = {"ling", "yi",  "er", "san", "si",
                        "wu",   "liu", "qi", "ba",  "jiu"};

struct Solution {
    string S;

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        int sum = 0;
        for (int i = 0; i < S.size(); ++i) sum += S[i] - '0';
        sprintf(Buffer, "%d", sum);
        S = Buffer;
        bool head = true;
        for (int i = 0; i < S.size(); ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%s", Pinyin[S[i] - '0']);
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
