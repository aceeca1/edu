#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[101];

struct Solution {
    string S;
    vector<int> Count;

    bool Accept() {
        Count.resize(256);
        for (int i = 0; i < S.size(); ++i) ++Count[S[i]];
        if (Count['P'] + Count['A'] + Count['T'] != S.size()) return false;
        if (Count['P'] != 1 || Count['T'] != 1) return false;
        int indexP = 0;
        while (S[indexP] != 'P') ++indexP;
        int indexT = 0;
        while (S[indexT] != 'T') ++indexT;
        int a1 = indexP;
        int a2 = indexT - indexP - 1;
        int a3 = S.size() - indexT - 1;
        return a2 != 0 && a1 * a2 == a3;
    }

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        printf(Accept() ? "YES\n" : "NO\n");
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
