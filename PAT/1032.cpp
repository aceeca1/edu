#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> Score;

    void Solve() {
        scanf("%d", &N);
        Score.resize(N + 1);
        for (int i = 0; i < N; ++i) {
            int school, score;
            scanf("%d%d", &school, &score);
            Score[school] += score;
        }
        int argmax = max_element(Score.begin(), Score.end()) - Score.begin();
        printf("%d %d\n", argmax, Score[argmax]);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
