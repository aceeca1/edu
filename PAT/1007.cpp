#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int N;

    vector<int> AllPrimes() {
        vector<int> answer;
        vector<bool> composite(N + 1);
        for (int i = 2; i <= N; ++i) {
            if (composite[i]) continue;
            answer.push_back(i);
            for (int64 j = int64(i) * i; j <= N; j += i) {
                composite[j] = true;
            }
        }
        return answer;
    }

    void Solve() {
        scanf("%d", &N);
        vector<int> primes = AllPrimes();
        int answer = 0;
        for (int i = 0; i < primes.size() - 1; ++i) {
            if (primes[i + 1] - primes[i] == 2) ++answer;
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
