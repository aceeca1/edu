#include <cctype>
#include <cstdio>
#include <string>
using namespace std;

const int Weight[17] = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};

char Buffer[19];

struct Solution {
    string S;

    bool Valid() {
        int sum = 0;
        for (int i = 0; i < 17; ++i) {
            if (!isdigit(S[i])) return false;
            sum += (S[i] - '0') * Weight[i];
        }
        int check = (12 - sum % 11) % 11;
        char c = check == 10 ? 'X' : '0' + check;
        return c == S[17];
    }

    void Solve() {
        int t;
        scanf("%d", &t);
        bool head = true;
        for (int i = 0; i < t; ++i) {
            scanf("%s", Buffer);
            S = Buffer;
            if (!Valid()) {
                head = false;
                printf("%s\n", S.c_str());
            }
        }
        if (head) printf("All passed\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
