#include <cstdio>
#include <string>
using namespace std;

char Buffer[80];

bool AssignMin(string* p, const string& v) {
    if (v < *p) return *p = v, true;
    return false;
}

bool AssignMax(string* p, const string& v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N;
    string Max = "0000/00/00", Min = "9999/99/99", Argmin, Argmax;

    void Solve() {
        scanf("%d", &N);
        int valid = 0;
        for (int i = 0; i < N; ++i) {
            scanf("%s", Buffer);
            string name = Buffer;
            scanf("%s", Buffer);
            string date = Buffer;
            if (date < "1814/09/06" || "2014/09/06" < date) continue;
            ++valid;
            if (AssignMin(&Min, date)) Argmin = name;
            if (AssignMax(&Max, date)) Argmax = name;
        }
        if (valid == 0) {
            printf("0\n");
        } else {
            printf("%d %s %s\n", valid, Argmin.c_str(), Argmax.c_str());
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
