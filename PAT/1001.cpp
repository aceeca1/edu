#include <cstdio>
using namespace std;

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        int answer = 0;
        while (N != 1) {
            N = N % 2 == 0 ? N / 2 : (3 * N + 1) / 2;
            ++answer;
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
