#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

typedef long long int64;

struct Solution {
    int N, P;
    vector<int> A;

    void Solve() {
        scanf("%d%d", &N, &P);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        sort(A.begin(), A.end());
        int p = 0, q = 1, answer = 0;
        while (true) {
            while (q < N && A[q] <= int64(P) * A[p]) ++q;
            AssignMax(&answer, q - p);
            if (q == N) break;
            ++p;
        }
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
