#include <cstdio>
#include <string>
using namespace std;

char Buffer[10000];

struct Solution {
    string S;

    string Unscientific(const string& s) {
        string answer = s[0] == '-' ? "-" : "";
        int e = 1;
        while (s[e] != 'E') ++e;
        string coefficient = s.substr(1, e - 1);
        string exponent = s.substr(e + 1);
        int exponentNumber;
        sscanf(exponent.c_str(), "%d", &exponentNumber);
        if (exponentNumber < 0) {
            answer += "0.";
            answer += string(-1 - exponentNumber, '0');
            answer += coefficient[0];
            answer += coefficient.substr(2);
        } else if (exponentNumber < coefficient.size() - 2) {
            answer += coefficient[0];
            answer += coefficient.substr(2, exponentNumber);
            answer += '.';
            answer += coefficient.substr(2 + exponentNumber);
        } else {
            answer += coefficient[0];
            answer += coefficient.substr(2);
            answer += string(exponentNumber - (coefficient.size() - 2), '0');
        }
        return answer;
    }

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        printf("%s\n", Unscientific(S).c_str());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
