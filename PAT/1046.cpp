#include <cstdio>
using namespace std;

struct Solution {
    int N, A, B;

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            int a, aSum, b, bSum;
            scanf("%d%d%d%d", &a, &aSum, &b, &bSum);
            bool aCorrect = aSum == a + b;
            bool bCorrect = bSum == a + b;
            if (aCorrect == bCorrect) continue;
            if (aCorrect) ++B;
            if (bCorrect) ++A;
        }
        printf("%d %d\n", A, B);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
