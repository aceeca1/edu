#include <cstdio>
using namespace std;

struct Solution {
    int N;
    char A;

    void Solve() {
        scanf("%d %c", &N, &A);
        for (int i = 0; i < N; ++i) putchar(A);
        putchar('\n');
        for (int i = 0; i < (N + 1) / 2 - 2; ++i) {
            putchar(A);
            for (int j = 0; j < N - 2; ++j) putchar(' ');
            putchar(A);
            putchar('\n');
        }
        for (int i = 0; i < N; ++i) putchar(A);
        putchar('\n');
    }
};

int main() {
    Solution().Solve();
    return 0;
}
