#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int M, N;

    vector<int> AllPrimes(int n) {
        vector<int> answer;
        vector<bool> composite(n + 1);
        for (int i = 2; i <= n; ++i) {
            if (composite[i]) continue;
            answer.push_back(i);
            for (int64 j = int64(i) * i; j <= n; j += i) {
                composite[j] = true;
            }
        }
        return answer;
    }

    void Solve() {
        scanf("%d%d", &M, &N);
        vector<int> primes = AllPrimes(200000);
        int no = 0;
        for (int i = M - 1; i <= N - 1; ++i) {
            if (++no == 10 || i == N - 1) {
                no = 0;
                printf("%d\n", primes[i]);
            } else {
                printf("%d ", primes[i]);
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
