#include <cstdio>
#include <string>
using namespace std;

char Buffer[1001];

struct Solution {
    string A, Div;
    int B, Mod;

    void Solve() {
        scanf("%s%d", Buffer, &B);
        A = Buffer;
        Mod = 0;
        Div.resize(A.size());
        for (int i = 0; i < A.size(); ++i) {
            Mod = Mod * 10 + (A[i] - '0');
            Div[i] = '0' + Mod / B;
            Mod %= B;
        }
        if (2 <= Div.size() && Div[0] == '0') Div.erase(0, 1);
        printf("%s %d\n", Div.c_str(), Mod);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
