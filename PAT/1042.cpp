#include <cctype>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[1002];

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    string S;

    void Solve() {
        fgets(Buffer, 1002, stdin);
        S = Buffer;
        vector<int> count(256);
        for (int i = 0; i < S.size(); ++i) ++count[tolower(S[i])];
        int maxCount = 0;
        char argmax;
        for (int i = 'a'; i <= 'z'; ++i) {
            if (AssignMax(&maxCount, count[i])) argmax = i;
        }
        printf("%c %d\n", argmax, maxCount);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
