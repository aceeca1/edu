#include <cstdio>
#include <vector>
using namespace std;

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Problem {
    int Score, Wrong;
    vector<bool> Chosen;
};

struct Solution {
    int N, M;
    vector<Problem> Problems;

    void Solve() {
        scanf("%d%d", &N, &M);
        Problems.resize(M);
        for (int i = 0; i < M; ++i) {
            int numTotalChoice, numChoice;
            scanf("%d%d%d", &Problems[i].Score, &numTotalChoice, &numChoice);
            Problems[i].Chosen.resize(numTotalChoice);
            for (int j = 0; j < numChoice; ++j) {
                char c;
                scanf(" %c", &c);
                Problems[i].Chosen[c - 'a'] = true;
            }
            Problems[i].Wrong = 0;
        }
        for (int i = 0; i < N; ++i) {
            int total = 0;
            for (int j = 0; j < M; ++j) {
                int numChoice;
                scanf(" (%d", &numChoice);
                vector<bool> chosen;
                chosen.resize(Problems[j].Chosen.size());
                for (int k = 0; k < numChoice; ++k) {
                    char c;
                    scanf(" %c", &c);
                    chosen[c - 'a'] = true;
                }
                scanf(" )");
                if (chosen == Problems[j].Chosen) {
                    total += Problems[j].Score;
                } else {
                    ++Problems[j].Wrong;
                }
            }
            printf("%d\n", total);
        }
        int maxWrong = 0;
        for (int i = 0; i < M; ++i) {
            AssignMax(&maxWrong, Problems[i].Wrong);
        }
        if (maxWrong == 0) {
            printf("Too simple");
        } else {
            printf("%d", maxWrong);
            for (int i = 0; i < M; ++i) {
                if (Problems[i].Wrong == maxWrong) printf(" %d", i + 1);
            }
            printf("\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
