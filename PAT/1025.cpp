#include <algorithm>
#include <cstdio>
#include <map>
#include <string>
#include <vector>
using namespace std;

struct Node {
    string Address, Next;
    int Data;
};

char Buffer[6];

struct Solution {
    int N, K;
    string Head;
    map<string, Node> Nodes;

    void Solve() {
        scanf("%s%d%d", Buffer, &N, &K);
        Head = Buffer;
        for (int i = 0; i < N; ++i) {
            Node node;
            scanf("%s%d", Buffer, &node.Data);
            node.Address = Buffer;
            scanf("%s", Buffer);
            node.Next = Buffer;
            Nodes[node.Address] = node;
        }
        vector<Node> answer;
        while (Head != "-1") {
            answer.push_back(Nodes[Head]);
            Head = Nodes[Head].Next;
        }
        N = answer.size();
        for (int i = 0; i <= N - K; i += K) {
            reverse(answer.begin() + i, answer.begin() + i + K);
        }
        for (int i = 0; i < N - 1; ++i) {
            printf("%s %d %s\n", answer[i].Address.c_str(), answer[i].Data,
                   answer[i + 1].Address.c_str());
        }
        printf("%s %d -1\n", answer[N - 1].Address.c_str(), answer[N - 1].Data);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
