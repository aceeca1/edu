#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> A, PrefixMax, SuffixMin;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        PrefixMax.resize(N);
        PrefixMax[0] = A[0];
        for (int i = 1; i < N; ++i) {
            PrefixMax[i] = max(PrefixMax[i - 1], A[i]);
        }
        SuffixMin.resize(N);
        SuffixMin[N - 1] = A[N - 1];
        for (int i = N - 2; i >= 0; --i) {
            SuffixMin[i] = min(SuffixMin[i + 1], A[i]);
        }
        vector<int> pivots;
        for (int i = 0; i < N; ++i) {
            if (i != 0 && A[i] < PrefixMax[i - 1]) continue;
            if (i != N - 1 && SuffixMin[i + 1] < A[i]) continue;
            pivots.push_back(A[i]);
        }
        printf("%d\n", pivots.size());
        bool head = true;
        for (int i = 0; i < pivots.size(); ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", pivots[i]);
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
