#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[81];

struct Solution {
    string S1, S2;

    void Solve() {
        scanf("%s", Buffer);
        S1 = Buffer;
        scanf("%s", Buffer);
        S2 = Buffer;
        vector<int> bad(256);
        int p1 = 0, p2 = 0;
        while (p1 < S1.size()) {
            if (p2 < S2.size() && S1[p1] == S2[p2]) {
                ++p1;
                ++p2;
            } else {
                char upper = toupper(S1[p1]);
                if (!bad[upper]) {
                    bad[upper] = true;
                    putchar(upper);
                }
                ++p1;
            }
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
