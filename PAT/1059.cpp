#include <cstdio>
#include <vector>
using namespace std;

typedef long long int64;

struct Solution {
    int N, K;
    vector<int> Rank;
    vector<bool> IsComposite, Checked;

    void Sieve() {
        IsComposite.resize(N + 1);
        for (int i = 2; i <= N; ++i) {
            if (IsComposite[i]) continue;
            for (int64 j = int64(i) * i; j <= N; j += i) {
                IsComposite[j] = true;
            }
        }
    }

    void Solve() {
        scanf("%d", &N);
        Sieve();
        Rank.resize(10000);
        for (int i = 1; i <= N; ++i) {
            int X;
            scanf("%d", &X);
            Rank[X] = i;
        }
        Checked.resize(N + 1);
        scanf("%d", &K);
        for (int i = 0; i < K; ++i) {
            int X;
            scanf("%d", &X);
            if (Rank[X] == 0) {
                printf("%04d: Are you kidding?\n", X);
            } else if (Checked[Rank[X]]) {
                printf("%04d: Checked\n", X);
            } else if (Rank[X] == 1) {
                printf("%04d: Mystery Award\n", X);
                Checked[Rank[X]] = true;
            } else if (!IsComposite[Rank[X]]) {
                printf("%04d: Minion\n", X);
                Checked[Rank[X]] = true;
            } else {
                printf("%04d: Chocolate\n", X);
                Checked[Rank[X]] = true;
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
