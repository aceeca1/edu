#include <cstdio>
#include <string>
using namespace std;

char Buffer[100002];

struct Solution {
    string S;
    int Sum, X0, X1;

    void Solve() {
        fgets(Buffer, 100002, stdin);
        S = Buffer;
        Sum = 0;
        for (int i = 0; i < S.size(); ++i) {
            if (isalpha(S[i])) {
                Sum += tolower(S[i]) - 'a' + 1;
            }
        }
        X0 = X1 = 0;
        while (Sum != 0) {
            if (Sum & 1) {
                ++X1;
            } else {
                ++X0;
            }
            Sum >>= 1;
        }
        printf("%d %d\n", X0, X1);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
