#include <cstdio>
#include <string>
using namespace std;

struct Solution {
    int N;
    char C;

    void Solve() {
        scanf("%d %c", &N, &C);
        int numLines = 1;
        --N;
        while (true) {
            int need = (numLines + 2) * 2;
            if (N < need) break;
            N -= need;
            numLines += 2;
        }
        for (int i = 0; i < numLines; ++i) {
            int d = numLines / 2 - i;
            if (d < 0) d = -d;
            int numStars = d * 2 + 1;
            int numSpaces = (numLines - numStars) / 2;
            printf("%s%s\n", string(numSpaces, ' ').c_str(),
                   string(numStars, C).c_str());
        }
        printf("%d\n", N);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
