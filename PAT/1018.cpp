#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;

    int Compare(char a1, char a2) {
        if (a1 == a2) return 0;
        switch (a1) {
            case 'C':
                return a2 == 'J' ? 1 : -1;
            case 'J':
                return a2 == 'B' ? 1 : -1;
            case 'B':
                return a2 == 'C' ? 1 : -1;
        }
        return 0;
    }

    void Solve() {
        scanf("%d", &N);
        vector<int> times(3), winA(26), winB(26);
        for (int i = 0; i < N; ++i) {
            char c1, c2;
            scanf(" %c %c", &c1, &c2);
            int compare = Compare(c1, c2);
            ++times[compare + 1];
            switch (compare) {
                case 1:
                    ++winA[c1 - 'A'];
                    break;
                case -1:
                    ++winB[c2 - 'A'];
                    break;
            }
        }
        printf("%d %d %d\n", times[2], times[1], times[0]);
        printf("%d %d %d\n", times[0], times[1], times[2]);
        char bestA = max_element(winA.begin(), winA.end()) - winA.begin() + 'A';
        if (bestA == 'A') bestA = 'B';
        char bestB = max_element(winB.begin(), winB.end()) - winB.begin() + 'A';
        if (bestB == 'A') bestB = 'B';
        printf("%c %c\n", bestA, bestB);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
