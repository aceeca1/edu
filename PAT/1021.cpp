#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[1001];

struct Solution {
    string S;

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        vector<int> count(10);
        for (int i = 0; i < S.size(); ++i) ++count[S[i] - '0'];
        for (int i = 0; i < 10; ++i) {
            if (count[i] == 0) continue;
            printf("%d:%d\n", i, count[i]);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
