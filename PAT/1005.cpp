#include <algorithm>
#include <cstdio>
#include <set>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> A;
    set<int> B;

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        sort(A.begin(), A.end());
        for (int i = 0; i < N; ++i) {
            int num = A[i];
            while (num != 1) {
                num = num % 2 == 0 ? num / 2 : (3 * num + 1) / 2;
                if (!B.insert(num).second) break;
            }
        }
        bool head = true;
        for (int i = N - 1; i >= 0; --i) {
            if (B.count(A[i])) continue;
            if (!head) putchar(' ');
            head = false;
            printf("%d", A[i]);
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
