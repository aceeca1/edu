#include <cstdio>
using namespace std;

typedef long long int64;

struct Money {
    int64 Galleon, Sickle, Knut;

    int64 ToKnut() const { return (Galleon * 17 + Sickle) * 29 + Knut; }

    void FromKnut(int64 knut) {
        Knut = knut % 29;
        Sickle = knut / 29;
        Galleon = Sickle / 17;
        Sickle %= 17;
    }
};

struct Solution {
    Money P, A;

    void Solve() {
        scanf("%d.%d.%d", &P.Galleon, &P.Sickle, &P.Knut);
        scanf("%d.%d.%d", &A.Galleon, &A.Sickle, &A.Knut);
        int64 r = A.ToKnut() - P.ToKnut();
        if (r < 0) {
            r = -r;
            putchar('-');
        }
        A.FromKnut(r);
        printf("%d.%d.%d\n", A.Galleon, A.Sickle, A.Knut);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
