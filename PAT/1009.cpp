#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[81];

struct Solution {
    vector<string> Words;

    void Solve() {
        while (true) {
            if (scanf("%s", Buffer) < 1) break;
            Words.push_back(Buffer);
        }
        bool head = true;
        for (int i = Words.size() - 1; i >= 0; --i) {
            if (!head) putchar(' ');
            head = false;
            printf("%s", Words[i].c_str());
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
