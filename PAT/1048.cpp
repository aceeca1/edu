#include <algorithm>
#include <cstdio>
#include <string>
using namespace std;

char Buffer[101];

struct Solution {
    string A, B;

    void Solve() {
        scanf("%s", Buffer);
        A = Buffer;
        scanf("%s", Buffer);
        B = Buffer;
        reverse(A.begin(), A.end());
        reverse(B.begin(), B.end());
        while (A.size() < B.size()) A += '0';
        while (B.size() < A.size()) B += '0';
        bool odd = true;
        for (int i = 0; i < A.size(); ++i) {
            if (odd) {
                int v = ((A[i] - '0') + (B[i] - '0')) % 13;
                switch (v) {
                    case 10:
                        A[i] = 'J';
                        break;
                    case 11:
                        A[i] = 'Q';
                        break;
                    case 12:
                        A[i] = 'K';
                        break;
                    default:
                        A[i] = '0' + v;
                        break;
                }
            } else {
                A[i] = '0' + (B[i] - A[i] + 10) % 10;
            }
            odd = !odd;
        }
        reverse(A.begin(), A.end());
        printf("%s\n", A.c_str());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
