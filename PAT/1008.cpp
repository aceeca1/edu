#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, K;
    vector<int> A;
    bool Head;

    void Print(int start, int stop) {
        for (int i = start; i < stop; ++i) {
            if (!Head) putchar(' ');
            Head = false;
            printf("%d", A[i]);
        }
    }

    void Solve() {
        scanf("%d%d", &N, &K);
        K %= N;
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        Head = true;
        Print(N - K, N);
        Print(0, N - K);
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
