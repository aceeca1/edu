#include <algorithm>
#include <cstdio>
#include <deque>
#include <string>
#include <vector>
using namespace std;

char Buffer[9];

struct Person {
    string Name;
    int Height;

    bool operator<(const Person& that) const {
        return Height < that.Height ||
               Height == that.Height && Name > that.Name;
    }
};

struct Solution {
    int N, K;
    vector<Person> Persons;

    void Solve() {
        scanf("%d%d", &N, &K);
        Persons.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%s%d", Buffer, &Persons[i].Height);
            Persons[i].Name = Buffer;
        }
        sort(Persons.begin(), Persons.end());
        for (int i = K - 1; i >= 0; --i) {
            int begin = i * (N / K);
            int end = (i + 1) * (N / K);
            if (i == K - 1) end = N;
            deque<string> row;
            bool back = true;
            for (int i = end - 1; i >= begin; --i) {
                if (back) {
                    row.push_back(Persons[i].Name);
                } else {
                    row.push_front(Persons[i].Name);
                }
                back = !back;
            }
            bool head = true;
            for (int i = 0; i < row.size(); ++i) {
                if (!head) putchar(' ');
                head = false;
                printf("%s", row[i].c_str());
            }
            printf("\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
