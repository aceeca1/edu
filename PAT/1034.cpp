#include <cstdio>
#include <string>
using namespace std;

typedef long long int64;

int64 Gcd(int64 a, int64 b) {
    if (b == 0) return a;
    return Gcd(b, a % b);
}

char Buffer[80];

struct Rational {
    int64 A, B;

    Rational& Simplify() {
        int64 gcd = Gcd(A, B);
        A /= gcd;
        B /= gcd;
        if (B < 0) {
            A = -A;
            B = -B;
        }
        return *this;
    }

    Rational& Assign(const string& s) {
        sscanf(s.c_str(), "%lld/%lld", &A, &B);
        return *this;
    }

    string ToString() const {
        if (B == 0) return "Inf";
        if (A < 0) {
            Rational n = {-A, B};
            return "(-" + n.ToString() + ")";
        }
        string answer;
        int64 k = A / B;
        int64 p = A % B;
        if (k != 0) {
            sprintf(Buffer, "%lld", k);
            answer += Buffer;
        }
        if (k != 0 && p != 0) answer += " ";
        if (k == 0 && p == 0) answer += "0";
        if (p != 0) {
            sprintf(Buffer, "%lld/%lld", p, B);
            answer += Buffer;
        }
        return answer;
    }

    Rational operator+(const Rational& that) {
        Rational answer = {A * that.B + that.A * B, B * that.B};
        return answer;
    }

    Rational operator-(const Rational& that) {
        Rational answer = {A * that.B - that.A * B, B * that.B};
        return answer;
    }

    Rational operator*(const Rational& that) {
        Rational answer = {A * that.A, B * that.B};
        return answer;
    }

    Rational operator/(const Rational& that) {
        Rational answer = {A * that.B, B * that.A};
        return answer;
    }
};

struct Solution {
    Rational r1, r2;

    void Solve() {
        scanf("%s", Buffer);
        r1.Assign(Buffer).Simplify();
        scanf("%s", Buffer);
        r2.Assign(Buffer).Simplify();
        string s1 = r1.ToString();
        string s2 = r2.ToString();
        printf("%s + %s = %s\n", s1.c_str(), s2.c_str(),
               (r1 + r2).Simplify().ToString().c_str());
        printf("%s - %s = %s\n", s1.c_str(), s2.c_str(),
               (r1 - r2).Simplify().ToString().c_str());
        printf("%s * %s = %s\n", s1.c_str(), s2.c_str(),
               (r1 * r2).Simplify().ToString().c_str());
        printf("%s / %s = %s\n", s1.c_str(), s2.c_str(),
               (r1 / r2).Simplify().ToString().c_str());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
