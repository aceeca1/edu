#include <cstdio>
#include <map>
using namespace std;

struct Solution {
    int N;
    map<int, int> ToCount;

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            int x;
            scanf("%d", &x);
            ++ToCount[x];
        }
        scanf("%d", &N);
        bool head = true;
        for (int i = 0; i < N; ++i) {
            int x;
            scanf("%d", &x);
            if (!head) putchar(' ');
            head = false;
            printf("%d", ToCount[x]);
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
