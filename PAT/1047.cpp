#include <cstdio>
#include <map>
using namespace std;

typedef map<int, int>::iterator Type1;

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N;
    map<int, int> ToScore;

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            int team, score;
            scanf("%d-%*d %d", &team, &score);
            ToScore[team] += score;
        }
        int team, score = 0;
        for (Type1 it = ToScore.begin(); it != ToScore.end(); ++it) {
            if (AssignMax(&score, it->second)) team = it->first;
        }
        printf("%d %d\n", team, score);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
