#include <algorithm>
#include <cstdio>
#include <string>
using namespace std;

char Buffer[80];

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        if (N % 1111 == 0) {
            printf("%d - %d = 0000\n", N, N);
            return;
        }
        while (true) {
            sprintf(Buffer, "%04d", N);
            string s = Buffer;
            sort(s.begin(), s.end());
            string s1 = s;
            reverse(s.begin(), s.end());
            string s2 = s;
            int num1, num2;
            sscanf(s1.c_str(), "%d", &num1);
            sscanf(s2.c_str(), "%d", &num2);
            int difference = num2 - num1;
            printf("%s - %s = %04d\n", s2.c_str(), s1.c_str(), difference);
            if (difference == 6174) break;
            N = difference;
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
