#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, M;
    vector<bool> Answers;
    vector<int> Scores;

    void Solve() {
        scanf("%d%d", &N, &M);
        Answers.resize(M);
        Scores.resize(M);
        for (int i = 0; i < M; ++i) scanf("%d", &Scores[i]);
        for (int i = 0; i < M; ++i) {
            int X;
            scanf("%d", &X);
            Answers[i] = X;
        }
        for (int i = 0; i < N; ++i) {
            int score = 0;
            for (int j = 0; j < M; ++j) {
                int X;
                scanf("%d", &X);
                if (X == Answers[j]) score += Scores[j];
            }
            printf("%d\n", score);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
