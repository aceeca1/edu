#include <algorithm>
#include <cstdio>
#include <string>
using namespace std;

struct Solution {
    int A, B, D;

    string Format(int k, int n) {
        string answer;
        while (n != 0) {
            answer += '0' + n % k;
            n /= k;
        }
        if (answer.empty()) return "0";
        reverse(answer.begin(), answer.end());
        return answer;
    }

    void Solve() {
        scanf("%d%d%d", &A, &B, &D);
        printf("%s\n", Format(D, A + B).c_str());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
