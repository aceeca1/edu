#include <cstdio>
#include <utility>
using namespace std;

struct Solution {
    int N1, M1, N2, M2, K;

    int GCD(int a1, int a2) {
        if (a2 == 0) return a1;
        return GCD(a2, a1 % a2);
    }

    void Solve() {
        scanf("%d/%d %d/%d %d", &N1, &M1, &N2, &M2, &K);
        if (N2 * M1 < N1 * M2) {
            swap(N1, N2);
            swap(M1, M2);
        }
        int lower = N1 * K / M1 + 1;
        int upper = (N2 * K - 1) / M2;
        bool head = true;
        for (int i = lower; i <= upper; ++i) {
            if (GCD(i, K) == 1) {
                if (!head) putchar(' ');
                head = false;
                printf("%d/%d", i, K);
            }
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
