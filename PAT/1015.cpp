#include <algorithm>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

struct Student {
    string Id;
    int Moral, Ability;
};

struct Judger {
    int H;

    int WhichClass(const Student& student) {
        if (H <= student.Moral && H <= student.Ability) return 1;
        if (H <= student.Moral) return 2;
        if (student.Ability < H && student.Ability <= student.Moral) return 3;
        return 4;
    }

    int Total(const Student& student) {
        return student.Moral + student.Ability;
    }

    bool operator()(const Student& s1, const Student& s2) {
        int class1 = WhichClass(s1);
        int class2 = WhichClass(s2);
        if (class1 < class2) return true;
        if (class2 < class1) return false;
        int total1 = Total(s1);
        int total2 = Total(s2);
        if (total1 < total2) return false;
        if (total2 < total1) return true;
        if (s1.Moral < s2.Moral) return false;
        if (s2.Moral < s1.Moral) return true;
        return s1.Id < s2.Id;
    }
};

char Buffer[9];

struct Solution {
    int N, L, H;
    vector<Student> Students;

    void Solve() {
        scanf("%d%d%d", &N, &L, &H);
        Students.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%s%d%d", Buffer, &Students[i].Moral, &Students[i].Ability);
            Students[i].Id = Buffer;
        }
        int newSize = 0;
        for (int i = 0; i < N; ++i) {
            if (L <= Students[i].Moral && L <= Students[i].Ability) {
                Students[newSize++] = Students[i];
            }
        }
        Students.resize(newSize);
        Judger judger;
        judger.H = H;
        sort(Students.begin(), Students.end(), judger);
        printf("%d\n", int(Students.size()));
        for (int i = 0; i < Students.size(); ++i) {
            printf("%s %d %d\n", Students[i].Id.c_str(), Students[i].Moral,
                   Students[i].Ability);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
