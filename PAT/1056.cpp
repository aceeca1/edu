#include <cstdio>
using namespace std;

struct Solution {
    int N, Sum;

    void Solve() {
        scanf("%d", &N);
        Sum = 0;
        for (int i = 0; i < N; ++i) {
            int x;
            scanf("%d", &x);
            Sum += x;
        }
        printf("%d\n", Sum * (N - 1) * 11);
    }
};

int main() {
    Solution().Solve();
    return 0;
}