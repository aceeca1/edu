#include <cstdio>
#include <map>
#include <string>
using namespace std;

const char* Mars1[] = {"tret", "tam", "hel", "maa", "huh", "tou", "kes",
                       "hei",  "elo", "syy", "lok", "mer", "jou"};
const char* Mars0[] = {"tret", "jan", "feb", "mar", "apr", "may", "jun",
                       "jly",  "aug", "sep", "oct", "nov", "dec"};
map<string, int> Mars;

char Buffer[80];

struct Solution {
    string S;

    string ToMars(int x) {
        int x1 = x / 13, x0 = x % 13;
        if (x1 == 0) return Mars0[x0];
        if (x0 == 0) return Mars1[x1];
        string answer = Mars1[x1];
        answer += " ";
        return answer += Mars0[x0];
    }

    int FromMars(string s) {
        int space = 0;
        while (space < s.size() && s[space] != ' ') ++space;
        if (space == s.size()) return Mars[s];
        string s1 = s.substr(0, space);
        string s0 = s.substr(space + 1);
        return Mars[s1] + Mars[s0];
    }

    void Solve() {
        fgets(Buffer, 80, stdin);
        S = Buffer;
        S.pop_back();
        if (isdigit(S[0])) {
            int x;
            sscanf(S.c_str(), "%d", &x);
            printf("%s\n", ToMars(x).c_str());
        } else {
            printf("%d\n", FromMars(S));
        }
    }
};

int main() {
    for (int i = 0; i < 13; ++i) {
        Mars[Mars1[i]] = i * 13;
        Mars[Mars0[i]] = i;
    }
    int t;
    scanf("%d\n", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
