#include <cstdio>
#include <string>
using namespace std;

struct Solution {
    int N;

    void Solve() {
        scanf("%d", &N);
        int n1 = N % 10;
        int n2 = N / 10 % 10;
        int n3 = N / 100;
        string s1(n3, 'B');
        string s2(n2, 'S');
        string s3(n1, 0);
        for (int i = 0; i < n1; ++i) s3[i] = '1' + i;
        printf("%s%s%s", s1.c_str(), s2.c_str(), s3.c_str());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
