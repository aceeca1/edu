#include <algorithm>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[80];

struct Solution {
    int N;
    vector<int> A;
    vector<vector<int> > B;

    string B0() {
        int sum = 0, count = 0;
        for (int i = 0; i < B[0].size(); ++i) {
            if (B[0][i] % 2 == 0) {
                sum += B[0][i];
                ++count;
            }
        }
        if (count == 0) return "N";
        sprintf(Buffer, "%d", sum);
        return Buffer;
    }

    string B1() {
        if (B[1].size() == 0) return "N";
        bool negative = false;
        int answer = 0;
        for (int i = 0; i < B[1].size(); ++i) {
            answer += negative ? -B[1][i] : B[1][i];
            negative = !negative;
        }
        sprintf(Buffer, "%d", answer);
        return Buffer;
    }

    string B2() {
        if (B[2].size() == 0) return "N";
        sprintf(Buffer, "%d", int(B[2].size()));
        return Buffer;
    }

    string B3() {
        if (B[3].size() == 0) return "N";
        int sum = 0;
        for (int i = 0; i < B[3].size(); ++i) sum += B[3][i];
        sprintf(Buffer, "%.1lf", double(sum) / B[3].size());
        return Buffer;
    }

    string B4() {
        if (B[4].size() == 0) return "N";
        sprintf(Buffer, "%d", *max_element(B[4].begin(), B[4].end()));
        return Buffer;
    }

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        B.resize(5);
        for (int i = 0; i < N; ++i) B[A[i] % 5].push_back(A[i]);
        printf("%s %s %s %s %s\n", B0().c_str(), B1().c_str(), B2().c_str(),
               B3().c_str(), B4().c_str());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
