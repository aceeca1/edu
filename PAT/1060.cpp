#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> Count;

    void Solve() {
        scanf("%d", &N);
        Count.resize(N + 2);
        for (int i = 0; i < N; ++i) {
            int X;
            scanf("%d", &X);
            if (N + 1 < X) X = N + 1;
            ++Count[X];
        }
        for (int i = N; i >= 0; --i) Count[i] += Count[i + 1];
        int eddington = N;
        while (Count[eddington + 1] < eddington) --eddington;
        printf("%d\n", eddington);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
