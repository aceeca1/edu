#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[80];

struct Solution {
    string S;
    vector<string> Hand, Eye, Mouth;
    int N;

    void Parse(vector<string>* collection) {
        int position1 = 0;
        while (true) {
            while (position1 < S.size() && S[position1] != '[') ++position1;
            if (position1 == S.size()) break;
            int position2 = position1 + 1;
            while (position2 < S.size() && S[position2] != ']') ++position2;
            string s = S.substr(position1 + 1, position2 - position1 - 1);
            collection->push_back(s);
            position1 = position2 + 1;
        }
    }

    void Solve() {
        fgets(Buffer, 80, stdin);
        S = Buffer;
        Parse(&Hand);
        fgets(Buffer, 80, stdin);
        S = Buffer;
        Parse(&Eye);
        fgets(Buffer, 80, stdin);
        S = Buffer;
        Parse(&Mouth);
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            int leftHand, leftEye, mouth, rightEye, rightHand;
            scanf("%d%d%d%d%d", &leftHand, &leftEye, &mouth, &rightEye,
                  &rightHand);
            if (leftHand <= 0 || Hand.size() < leftHand || leftEye <= 0 ||
                Eye.size() < leftEye || mouth <= 0 || Mouth.size() < mouth ||
                rightEye <= 0 || Eye.size() < rightEye || rightHand <= 0 ||
                Hand.size() < rightHand) {
                printf("Are you kidding me? @\\/@\n");
            } else {
                printf("%s(%s%s%s)%s\n", Hand[leftHand - 1].c_str(),
                       Eye[leftEye - 1].c_str(), Mouth[mouth - 1].c_str(),
                       Eye[rightEye - 1].c_str(), Hand[rightHand - 1].c_str());
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
