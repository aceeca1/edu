#include <cstdio>
#include <string>
using namespace std;

typedef long long int64;

char Buffer[11];

struct Solution {
    string A, B;
    char DA, DB;

    int64 P(const string& s, char d) {
        int64 answer = 0;
        for (int i = 0; i < s.size(); ++i) {
            if (s[i] == d) answer = answer * 10 + (d - '0');
        }
        return answer;
    }

    void Solve() {
        scanf("%s %c", Buffer, &DA);
        A = Buffer;
        scanf("%s %c", Buffer, &DB);
        B = Buffer;
        printf("%lld\n", P(A, DA) + P(B, DB));
    }
};

int main() {
    Solution().Solve();
    return 0;
}
