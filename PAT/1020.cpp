#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Mooncake {
    double Amount, Price;

    bool operator<(const Mooncake& that) { return that.Price < Price; }
};

struct Solution {
    int N;
    double D;
    vector<Mooncake> Mooncakes;

    void Solve() {
        scanf("%d%lf", &N, &D);
        Mooncakes.resize(N);
        for (int i = 0; i < N; ++i) scanf("%lf", &Mooncakes[i].Amount);
        for (int i = 0; i < N; ++i) {
            double total;
            scanf("%lf", &total);
            Mooncakes[i].Price = total / Mooncakes[i].Amount;
        }
        sort(Mooncakes.begin(), Mooncakes.end());
        double total = 0.0;
        for (int i = 0; i < N; ++i) {
            if (D <= Mooncakes[i].Amount) {
                total += Mooncakes[i].Price * D;
                break;
            } else {
                total += Mooncakes[i].Price * Mooncakes[i].Amount;
                D -= Mooncakes[i].Amount;
            }
        }
        printf("%.2lf\n", total);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
