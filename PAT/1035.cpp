#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, M;
    vector<int> A, B, A1;

    void MergeSortNext() {
        for (int i = 0; i < N; i += M + M) {
            int end = min(i + M + M, N);
            sort(A1.begin() + i, A1.begin() + end);
        }
    }

    bool MergeSort() {
        M = 1;
        while (M < N) {
            MergeSortNext();
            M += M;
            if (A1 == B) return true;
        }
        return false;
    }

    void InsertionSortNext() {
        int end = N;
        // Should be 2 here.
        while (3 <= end && A[end - 1] == B[end - 1]) --end;
        sort(B.begin(), B.begin() + 1 + end);
        A1 = B;
    }

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        B.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &B[i]);
        A1 = A;
        if (MergeSort()) {
            printf("Merge Sort\n");
            MergeSortNext();
        } else {
            printf("Insertion Sort\n");
            InsertionSortNext();
        }
        bool head = true;
        for (int i = 0; i < N; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", A1[i]);
        }
        printf("\n");
    }
};

int main() {
    Solution().Solve();
    return 0;
}
