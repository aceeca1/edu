// Wrong Problem Description
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <string>
using namespace std;

char Buffer[61];

const char* DayName[] = {"MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"};

struct Solution {
    string S1, S2, S3, S4;

    int FindSame1(const string& s1, const string& s2, int k) {
        int size = min(s1.size(), s2.size());
        for (int i = k; i < size; ++i) {
            if (s1[i] == s2[i] && 'A' <= s1[i] && s1[i] <= 'G') return i;
        }
        return -1;
    }

    int FindSame2(const string& s1, const string& s2, int k) {
        int size = min(s1.size(), s2.size());
        for (int i = k; i < size; ++i) {
            if (s1[i] == s2[i] && isdigit(s1[i])) return i;
            if (s1[i] == s2[i] && 'A' <= s1[i] && s1[i] <= 'N') return i;
        }
        return -1;
    }

    int FindSame3(const string& s1, const string& s2, int k) {
        int size = min(s1.size(), s2.size());
        for (int i = k; i < size; ++i) {
            if (s1[i] == s2[i] && isalpha(s1[i])) return i;
        }
        return -1;
    }

    void Solve() {
        scanf("%s", Buffer);
        S1 = Buffer;
        scanf("%s", Buffer);
        S2 = Buffer;
        scanf("%s", Buffer);
        S3 = Buffer;
        scanf("%s", Buffer);
        S4 = Buffer;
        int k1 = FindSame1(S1, S2, 0);
        int k2 = FindSame2(S1, S2, k1 + 1);
        int k3 = FindSame3(S3, S4, 0);
        int day = S1[k1] - 'A';
        int hour = isalpha(S1[k2]) ? S1[k2] - 'A' + 10 : S1[k2] - '0';
        int minute = k3;
        printf("%s %02d:%02d\n", DayName[day], hour, minute);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
