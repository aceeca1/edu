#include <cstdio>
#include <climits>
#include <map>
#include <vector>
using namespace std;

struct Edge {
    int Target;
    long long U;
};

struct Graph {
    vector<Edge> Edges;
    vector<vector<int> > Out;
    int Source, Target;
    vector<int> H, Begin, SizeH;
    vector<long long> Excess;
    multimap<int, int> Q;

    void AddEdge(int s, int t, long long u) {
        Edge e1 = {t, u}, e2 = {s, 0};
        Edges.push_back(e1);
        Edges.push_back(e2);
        Out[s].push_back(Edges.size() - 2);
        Out[t].push_back(Edges.size() - 1);
    }

    void Push(int no, int ei) {
        int target = Edges[ei].Target;
        if (!(no == Source || H[target] + 1 == H[no])) return;
        long long delta = min(Excess[no], Edges[ei].U);
        if (delta == 0) return;
        Excess[no] -= delta;
        Excess[target] += delta;
        Edges[ei].U -= delta;
        Edges[ei ^ 1].U += delta;
        if (target == Source || target == Target) return;
        Q.insert(make_pair(H[target], target));
    }

    void Relabel(int no) {
        if (no == Source || no == Target) return;
        Begin[no] = 0;
        if (--SizeH[H[no]] == 0) {
            for (int i = 0; i < Out.size(); ++i) {
                if (H[no] < H[i]) H[i] = Out.size();
            }
        }
        Q.insert(make_pair(++H[no], no));
        ++SizeH[H[no]];
    }

    long long MaxFlow() {
        H.resize(Out.size());
        H[Source] = Out.size();
        SizeH.resize(Out.size() + Out.size() + 1);
        SizeH[0] = Out.size() - 1;
        SizeH[Out.size()] = 1;
        Begin.resize(Out.size());
        Excess.resize(Out.size());
        Excess[Source] = LONG_LONG_MAX;
        Q.insert(make_pair(H[Source], Source));
        while (!Q.empty()) {
            int no = Q.rbegin()->second;
            Q.erase(--Q.end());
            if (Excess[no] == 0) continue;
            for (int& i = Begin[no]; i < Out[no].size(); ++i) {
                Push(no, Out[no][i]);
                if (Excess[no] == 0) break;
            }
            if (0 < Excess[no]) Relabel(no);
        }
        return Excess[Target];
    }
};

struct Solution {
    int N, M;
    Graph G;

    void Solve() {
        scanf("%d%d%d%d", &N, &M, &G.Source, &G.Target);
        G.Out.resize(N + 1);
        for (int i = 0; i < M; ++i) {
            int u, v, c;
            scanf("%d%d%d", &u, &v, &c);
            G.AddEdge(u, v, c);
        }
        printf("%lld\n", G.MaxFlow());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
