#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> A;

    void Solve() {
        scanf("%d", &N);
        A.resize(N + N);
        for (int i = 0; i < A.size(); ++i) scanf("%d", &A[i]);
        sort(A.begin(), A.end());
        int answer = 0;
        for (int i = 0; i < A.size(); i += 2) answer += A[i];
        printf("%d\n", answer);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
