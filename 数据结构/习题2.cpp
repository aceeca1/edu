#include <cstdio>
#include <climits>
#include <vector>
using namespace std;

struct Node {
    Node* Next;
    int Data;
};

Node* CreateLinkedList(const vector<int>& input) {
    Node *head = new Node, *tail = head;
    for (int i : input) {
        tail->Next = new Node;
        tail->Next->Data = i;
        tail = tail->Next;
    }
    tail->Next = nullptr;
    return head;
}

void PrintLinkedList(const char* label, const Node* input) {
    printf("%s: ", label);
    for (const Node *i = input->Next; i; i = i->Next) {
        printf("%d, ", i->Data);
    }
    printf("\n");
}

Node* RemoveAndGetNext(Node* input) {
    Node* next = input->Next;
    delete input;
    return next;
}

void Recycle(Node* input) {
    while (input) input = RemoveAndGetNext(input);
}

void PrintVector(const char* label, const vector<int>& input) {
    printf("%s: ", label);
    for (int i: input) printf("%d, ", i);
    printf("\n");
}
    
// 习题 2.1: 
// 将两个递增的有序链表合并为一个递增的有序链表。
// 要求结果链表仍使用原来两个链表的存储空间, 不另外占用其它的存储空间。
// 表中不允许有重复的数据。
Node* Merge(Node* list1, Node* list2) {
    Node *head = list1, *tail = head;
    list1 = list1->Next;
    list2 = RemoveAndGetNext(list2);
    while (list1 && list2) {
        if (list1->Data < list2->Data) {
            tail->Next = list1;
            tail = list1;
            list1 = list1->Next;
        } else if (list2->Data < list1->Data) {
            tail->Next = list2;
            tail = list2;
            list2 = list2->Next;
        } else {
            tail->Next = list1;
            tail = list1;
            list1 = list1->Next;
            list2 = RemoveAndGetNext(list2);
        }
    }
    tail->Next = list1 ? list1 : list2;
    return head;
}

void ex2_1() {
    printf("==========习题2.1==========\n");
    Node *list1 = CreateLinkedList({1, 2, 4, 5, 7});
    PrintLinkedList("list1", list1);
    Node *list2 = CreateLinkedList({3, 4, 6, 8});
    PrintLinkedList("list2", list2);
    Node *merged = Merge(list1, list2);
    PrintLinkedList("merged", merged);
    Recycle(merged);
}

// 习题 2.2:
// 已知两个链表A和B分别表示两个集合，其元素递增排列。
// 请设计算法求出A与B的交集，并存放于A链表中。
// 原来的两个链表所占的空间，如果没有在结果中使用的，请回收。
void Intersect(Node* a, Node* b) {
    Node *head = a, *tail = a;
    a = a->Next;
    b = RemoveAndGetNext(b);
    while (a) {
        while (b && b->Data < a->Data) b = RemoveAndGetNext(b);
        if (b && b->Data == a->Data) {
            tail->Next = a;
            tail = a;
            a = a->Next;
        } else {
            a = RemoveAndGetNext(a);
        }
    }
    tail->Next = nullptr;
    while (b) b = RemoveAndGetNext(b);
}

void ex2_2() {
    printf("==========习题2.2==========\n");
    Node *list1 = CreateLinkedList({1, 2, 4, 5, 7});
    PrintLinkedList("list1", list1);
    Node *list2 = CreateLinkedList({3, 4, 6, 7, 8});
    PrintLinkedList("list2", list2);
    Intersect(list1, list2);
    PrintLinkedList("intersect", list1);
    Recycle(list1);
}

// 习题 2.3:
// 设计算法将一个带头结点的单链表A分解为两个具有相同结构的链表B、C，
// 其中B表的结点为A表中值小于零的结点，
// 而C表的结点为A表中值大于零的结点
// （链表A的元素类型为整型，要求B、C表利用A表的结点）。
void Dispatch(Node* list, Node*& list1, Node*& list2) {
    list1 = new Node;
    list2 = new Node;
    Node *tail1 = list1, *tail2 = list2;
    list = RemoveAndGetNext(list);
    while (list) {
        if (list->Data < 0) {
            tail1->Next = list;
            tail1 = list;
        } else {
            tail2->Next = list;
            tail2 = list;
        }
        list = list->Next;
    }
    tail1->Next = tail2->Next = nullptr;
}

void ex2_3() {
    printf("==========习题2.3==========\n");
    Node *list = CreateLinkedList({1, -2, 4, -5, 7});
    PrintLinkedList("list", list);
    Node *list1, *list2;
    Dispatch(list, list1, list2);
    PrintLinkedList("list1", list1);
    PrintLinkedList("list2", list2);
    Recycle(list1);
    Recycle(list2);
}

// 习题 2.4:
// 设计一个算法，通过一趟遍历在单链表中确定值最大的结点。
int GetMax(Node* list) {
    list = list->Next;
    int answer = INT_MIN;
    while (list) {
        if (answer < list->Data) answer = list->Data;
        list = list->Next;
    }
    return answer;
}

void ex2_4() {
    printf("==========习题2.4==========\n");
    Node *list = CreateLinkedList({1, 5, 3, 2, 4});
    PrintLinkedList("list", list);
    printf("最大值: %d\n", GetMax(list));
    Recycle(list);
}

// 习题 2.5:
// 设计一个算法，通过遍历一趟，将链表中所有结点的链接方向逆转，
// 仍利用原表的存储空间。
void Reverse(Node* list) {
    Node *origin = list, *head = nullptr;
    list = list->Next;
    while (list) {
        Node* next = list->Next;
        list->Next = head;
        head = list;
        list = next;
    }
    origin->Next = head;
}

void ex2_5() {
    printf("==========习题2.5==========\n");
    Node *list = CreateLinkedList({1, 5, 3, 2, 4});
    PrintLinkedList("list", list);
    Reverse(list);
    PrintLinkedList("list", list);
    Recycle(list);
}    

// 习题 2.6: 
// 设计一个算法，删除递增有序链表中值大于mink且小于maxk的所有元素
// （mink和maxk是给定的两个参数，其值可以和表中的元素相同，也可以不同 ）。
void RemoveItems(Node* list, int minK, int maxK) {
    Node *head = list, *tail = list;
    list = list->Next;
    while (list) {
        if (minK < list->Data && list->Data < maxK) {
            list = RemoveAndGetNext(list);
        } else {
            tail->Next = list;
            tail = list;
            list = list->Next;
        }
    }
    tail->Next = nullptr;
}

void ex2_6() {
    printf("==========习题2.6==========\n");
    Node *list = CreateLinkedList({1, 2, 3, 5, 7, 9});
    PrintLinkedList("list", list);
    RemoveItems(list, 3, 8);
    PrintLinkedList("list", list);
    Recycle(list);
}

// 习题 2.7:
// 已知长度为n的线性表A采用顺序存储结构，
// 请写一时间复杂度为O(n)、空间复杂度为O(1)的算法，
// 该算法删除线性表中所有值为item的数据元素。
void RemoveAll(vector<int>& vec, int num) {
    int newSize = 0;
    for (int i = 0; i < vec.size(); ++i) {
        if (vec[i] != num) vec[newSize++] = vec[i];
    }
    vec.resize(newSize);
}

void ex2_7() {
    printf("==========习题2.7==========\n");
    vector<int> vec{1, 1, 2, 1, 2, 3, 1, 1};
    PrintVector("vec", vec);
    RemoveAll(vec, 1);
    PrintVector("vec", vec);
}

int main() {
    ex2_1();
    ex2_2();
    ex2_3();
    ex2_4();
    ex2_5();
    ex2_6();
    ex2_7();
    return 0;
}
