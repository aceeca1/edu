#include <cstdio>
using namespace std;

double Need;

int main() {
    scanf("%lf", &Need);
    double step = 2.0;
    int answer = 0;
    while (Need > 0) {
        Need -= step;
        step *= 0.98;
        ++answer;
    }
    printf("%d\n", answer);
    return 0;
}
