#include <cstdio>
using namespace std;

int A, B, C, D;

int main() {
    scanf("%d%d%d%d", &A, &B, &C, &D);
    int start = A * 60 + B;
    int end = C * 60 + D;
    int duration = end - start;
    printf("%d %d\n", duration / 60, duration % 60);
    return 0;
}