#include <cstdio>
using namespace std;

int N;

int main() {
    scanf("%d", &N);
    int part1 = N < 150 ? N : 150;
    int part2 = N < 150 ? 0 : N < 400 ? N - 150 : 250;
    int part3 = N < 400 ? 0 : N - 400;
    double cost1 = part1 * 0.4463;
    double cost2 = part2 * 0.4663;
    double cost3 = part3 * 0.5663;
    printf("%.1lf\n", cost1 + cost2 + cost3);
    return 0;
}
