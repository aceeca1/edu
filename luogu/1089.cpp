#include <cstdio>
using namespace std;

int main() {
    int remain = 0, mother = 0;
    for (int i = 1; i <= 12; ++i) {
        int cost;
        scanf("%d", &cost);
        remain += 300;
        if (remain < cost) {
            printf("%d\n", -i);
            return 0;
        }
        remain -= cost;
        int save = remain / 100 * 100;
        remain -= save;
        mother += save;
    }
    printf("%d\n", remain + mother / 5 * 6);
    return 0;
}
