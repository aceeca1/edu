#include <cstdio>
using namespace std;

int main() {
    int maxUnhappiness = 8, arg = 0;
    for (int i = 1; i <= 7; ++i) {
        int a, b;
        scanf("%d%d", &a, &b);
        if (maxUnhappiness < a + b) {
            maxUnhappiness = a + b;
            arg = i;
        }
    }
    printf("%d\n", arg);
}
