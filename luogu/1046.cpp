#include <cstdio>
#include <vector>
using namespace std;

vector<int> A(10);
int Taotao;

int main() {
    for (int i = 0; i < 10; ++i) scanf("%d", &A[i]);
    scanf("%d", &Taotao);
    int answer = 0;
    for (int i = 0; i < 10; ++i) {
        if (A[i] <= Taotao + 30) ++answer;
    }
    printf("%d\n", answer);
    return 0;
}
