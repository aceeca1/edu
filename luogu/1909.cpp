#include <cstdio>
#include <climits>
using namespace std;

int N;

int main() {
    scanf("%d", &N);
    int answer = INT_MAX;
    for (int i = 0; i < 3; ++i) {
        int num, price;
        scanf("%d%d", &num, &price);
        int total = (N + num - 1) / num * price;
        if (total < answer) answer = total;
    }
    printf("%d\n", answer);
    return 0;
}