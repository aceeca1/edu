#include <cstdio>
using namespace std;

int K, N;

int main() {
    scanf("%d", &K);
    double a = 0.0;
    N = 1;
    while (true) {
        a += 1.0 / N;
        if (K < a) break;
        ++N;
    }
    printf("%d\n", N);
    return 0;
}
