#include <cstdio>
#include <vector>
using namespace std;

vector<int> A;

int main() {
    while (true) {
        int x;
        scanf("%d", &x);
        if (x == 0) break;
        A.push_back(x);
    }
    bool head = true;
    for (int i = A.size() - 1; i >= 0; --i) {
        if (!head) putchar(' ');
        head = false;
        printf("%d", A[i]);
    }
    printf("\n");
    return 0;
}
