#include <cstdio>
#include <vector>
using namespace std;

int L, M;
vector<bool> Removed;

int main() {
    scanf("%d%d", &L, &M);
    Removed.resize(L + 1);
    for (int i = 0; i < M; ++i) {
        int v1, v2;
        scanf("%d%d", &v1, &v2);
        for (int j = v1; j <= v2; ++j) Removed[j] = true;
    }
    int answer = 0;
    for (int i = 0; i <= L; ++i) {
        if (!Removed[i]) ++answer;
    }
    printf("%d\n", answer);
    return 0;
}
