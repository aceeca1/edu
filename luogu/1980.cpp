#include <cstdio>
using namespace std;

int N, X;

int HowMany(int n) {
    int answer = 0;
    while (n != 0) {
        if (n % 10 == X) ++answer;
        n /= 10;
    }
    return answer;
}

int main() {
    scanf("%d%d", &N, &X);
    int answer = 0;
    for (int i = 1; i <= N; ++i) {
        answer += HowMany(i);
    }
    printf("%d\n", answer);
}