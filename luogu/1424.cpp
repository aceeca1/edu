#include <cstdio>
using namespace std;

int X, N;

int main() {
    scanf("%d%d", &X, &N);
    int answer = N / 7 * 5 * 250;
    N %= 7;
    for (int i = 0; i < N; ++i) {
        if ((X + i + 1) % 7 < 2) continue;
        answer += 250;
    }
    printf("%d\n", answer);
}
