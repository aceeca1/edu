#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

vector<int> A;
int A1, A2, A3;

void Search(int n) {
    if (n == 3) {
        A1 = A[0] * 100 + A[1] * 10 + A[2];
    }
    if (n == 6) {
        A2 = A[3] * 100 + A[4] * 10 + A[5];
        if (A1 * 2 != A2) return;       
    }
    if (n == 9) {
        A3 = A[6] * 100 + A[7] * 10 + A[8];
        if (A1 * 3 != A3) return;
        printf("%d %d %d\n", A1, A2, A3);
    }
    for (int i = n; i < 9; ++i) {
        swap(A[n], A[i]);
        Search(n + 1);
        swap(A[n], A[i]);
    }
}

int main() {
    A.resize(9);
    for (int i = 0; i < 9; ++i) A[i] = i + 1;
    Search(0);
}