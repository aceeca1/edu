#include <cstdio>
#include <string>
using namespace std;

char Buffer[81];

struct Solution {
    string S;

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        int answer = 0, current = 0;
        for (int i = 0; i < S.size(); ++i) {
            if (S[i] == 'X') {
                current = 0;
            } else {
                answer += ++current;
            }
        }
        printf("%d\n", answer);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}