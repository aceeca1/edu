#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[7];

struct Solution {
    vector<string> Board;
    int X, Y;
    bool Failure;

    void FindSpace() {
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                if (Board[i][j] == ' ') {
                    X = i;
                    Y = j;
                }
            }
        }
    }

    void MoveA() {
        if (X == 0) {
            Failure = true;
        } else {
            swap(Board[X][Y], Board[X - 1][Y]);
            X = X - 1;
        }
    }

    void MoveB() {
        if (X == 4) {
            Failure = true;
        } else {
            swap(Board[X][Y], Board[X + 1][Y]);
            X = X + 1;
        }
    }

    void MoveL() {
        if (Y == 0) {
            Failure = true;
        } else {
            swap(Board[X][Y], Board[X][Y - 1]);
            Y = Y - 1;
        }
    }

    void MoveR() {
        if (Y == 4) {
            Failure = true;
        } else {
            swap(Board[X][Y], Board[X][Y + 1]);
            Y = Y + 1;
        }
    }

    bool Solve(int caseNo) {
        Board.resize(5);
        for (int i = 0; i < 5; ++i) {
            fgets(Buffer, 7, stdin);
            Board[i] = Buffer;
            if (Board[i] == "Z\n") return false;
        }
        if (caseNo != 1) printf("\n");
        printf("Puzzle #%d:\n", caseNo);
        Failure = false;
        FindSpace();
        while (true) {
            int c = getchar();
            if (c == '\n') continue;
            if (c == '0') break;
            switch (c) {
                case 'A': MoveA(); break;
                case 'B': MoveB(); break;
                case 'L': MoveL(); break;
                case 'R': MoveR(); break;
            }
        }
        while (getchar() != '\n');
        if (Failure) {
            printf("This puzzle has no final configuration.\n");
        } else {
            for (int i = 0; i < 5; ++i) {
                printf("%c %c %c %c %c\n",
                    Board[i][0], Board[i][1], Board[i][2],
                    Board[i][3], Board[i][4]);
            }
        }
        return true;
    }
};

int main() {
    int caseNo = 1;
    while (Solution().Solve(caseNo)) ++caseNo;
    return 0;
}
