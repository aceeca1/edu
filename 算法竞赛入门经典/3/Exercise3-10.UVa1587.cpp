#include <cstdio>
#include <set>
#include <vector>
using namespace std;

typedef unsigned long long uint64;
typedef set<int>::iterator Type1;

uint64 Fingerprint(uint64 x) {
    const uint64 kMul = 0x9ddfea08eb382d69ULL;
    x *= kMul, x ^= x >> 47;
    x *= kMul, x ^= x >> 47;
    x *= kMul, x ^= x >> 47;
    return x * kMul;
}

struct Solution {
    vector<int> A;
    set<int> SetA;

    bool Possible() {
        vector<int> S(SetA.begin(), SetA.end());
        if (S.size() == 1) return Possible(S[0], S[0], S[0]);
        if (S.size() == 2) return Possible(S[0], S[0], S[1]) ||
                                  Possible(S[0], S[1], S[1]);
        if (S.size() == 3) return Possible(S[0], S[1], S[2]);
        return false;
    }

    bool Possible(int S0, int S1, int S2) {
        uint64 hash1 = Fingerprint(S0) + Fingerprint(S1);
        uint64 hash2 = Fingerprint(S0) + Fingerprint(S2);
        uint64 hash3 = Fingerprint(S1) + Fingerprint(S2);
        uint64 hash = (Fingerprint(hash1) + 
            Fingerprint(hash2) + Fingerprint(hash3)) << 1;
        uint64 hashA = 0;
        for (int i = 0; i < 12; i += 2) {
            hashA += Fingerprint(Fingerprint(A[i]) + Fingerprint(A[i + 1]));
        }
        return hash == hashA;
    }

    bool Solve() {
        A.resize(12);
        for (int i = 0; i < 12; ++i) {
            if (scanf("%d", &A[i]) != 1) return false;
            SetA.insert(A[i]);
        }
        printf(Possible() ? "POSSIBLE\n" : "IMPOSSIBLE\n");
        return true; 
    }
};

int main() {
    while (Solution().Solve());
    return 0;
}
