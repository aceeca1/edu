#include <cstdio>
#include <iostream>
using namespace std;

struct Solution {
    string S1, S2;

    bool Subsequence() {
        int current = 0;
        for (int i = 0; i < S2.size(); ++i) {
            if (S2[i] == S1[current]) ++current;
            if (S1.size() <= current) return true;
        }
        return false;
    }

    void Solve() {
        while (cin >> S1 >> S2) {
            printf(Subsequence() ? "Yes\n" : "No\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
