#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[11];

struct Solution {
    vector<string> Board;
    vector<pair<int, int> > Start;
    int R, C;

    bool Solve(int caseNo) {
        scanf("%d", &R);
        if (R == 0) return false;
        scanf("%d", &C);
        Board.resize(R);
        for (int i = 0; i < R; ++i) {
            scanf("%s", Buffer);
            Board[i] = Buffer;
        }
        if (caseNo != 1) printf("\n");
        printf("puzzle #%d:\n", caseNo);
        for (int i = 0; i < R; ++i) {
            for (int j = 0; j < C; ++j) {
                if (Board[i][j] == '*') continue;
                bool startV = i == 0 || Board[i - 1][j] == '*';
                bool startH = j == 0 || Board[i][j - 1] == '*';
                if (startV || startH) {
                    Start.push_back(make_pair(i, j));
                }
            }
        }
        printf("Across\n");
        for (int i = 0; i < Start.size(); ++i) {
            int no = i + 1;
            int x = Start[i].first;
            int y = Start[i].second;
            if (y == 0 || Board[x][y - 1] == '*') {
                string answer;
                for (int i = 0; ; ++i) {
                    if (C <= y + i || Board[x][y + i] == '*') break;
                    answer += Board[x][y + i];
                }
                printf("%3d.%s\n", no, answer.c_str());
            }
        }
        printf("Down\n");
        for (int i = 0; i < Start.size(); ++i) {
            int no = i + 1;
            int x = Start[i].first;
            int y = Start[i].second;
            if (x == 0 || Board[x - 1][y] == '*') {
                string answer;
                for (int i = 0; ; ++i) {
                    if (R <= x + i || Board[x + i][y] == '*') break;
                    answer += Board[x + i][y];
                }
                printf("%3d.%s\n", no, answer.c_str());
            }
        }
        return true;
    }
};

int main() {
    int caseNo = 1;
    while (Solution().Solve(caseNo)) ++caseNo;
    return 0;
}