#include <cstdio>
#include <string>
using namespace std;

char Buffer[81];
bool Head = true;

struct Solution {
    string S;

    int MinPeriod() {
        for (int i = 1; i <= S.size(); ++i) {
            if (S.size() % i != 0) continue;
            if (S.substr(0, S.size() - i) != S.substr(i)) continue;
            return i;
        }
        return 0;
    }

    void Solve() {
        if (!Head) printf("\n");
        Head = false;
        scanf("%s", Buffer);
        S = Buffer;
        printf("%d\n", MinPeriod());
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}