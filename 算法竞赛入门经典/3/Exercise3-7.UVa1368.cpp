#include <cstdio>
#include <string>
#include <vector>
using namespace std;

bool AssignMax(int* p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

char Buffer[1001];

struct Solution {
    vector<string> DNA;
    string Voted;
    int M, N;

    void Solve() {
        scanf("%d%d", &M, &N);
        DNA.resize(M);
        for (int i = 0; i < M; ++i) {
            scanf("%s", Buffer);
            DNA[i] = Buffer;
        }
        Voted.resize(N);
        for (int i = 0; i < N; ++i) {
            int nA = 0, nC = 0, nG = 0, nT = 0;
            for (int j = 0; j < M; ++j) {
                switch (DNA[j][i]) {
                    case 'A': ++nA; break;
                    case 'C': ++nC; break;
                    case 'G': ++nG; break;
                    case 'T': ++nT; break;
                }
            }
            int max = -1;
            if (AssignMax(&max, nA)) Voted[i] = 'A';
            if (AssignMax(&max, nC)) Voted[i] = 'C';
            if (AssignMax(&max, nG)) Voted[i] = 'G';
            if (AssignMax(&max, nT)) Voted[i] = 'T';
        }
        printf("%s\n", Voted.c_str());
        int answer = 0;
        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                if (DNA[i][j] != Voted[j]) ++answer;
            }
        }
        printf("%d\n", answer);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}