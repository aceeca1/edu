#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Query {
    int Input, No;
    vector<int> Output;

    bool operator<(const Query& that) {
        return Input < that.Input;
    }
};

struct Solution {
    int T;
    vector<Query> Queries;
    vector<int> A;

    void AddToA(int n) {
        while (n != 0) {
            ++A[n % 10];
            n /= 10;
        }
    }

    void Solve() {
        scanf("%d", &T);
        Queries.resize(T);
        for (int i = 0; i < T; ++i) {
            scanf("%d", &Queries[i].Input);
            Queries[i].No = i;
        }
        sort(Queries.begin(), Queries.end());
        int current = 1;
        A.resize(10);
        for (int i = 0; i < Queries.size(); ++i) {
            while (current <= Queries[i].Input) AddToA(current++);
            Queries[i].Output = A;
        }
        vector<int> order(Queries.size());
        for (int i = 0; i < Queries.size(); ++i) {
            order[Queries[i].No] = i;
        }
        for (int i = 0; i < order.size(); ++i) {
            bool head = true;
            for (int j = 0; j < 10; ++j) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", Queries[order[i]].Output[j]);
            }
            printf("\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}