#include <cstdio>
#include <climits>
#include <string>
using namespace std;

char Buffer[101];

bool AssignMin(int *p, int v) {
    if (v < *p) return *p = v, true;
    return false;
}

bool AssignMax(int *p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    string S1, S2;

    bool Solve() {
        if (scanf("%s", Buffer) != 1) return false;
        S1 = Buffer;
        if (scanf("%s", Buffer) != 1) return false;
        S2 = Buffer;
        int answer = INT_MAX;
        for (int offset = -int(S2.size()); offset < int(S1.size()); ++offset) {
            int thick = 0;
            for (int i2 = 0; i2 < S2.size(); ++i2) {
                int i1 = i2 + offset;
                if (0 <= i1 && i1 < S1.size()) {
                    AssignMax(&thick, S1[i1] - '0' + S2[i2] - '0');
                }
            }
            if (thick <= 3) {
                int left = min(offset, 0);
                int right = max(offset + S2.size(), S1.size());
                AssignMin(&answer, right - left);
            }
        }
        printf("%d\n", answer);
        return true;
    }
};

int main() {
    while (Solution().Solve());
    return 0;
}