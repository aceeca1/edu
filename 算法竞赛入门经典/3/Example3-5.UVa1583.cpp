#include <cstdio>
#include <vector>
using namespace std;

const int M = 100000;

struct Solution {
    vector<int> Generator;
    int T, N;

    int DigitSum(int x) {
        int answer = 0;
        while (x != 0) {
            answer += x % 10;
            x /= 10;
        }
        return answer;
    }
    
    void Solve() {
        Generator.resize(M + 1);
        for (int i = M; i >= 1; --i) {
            int x = i + DigitSum(i);
            if (x <= M) Generator[x] = i;
        }
        scanf("%d", &T);
        for (int i = 0; i < T; ++i) {
            scanf("%d", &N);
            printf("%d\n", Generator[N]);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}