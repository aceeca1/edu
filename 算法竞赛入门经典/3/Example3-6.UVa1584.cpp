#include <cstdio>
#include <string>
using namespace std;

char Buffer[101];

struct Solution {
    string S, Answer;

    void Solve() {
        scanf("%s", Buffer);
        S = Answer = Buffer;
        for (int i = 0; i < S.size(); ++i) {
            string s = S.substr(i) + S.substr(0, i);
            if (s < Answer) Answer = s;
        }
        printf("%s\n", Answer.c_str());
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
