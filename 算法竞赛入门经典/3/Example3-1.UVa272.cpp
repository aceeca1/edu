#include <cstdio>
using namespace std;

struct Solution {
    void Solve() {
        bool open = true;
        while (true) {
            int c = getchar();
            if (c == EOF) break;
            if (c == '"') {
                printf(open ? "``" : "''");
                open = !open;
            } else {
                putchar(c);
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}

