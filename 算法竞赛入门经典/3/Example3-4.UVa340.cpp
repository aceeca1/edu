#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<int> Question, Answer;

    bool Solve(int caseNo) {
        scanf("%d", &N);
        if (N == 0) return false;
        printf("Game %d:\n", caseNo);
        Answer.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &Answer[i]);
        Question.resize(N);
        while (true) {
            for (int i = 0; i < N; ++i) scanf("%d", &Question[i]);
            if (Question[0] == 0) break;
            int a = 0, b = 0;
            vector<int> aStat(10), qStat(10);
            for (int i = 0; i < N; ++i) {
                if (Answer[i] == Question[i]) ++a;
                ++aStat[Answer[i]];
                ++qStat[Question[i]];
            }
            for (int i = 0; i < 10; ++i) b += min(aStat[i], qStat[i]);
            printf("    (%d,%d)\n", a, b - a);
        }
        return true;
    }
};

int main() {
    int caseNo = 1;
    while (Solution().Solve(caseNo)) ++caseNo;
    return 0;
}
