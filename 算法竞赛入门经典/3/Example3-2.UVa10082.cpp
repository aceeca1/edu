#include <cstdio>
using namespace std;

const char S[] = "`1234567890-=QWERTYUIOP[]\\ASDFGHJKL;'ZXCVBNM,./";
int Index[256];

struct Solution {
    void Solve() {
        for (int i = 0; i < 256; ++i) Index[i] = -1;
        for (int i = 0; S[i]; ++i) Index[S[i]] = i;
        while (true) {
            int c = getchar();
            if (c == EOF) break;
            if (Index[c] == -1) {
                putchar(c);
            } else {
                putchar(S[Index[c] - 1]);
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}