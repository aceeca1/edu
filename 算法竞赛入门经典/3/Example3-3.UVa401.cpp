#include <cstdio>
#include <string>
using namespace std;

const char A[] = "AEHIJLMOSTUVWXYZ12358";
const char B[] = "A3HILJMO2TUVWXY51SEZ8";
char Mirror[256], Buffer[21];

struct Solution {
    string S;

    bool IsPalindrome() {
        int i1 = 0, i2 = S.size() - 1;
        while (i1 < i2) {
            if (S[i1] != S[i2]) return false;
            ++i1, --i2;
        }
        return true;
    }

    bool IsMirror() {
        int i1 = 0, i2 = S.size() - 1;
        while (i1 <= i2) {
            if (Mirror[S[i1]] != S[i2]) return false;
            ++i1, --i2;
        }
        return true;
    }

    void Solve() {
        for (int i = 0; i < 256; ++i) Mirror[i] = ' ';
        for (int i = 0; A[i]; ++i) Mirror[A[i]] = B[i];
        while (scanf("%s", Buffer) == 1) {
            S = Buffer;
            if (IsPalindrome()) {
                if (IsMirror()) {
                    printf("%s -- is a mirrored palindrome.\n\n", S.c_str());
                } else {
                    printf("%s -- is a regular palindrome.\n\n", S.c_str());
                }
            } else {
                if (IsMirror()) {
                    printf("%s -- is a mirrored string.\n\n", S.c_str());
                } else {
                    printf("%s -- is not a palindrome.\n\n", S.c_str());
                }
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}