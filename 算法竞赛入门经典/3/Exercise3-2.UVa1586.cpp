#include <cstdio>
#include <string>
using namespace std;

char Buffer[80];

struct Solution {
    string S;
    int P;

    void Solve() {
        scanf("%s", Buffer);
        S = Buffer;
        P = 0;
        double answer = 0.0;
        while (P < S.size()) {
            double mass = 0.0;
            switch (S[P]) {
                case 'C': mass = 12.01; break;
                case 'H': mass = 1.008; break;
                case 'O': mass = 16.00; break;
                case 'N': mass = 14.01; break;
            }
            int number = 1, chars = 0;
            sscanf(S.c_str() + P + 1, "%d%n", &number, &chars);
            P += 1 + chars;
            answer += mass * number;
        }
        printf("%.3lf\n", answer);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}