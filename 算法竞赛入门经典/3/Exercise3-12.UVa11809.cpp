#include <cstdio>
#include <cmath>
#include <string>
#include <vector>
using namespace std;

struct FloatFormat {
    double Base, Exponent;
    int M, E;
};

char Buffer[80];

bool AssignMin(double* p, double v) {
    if (v < *p) return *p = v, true;
    return false;
}

struct Solution {
    vector<FloatFormat> Formats;
    string S;

    void Solve() {
        for (int m = 9; m >= 0; --m) {
            for (int e = 30; e >= 1; --e) {
                int a1 = 1 << (1 + m);
                double a2 = double(a1 - 1) / a1;
                int b1 = 1 << e;
                double b2 = double(b1 - 1);
                double logMax = log10(a2) + log10(2.0) * b2;
                double exponent = floor(logMax);
                double base = pow(10.0, logMax - exponent);
                FloatFormat format = {base, exponent, m, e};
                Formats.push_back(format);
            }
        }
        while (true) {
            scanf("%s", &Buffer);
            S = Buffer;
            if (S == "0e0") break;
            S[17] = ' ';
            double base, exponent;
            sscanf(S.c_str(), "%lf%lf", &base, &exponent);
            double minError = 1.0 / 0.0;
            int arg = -1;
            for (int i = 0; i < Formats.size(); ++i) {
                if (Formats[i].Exponent != exponent) continue;
                if (AssignMin(&minError, abs(Formats[i].Base - base))) {
                    arg = i;
                }
            }
            printf("%d %d\n", Formats[arg].M, Formats[arg].E);
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
