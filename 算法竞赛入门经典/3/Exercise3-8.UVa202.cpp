#include <cstdio>
#include <map>
#include <string>
#include <vector>
using namespace std;

typedef map<int, int>::iterator Type1;
typedef pair<Type1, bool> Type2;

struct Solution {
    int A1, A2, Cycle;
    string Answer;
    map<int, int> RemPlace;

    bool Solve() {
        if (scanf("%d%d", &A1, &A2) != 2) return false;
        int B = A1 / A2;
        printf("%d/%d = %d.", A1, A2, B);
        A1 %= A2;
        for (int i = 0; ; ++i) {
            Type2 returnValue = RemPlace.insert(make_pair(A1, i));
            if (returnValue.second == false) {
                Cycle = i - returnValue.first->second;
                break;
            }
            A1 = A1 * 10;
            Answer += char('0' + A1 / A2);
            A1 %= A2;
        }
        printf("%s", Answer.substr(0, Answer.size() - Cycle).c_str());
        string Answer2 = Answer.substr(Answer.size() - Cycle);
        int answerSize = Answer2.size();
        if (50 < Answer2.size()) {
            Answer2.resize(50);
            Answer2 += "...";
        }
        printf("(%s)\n", Answer2.c_str());
        printf("   %d = number of digits in repeating cycle\n\n", answerSize);
        return true;
    }
};

int main() {
    while (Solution().Solve());
    return 0;
}