#include <cstdio>
#include <set>
#include <string>
#include <utility>
#include <vector>
using namespace std;

struct RCPair {
    int R, C;
};

char Buffer[3];

struct Solution {
    int R, C, Op, A;
    vector<vector<RCPair>> Spreadsheet, Old;
    string OpName;

    void DeleteColumn(const set<int>& no) {
        for (int i = 0; i < Spreadsheet.size(); ++i) {
            int newSize = 0;
            for (int j = 0; j < Spreadsheet[i].size(); ++j) {
                if (no.count(j)) continue;
                Spreadsheet[i][newSize++] = Spreadsheet[i][j];
            }
            Spreadsheet[i].resize(newSize);
        }
    }

    void DeleteRow(const set<int>& no) {
        int newSize = 0;
        for (int i = 0; i < Spreadsheet.size(); ++i) {
            if (no.count(i)) continue;
            swap(Spreadsheet[newSize++], Spreadsheet[i]);
        }
        Spreadsheet.resize(newSize);
    }

    void InsertColumn(const set<int>& no) {
        for (int i = 0; i < Spreadsheet.size(); ++i) {
            vector<RCPair> answer;
            for (int j = 0; j < Spreadsheet[i].size(); ++j) {
                if (no.count(j)) {
                    RCPair newColumn = {-1, -1};
                    answer.push_back(newColumn);
                }
                answer.push_back(Spreadsheet[i][j]);
            }
            swap(Spreadsheet[i], answer);
        }
    }

    void InsertRow(const set<int>& no) {
        vector<vector<RCPair>> answer;
        for (int i = 0; i < Spreadsheet.size(); ++i) {
            if (no.count(i)) {
                vector<RCPair> newRow(Spreadsheet[i].size());
                for (int i = 0; i < newRow.size(); ++i) {
                    newRow[i].R = newRow[i].C = -1;
                }
                answer.push_back(newRow);
            }
            answer.push_back(Spreadsheet[i]);
        }
        swap(Spreadsheet, answer);
    }

    void Exchange(int r1, int c1, int r2, int c2) {
        swap(Spreadsheet[r1][c1], Spreadsheet[r2][c2]);
    }

    bool Solve(int caseNo) {
        scanf("%d%d", &R, &C);
        if (R == 0) return false;
        Spreadsheet.resize(R);
        for (int i = 0; i < R; ++i) {
            Spreadsheet[i].resize(C);
            for (int j = 0; j < C; ++j) {
                Spreadsheet[i][j].R = i;
                Spreadsheet[i][j].C = j;
            }
        }
        scanf("%d", &Op);
        for (int i = 0; i < Op; ++i) {
            scanf("%s", Buffer);
            OpName = Buffer;
            if (OpName == "EX") {
                int r1, c1, r2, c2;
                scanf("%d%d%d%d", &r1, &c1, &r2, &c2);
                Exchange(r1 - 1, c1 - 1, r2 - 1, c2 - 1);
            } else {
                scanf("%d", &A);
                set<int> no;
                for (int j = 0; j < A; ++j) {
                    int no1;
                    scanf("%d", &no1);
                    no.insert(no1 - 1);
                }
                if (OpName == "DC") DeleteColumn(no);
                if (OpName == "DR") DeleteRow(no);
                if (OpName == "IC") InsertColumn(no);
                if (OpName == "IR") InsertRow(no);
            }
        }
        Old.resize(R);
        for (int i = 0; i < R; ++i) {
            Old[i].resize(C);
            for (int j = 0; j < C; ++j) Old[i][j].R = Old[i][j].C = -1;
        }
        for (int i = 0; i < Spreadsheet.size(); ++i) {
            for (int j = 0; j < Spreadsheet[i].size(); ++j) {
                int r = Spreadsheet[i][j].R;
                int c = Spreadsheet[i][j].C;
                if (r == -1) continue;
                Old[r][c].R = i;
                Old[r][c].C = j;
            }
        }
        if (caseNo != 1) printf("\n");
        printf("Spreadsheet #%d\n", caseNo);
        scanf("%d", &Op);
        for (int i = 0; i < Op; ++i) {
            int r, c;
            scanf("%d%d", &r, &c), --r, --c;
            if (Old[r][c].R == -1) {
                printf("Cell data in (%d,%d) GONE\n", r + 1, c + 1);
            } else {
                printf("Cell data in (%d,%d) moved to (%d,%d)\n", r + 1, c + 1,
                    Old[r][c].R + 1, Old[r][c].C + 1);
            }
        }
        return true;
    }
};

int main() {
    int caseNo = 1;
    while (Solution().Solve(caseNo)) ++caseNo;
    return 0;
}
