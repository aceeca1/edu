#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, K, M;
    vector<int> A;

    void Go(int* p, int delta) {
        while (true) {
            *p = (*p + delta) % N;
            if (A[*p] != 0) return;
        }
    }

    bool Solve() {
        scanf("%d%d%d", &N, &K, &M);
        if (N == 0) return false;
        A.resize(N);
        for (int i = 0; i < N; ++i) A[i] = i + 1;
        int left = N;
        int p1 = N - 1, p2 = 0;
        while (left > 0) {
            for (int i = 0; i < K; ++i) Go(&p1, 1);
            for (int i = 0; i < M; ++i) Go(&p2, N - 1);
            if (p1 == p2) {
                printf("%3d", A[p1]);
                --left;
            } else {
                printf("%3d%3d", A[p1], A[p2]);
                left -= 2;
            }
            A[p1] = A[p2] = 0;
            if (left > 0) printf(",");
        }
        printf("\n");
        return true;
    }
};

int main() {
    while (Solution().Solve());
    return 0;
}
