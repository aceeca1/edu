#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[101];

struct Solution {
    string S1, S2;

    vector<int> Stat(const string& s) {
        vector<int> num(26);
        for (int i = 0; i < s.size(); ++i) {
            ++num[s[i] - 'A'];
        }
        vector<int> answer(s.size() + 1);
        for (int i = 0; i < 26; ++i) ++answer[num[i]];
        return answer;
    }

    bool Solve() {
        if (scanf("%s", Buffer) != 1) return false;
        S1 = Buffer;
        if (scanf("%s", Buffer) != 1) return false;
        S2 = Buffer;
        printf(Stat(S1) == Stat(S2) ? "YES\n" : "NO\n");
        return true;
    }
};

int main() {
    while (Solution().Solve());
    return 0;
}
