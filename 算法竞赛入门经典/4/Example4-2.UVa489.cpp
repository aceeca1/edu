#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[80];

struct Solution {
    int No;
    string S1, S2;

    string GetResult() {
        vector<bool> inS1(26);
        for (int i = 0; i < S1.size(); ++i) inS1[S1[i] - 'a'] = true;
        int inS1Size = 0;
        for (int i = 0; i < 26; ++i) {
            if (inS1[i]) ++inS1Size;
        }
        int chance = 7;
        for (int i = 0; i < S2.size(); ++i) {
            if (inS1[S2[i] - 'a']) {
                inS1[S2[i] - 'a'] = false;
                if (--inS1Size == 0) return "You win.";
            } else {
                if (--chance == 0) return "You lose.";
            }
        }
        return "You chickened out.";
    }

    bool Solve() {
        scanf("%d", &No);
        if (No == -1) return false;
        scanf("%s", Buffer);
        S1 = Buffer;
        scanf("%s", Buffer);
        S2 = Buffer;
        printf("Round %d\n", No);
        printf("%s\n", GetResult().c_str());
        return true;
    }
};

int main() {
    while (Solution().Solve());
    return 0;
}
