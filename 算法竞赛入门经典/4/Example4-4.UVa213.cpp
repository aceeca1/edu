#include <cstdio>
#include <map>
#include <string>
using namespace std;

struct Solution {
    string Header;
    int HeaderPosition;
    map<string, char> Map;
    string Partial;

    bool ReadHeader() {
        while (true) {
            int c = getchar();
            if (c == '\n') return true;
            if (c == EOF) return false;
            Header += c;
        }
    }

    bool AllOne(const string& s) {
        for (int i = 0; i < s.size(); ++i) {
            if (s[i] != '1') return false;
        }
        return true;
    }

    void GenerateMap(int n) {
        if (Partial.size() == n) {
            if (AllOne(Partial) || Header.size() <= HeaderPosition) return;
            Map[Partial] = Header[HeaderPosition++];
            return;
        }
        Partial += '0';
        GenerateMap(n);
        Partial.pop_back();
        Partial += '1';
        GenerateMap(n);
        Partial.pop_back();
    }

    int GetCharN() {
        while (true) {
            int c = getchar();
            if (c != '\n') return c;
        }
    }

    bool Solve() {
        if (!ReadHeader()) return false;
        HeaderPosition = 0;
        for (int i = 1; i <= 7; ++i) GenerateMap(i);
        string answer;
        while (true) {
            int num1 = (GetCharN() - '0') << 2;
            int num2 = (GetCharN() - '0') << 1;
            int num3 = (GetCharN() - '0') << 0;
            int num = num1 + num2 + num3;
            if (num == 0) break;
            while (true) {
                string S;
                for (int i = 0; i < num; ++i) S += GetCharN();
                if (AllOne(S)) break;
                answer += Map[S];
            }
        }
        getchar();
        printf("%s\n", answer.c_str());
        return true;
    }
};

int main() {
    while (Solution().Solve());
    return 0;
}
