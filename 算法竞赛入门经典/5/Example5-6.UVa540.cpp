#include <cstdio>
#include <map>
#include <queue>
#include <string>
#include <vector>
using namespace std;

char Buffer[8];

struct Solution {
    int T, N, X;
    map<int, int> Team;
    queue<int> TeamQueue;
    vector<queue<int> > Queue;
    string S;

    bool Solve(int caseNo) {
        scanf("%d", &T);
        if (T == 0) return false;
        Queue.resize(T);
        printf("Scenario #%d\n", caseNo);
        for (int i = 0; i < T; ++i) {
            scanf("%d", &N);
            for (int j = 0; j < N; ++j) {
                scanf("%d", &X);
                Team[X] = i;
            }
        }
        while (true) {
            scanf("%s", Buffer);
            S = Buffer;
            if (S == "STOP") break;
            if (S == "ENQUEUE") {
                scanf("%d", &X);
                int team = Team[X];
                if (Queue[team].empty()) TeamQueue.push(team);
                Queue[team].push(X);
            }
            if (S == "DEQUEUE") {
                int team = TeamQueue.front();
                printf("%d\n", Queue[team].front());
                Queue[team].pop();
                if (Queue[team].empty()) TeamQueue.pop();
            }
        }
        printf("\n");
        return true;
    }
};

int main() {
    int caseNo = 1;
    while (Solution().Solve(caseNo)) ++caseNo;
    return 0;
}
