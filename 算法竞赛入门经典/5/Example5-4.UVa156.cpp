#include <algorithm>
#include <cstdio>
#include <map>
#include <string>
#include <vector>
using namespace std;

char Buffer[21];

struct Solution {
    string S;
    vector<string> Words, Features, Answer;
    map<string, int> Count;

    void Solve() {
        while (true) {
            scanf("%s", Buffer);
            S = Buffer;
            if (S == "#") break;
            Words.push_back(S);
            for (int j = 0; j < S.size(); ++j) S[j] = tolower(S[j]);
            sort(S.begin(), S.end());
            Features.push_back(S);
            ++Count[S];
        }
        for (int i = 0; i < Features.size(); ++i) {
            if (Count[Features[i]] == 1) Answer.push_back(Words[i]);
        }
        sort(Answer.begin(), Answer.end());
        for (int i = 0; i < Answer.size(); ++i) {
            printf("%s\n", Answer[i].c_str());
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
