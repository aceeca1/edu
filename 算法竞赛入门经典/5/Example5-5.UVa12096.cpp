#include <cstdio>
#include <map>
#include <set>
#include <stack>
#include <string>
#include <vector>
using namespace std;

typedef set<int> Set;
typedef map<Set, int>::iterator Type1;
typedef pair<Type1, bool> Type2;
typedef Set::iterator Type3;

struct SetCache {
    vector<Set> ToSet;
    map<Set, int> ToID;

    int GetID(const Set& s) {
        Type2 returnValue = ToID.insert(make_pair(s, ToSet.size()));
        if (returnValue.second) {
            ToSet.push_back(s);
            return ToSet.size() - 1;
        }
        return returnValue.first->second;
    }

    int Union(int s1, int s2) {
        Set answer(ToSet[s1]);
        for (Type3 it = ToSet[s2].begin(); it != ToSet[s2].end(); ++it) {
            answer.insert(*it);
        }
        return GetID(answer);
    }

    int Intersect(int s1, int s2) {
        Set answer;
        for (Type3 it = ToSet[s1].begin(); it != ToSet[s1].end(); ++it) {
            if (ToSet[s2].count(*it)) answer.insert(*it);
        }
        return GetID(answer);
    }

    int Add(int s1, int s2) {
        Set answer(ToSet[s1]);
        answer.insert(s2);
        return GetID(answer);
    }
};

char Buffer[10];

struct Solution {
    SetCache Cache;
    int N;
    string S;
    stack<int> Stack;

    void Solve() {
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            scanf("%s", Buffer);
            S = Buffer;
            if (S == "PUSH") Stack.push(Cache.GetID(Set()));
            if (S == "DUP") Stack.push(Stack.top());
            if (S == "UNION") {
                int s1 = Stack.top(); Stack.pop();
                int s2 = Stack.top(); Stack.pop();
                Stack.push(Cache.Union(s2, s1));
            }
            if (S == "INTERSECT") {
                int s1 = Stack.top(); Stack.pop();
                int s2 = Stack.top(); Stack.pop();
                Stack.push(Cache.Intersect(s2, s1));
            }
            if (S == "ADD") {
                int s1 = Stack.top(); Stack.pop();
                int s2 = Stack.top(); Stack.pop();
                Stack.push(Cache.Add(s2, s1));
            }
            printf("%d\n", Cache.ToSet[Stack.top()].size());
        }
        printf("***\n");
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
