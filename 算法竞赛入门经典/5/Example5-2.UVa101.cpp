#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[5];

struct Solution {
    int N;
    vector<vector<int> > Pile;
    string S1, S2;
    int P1, P2, Pile1, Num1, Pile2, Num2;

    void FindBlock(int p, int* pile, int* num) {
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < Pile[i].size(); ++j) {
                if (Pile[i][j] == p) {
                    *pile = i;
                    *num = j;
                    return;
                }
            }
        }
    }

    void MoveBack(int pile, int num) {
        for (int i = num + 1; i < Pile[pile].size(); ++i) {
            int b = Pile[pile][i];
            Pile[b].push_back(b);
        }
        Pile[pile].resize(num + 1);
    }

    void MovePile(int pile1, int num1, int pile2) {
        for (int i = num1; i < Pile[pile1].size(); ++i) {
            Pile[pile2].push_back(Pile[pile1][i]);
        }
        Pile[pile1].resize(num1);
    }

    void Solve() {
        scanf("%d", &N);
        Pile.resize(N);
        for (int i = 0; i < N; ++i) Pile[i].push_back(i);
        while (true) {
            scanf("%s", Buffer);
            S1 = Buffer;
            if (S1 == "quit") break;
            scanf("%d%s%d", &P1, Buffer, &P2);
            S2 = Buffer;
            FindBlock(P1, &Pile1, &Num1);
            FindBlock(P2, &Pile2, &Num2);
            if (Pile1 == Pile2) continue;
            if (S1 == "move") MoveBack(Pile1, Num1);
            if (S2 == "onto") MoveBack(Pile2, Num2);
            MovePile(Pile1, Num1, Pile2);
        }
        for (int i = 0; i < N; ++i) {
            printf("%d:", i);
            for (int j = 0; j < Pile[i].size(); ++j) {
                printf(" %d", Pile[i][j]);
            }
            printf("\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
