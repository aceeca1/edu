#include <cstdio>
#include <map>
#include <string>
#include <utility>
#include <vector>
using namespace std;

typedef map<string, int>::iterator Type1;
typedef pair<Type1, bool> Type2;

char Buffer[81];

struct Solution {
    int N, M;
    vector<vector<string> > Table;

    bool Solve() {
        if (scanf("%d%d", &N, &M) != 2) return false;
        getchar();
        Table.resize(N);
        for (int i = 0; i < N; ++i) {
            Table[i].resize(M);
            for (int j = 0; j < M; ++j) {
                scanf("%[^,\n]", Buffer);
                getchar();
                Table[i][j] = Buffer;
            }
        }
        for (int c1 = 0; c1 < M; ++c1) {
            for (int c2 = c1 + 1; c2 < M; ++c2) {
                map<string, int> row;
                for (int i = 0; i < N; ++i) {
                    string content(Table[i][c1]);
                    content += ',';
                    content += Table[i][c2];
                    Type2 returnValue = row.insert(make_pair(content, i));
                    if (!returnValue.second) {
                        int r1 = returnValue.first->second, r2 = i;
                        printf("NO\n%d %d\n%d %d\n",
                            r1 + 1, r2 + 1, c1 + 1, c2 + 1);
                        return true;
                    }
                }
            }
        }
        printf("YES\n");
        return true;
    }
};

int main() {
    while (Solution().Solve());
    return 0;
}
