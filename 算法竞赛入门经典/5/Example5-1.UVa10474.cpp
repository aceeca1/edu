#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N, Q;
    vector<int> A;

    bool Solve(int caseNo) {
        scanf("%d%d", &N, &Q);
        if (N == 0) return false;
        printf("CASE# %d:\n", caseNo);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        sort(A.begin(), A.end());
        for (int i = 0; i < Q; ++i) {
            int x;
            scanf("%d", &x);
            int p = lower_bound(A.begin(), A.end(), x) - A.begin();
            if (p < A.size() && A[p] == x) {
                printf("%d found at %d\n", x, p + 1);
            } else {
                printf("%d not found\n", x);
            }
        }
        return true;
    }
};

int main() {
    int caseNo = 1;
    while (Solution().Solve(caseNo)) ++caseNo;
    return 0;
}
