#include <cstdio>
#include <cctype>
#include <set>
#include <string>
using namespace std;

typedef set<string>::iterator Type1;

char Buffer[80];

struct Solution {
    set<string> Dictionary;
    string S;

    void Solve() {
        while (true) {
            if (scanf("%s", Buffer) != 1) break;
            S = Buffer;
            for (int i = 0; i < S.size(); ++i) {
                if (isalpha(S[i])) {
                    S[i] = tolower(S[i]);
                } else {
                    S[i] = ' ';
                }
            }
            int p = 0;
            while (p < S.size()) {
                int delta = 0;
                sscanf(S.c_str() + p, "%s%n", Buffer, &delta);
                if (delta == 0) break;
                p += delta;
                Dictionary.insert(Buffer);
            }
        }
        for (Type1 it = Dictionary.begin(); it != Dictionary.end(); ++it) {
            printf("%s\n", it->c_str());
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
