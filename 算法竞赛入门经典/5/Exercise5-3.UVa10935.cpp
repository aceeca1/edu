#include <cstdio>
#include <queue>
using namespace std;

struct Solution {
    int N;
    queue<int> A;

    bool Solve() {
        scanf("%d", &N);
        if (N == 0) return false;
        for (int i = 1; i <= N; ++i) A.push(i);
        printf("Discarded cards:");
        if (N != 1) putchar(' ');
        bool head = true;
        for (int i = 1; i < N; ++i) {
            if (!head) printf(", ");
            head = false;
            printf("%d", A.front());
            A.pop();
            A.push(A.front());
            A.pop();
        }
        printf("\nRemaining card: %d\n", A.front());
        return true;
    }
};

int main() {
    while (Solution().Solve());
    return 0;
}
