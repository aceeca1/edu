#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Coordinate {
    int X, Y;
};

struct OrderL {
    bool operator()(const Coordinate& c1, const Coordinate& c2) {
        return c1.X < c2.X || c1.X == c2.X && c1.Y < c2.Y;
    }
};

struct OrderR {
    bool operator()(const Coordinate& c1, const Coordinate& c2) {
        return c1.X < c2.X || c1.X == c2.X && c2.Y < c1.Y;
    }
};

struct Solution {
    int N;
    vector<Coordinate> A;

    bool Symmetry() {
        sort(A.begin(), A.end(), OrderL());
        int mid2 = A[0].X + A.back().X;
        int p = 0, q = A.size() - 1;
        while (p <= q && A[p].X * 2 < mid2) ++p;
        while (p <= q && mid2 < A[q].X * 2) --q;
        sort(A.begin() + q + 1, A.end(), OrderR());
        for (int i = 1; ; ++i) {
            if (p - i < 0 && N <= q + i) return true;
            if (p - i < 0 || N <= q + i) return false;
            if (A[p - i].X + A[q + i].X != mid2) return false;
            if (A[p - i].Y != A[q + i].Y) return false;
        }
    }

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%d%d", &A[i].X, &A[i].Y);
        }
        printf(Symmetry() ? "YES\n" : "NO\n");
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
