#include <cstdio>
#include <queue>
using namespace std;

struct Solution {
    int N, M, QMax;
    queue<int> Q;
    priority_queue<int> P;

    void Solve() {
        scanf("%d%d", &N, &M);
        for (int i = 0; i < N; ++i) {
            int x;
            scanf("%d", &x);
            Q.push(x);
            P.push(x);
        }
        for (int time = 1; ;) {
            int front = Q.front();
            Q.pop(), --M;
            if (front == P.top()) {
                P.pop();
                if (M == -1) {
                    printf("%d\n", time);
                    break;
                }
                ++time;
            } else {
                Q.push(front);
                if (M == -1) M = Q.size() - 1;
            }
        }
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
