#include <algorithm>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[61];

bool AssignMax(int *p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    int N, M;
    vector<string> File;

    bool Solve() {
        if (scanf("%d", &N) != 1) return false;
        M = 0;
        File.resize(N);
        for (int i = 0; i < N; ++i) {
            scanf("%s", Buffer);
            File[i] = Buffer;
            AssignMax(&M, File[i].size());
        }
        int columns = (60 - M) / (M + 2) + 1;
        int rows = (N + columns - 1) / columns;
        printf("------------------------------"
               "------------------------------\n");
        sort(File.begin(), File.end());
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < columns; ++j) {
                int index = i + j * rows;
                if (index < N) {
                    sprintf(Buffer, "%%%ds", j == columns - 1 ? M : M + 2);
                    printf(Buffer, File[index].c_str());
                }
            }
            printf("\n");
        }
        return true;
    }
};

int main() {
    while (Solution().Solve());
    return 0;
}
