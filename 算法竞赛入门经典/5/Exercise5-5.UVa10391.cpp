#include <algorithm>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[80];

struct Solution {
    vector<string> Words;

    bool HasWord(const string& s) {
        int lower = lower_bound(Words.begin(), Words.end(), s) - Words.begin();
        return lower < Words.size() && Words[lower] == s;
    }

    bool IsCompoundWord(const string& s) {
        for (int i = 1; i < s.size(); ++i) {
            if (HasWord(s.substr(0, i)) && HasWord(s.substr(i))) return true;
        }
        return false;
    }

    void Solve() {
        while (true) {
            if (scanf("%s", Buffer) != 1) break;
            Words.push_back(Buffer);
        }
        for (int i = 0; i < Words.size(); ++i) {
            if (IsCompoundWord(Words[i])) printf("%s\n", Words[i].c_str());
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
