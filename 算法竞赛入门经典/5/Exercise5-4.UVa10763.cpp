#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int N;
    vector<pair<int, int> > Candidate, Reverse;

    bool Solve() {
        scanf("%d", &N);
        if (N == 0) return false;
        Candidate.resize(N);
        Reverse.resize(N);
        for (int i = 0; i < N; ++i) {
            int a, b;
            scanf("%d%d", &a, &b);
            Candidate[i] = make_pair(a, b);
            Reverse[i] = make_pair(b, a);
        }
        sort(Candidate.begin(), Candidate.end());
        sort(Reverse.begin(), Reverse.end());
        printf("%s\n", Candidate == Reverse ? "YES" : "NO");
        return true;
    }
};

int main() {
    while (Solution().Solve());
    return 0;
}
