#include <cstdio>
#include <set>
using namespace std;

typedef long long int64;

struct Solution {
    set<int64> Set;

    void Solve() {
        Set.insert(1);
        for (int i = 1; i < 1500; ++i) {
            int64 front = *Set.begin();
            Set.erase(Set.begin());
            Set.insert(front * 2);
            Set.insert(front * 3);
            Set.insert(front * 5);
        }
        printf("%lld\n", *Set.begin());
    }
};

int main() {
    Solution().Solve();
    return 0;
}
