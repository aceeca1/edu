#include <cmath>
#include <cstdio>
#include <set>
#include <string>
#include <utility>
#include <vector>
using namespace std;

typedef set<string>::iterator Type1;
typedef pair<Type1, bool> Type2;

char Buffer[80];

struct Solution {
    int N;
    vector<int> A;
    set<string> S;

    bool AllZero() {
        for (int i = 0; i < A.size(); ++i) {
            if (A[i] != 0) return false;
        }
        return true;
    }

    bool Visited() {
        string s;
        for (int i = 0; i < A.size(); ++i) {
            sprintf(Buffer, "%d", A[i]);
            s += Buffer;
            s += ',';
        }
        Type2 returnValue = S.insert(s);
        return !returnValue.second;
    }

    void Ducci() {
        vector<int> answer(A.size());
        for (int i = 0; i < A.size() - 1; ++i) {
            answer[i] = abs(A[i] - A[i + 1]);
        }
        answer.back() = abs(A.back() - A[0]);
        swap(A, answer);
    }

    string Compute() {
        while (true) {
            if (AllZero()) return "ZERO";
            if (Visited()) return "LOOP";
            Ducci();
        }
    }

    void Solve() {
        scanf("%d", &N);
        A.resize(N);
        for (int i = 0; i < N; ++i) scanf("%d", &A[i]);
        printf("%s\n", Compute().c_str());
    }
};

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i) Solution().Solve();
    return 0;
}
