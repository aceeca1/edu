#include <cstdio>
#include <string>
#include <vector>
using namespace std;

char Buffer[182];

bool AssignMax(int *p, int v) {
    if (*p < v) return *p = v, true;
    return false;
}

struct Solution {
    vector<vector<string> > S;
    vector<int> Length;
    string Line;

    void Solve() {
        while (fgets(Buffer, 182, stdin)) {
            S.push_back(vector<string>());
            Line = Buffer;
            int p = 0;
            while (true) {
                int pDelta = 0;
                sscanf(Line.c_str() + p, "%s%n", Buffer, &pDelta);
                if (pDelta == 0) break;
                p += pDelta;
                S.back().push_back(Buffer);
            }
        }
        for (int i = 0; i < S.size(); ++i) {
            for (int j = 0; j < S[i].size(); ++j) {
                if (Length.size() <= j) Length.resize(j + 1);
                AssignMax(&Length[j], S[i][j].size());
            }
        }
        for (int i = 0; i < S.size(); ++i) {
            for (int j = 0; j < S[i].size(); ++j) {
                if (j == S[i].size() - 1) {
                    printf("%s\n", S[i][j].c_str());
                } else {
                    printf("%*s", -(Length[j] + 1), S[i][j].c_str());
                }
            }
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
