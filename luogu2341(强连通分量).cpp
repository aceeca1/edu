#include <cstdio>
#include <stack>
#include <vector>
using namespace std;

bool AssignMin(int* p, int v) {
    if (v < *p) return *p = v, true;
    return false;
}

struct Solution {
    int N, M, MaxDFSNumber, MaxSCC;
    vector<vector<int> > Out;
    vector<int> DFSNumber, Low, SCC, SCCOut;
    stack<int> Stack;
    
    void Visit(int no) {
        Low[no] = DFSNumber[no] = ++MaxDFSNumber;
        Stack.push(no);
        for (int i = 0; i < Out[no].size(); ++i) {
            int target = Out[no][i];
            if (DFSNumber[target] == 0) {
                Visit(target);
                AssignMin(&Low[no], Low[target]);
            } else if (SCC[target] == 0) {
                AssignMin(&Low[no], DFSNumber[target]);
            }
        }
        if (DFSNumber[no] == Low[no]) {
            ++MaxSCC;
            while (true) {
                int top = Stack.top();
                Stack.pop();
                SCC[top] = MaxSCC;
                if (top == no) break;
            }
        }
    }
    
    void GetSCC() {
        DFSNumber.resize(N);
        MaxDFSNumber = 0;
        Low.resize(N);
        SCC.resize(N);
        for (int i = 0; i < N; ++i) {
            if (DFSNumber[i] == 0) Visit(i);
        }
    }
    
    void Solve() {
        scanf("%d%d", &N, &M);
        Out.resize(N);
        for (int i = 0; i < M; ++i) {
            int u1, u2;
            scanf("%d%d", &u1, &u2);
            Out[u1 - 1].push_back(u2 - 1);
        }
        GetSCC();
        SCCOut.resize(MaxSCC + 1);
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < Out[i].size(); ++j) {
                int target = Out[i][j];
                if (SCC[target] == SCC[i]) continue;
                ++SCCOut[SCC[i]];
            }
        }
        int zeroSCCOut = 0;
        for (int i = 1; i <= MaxSCC; ++i) {
            if (SCCOut[i] == 0) ++zeroSCCOut;
        }
        if (zeroSCCOut == 1) {
            int id;
            for (int i = 1; i <= MaxSCC; ++i) {
                if (SCCOut[i] == 0) id = i;
            }
            int answer = 0;
            for (int i = 0; i < N; ++i) {
                if (SCC[i] == id) ++answer;
            }
            printf("%d\n", answer);
        } else {
            printf("0\n");
        }
    }
};

int main() {
    Solution().Solve();
    return 0;
}
